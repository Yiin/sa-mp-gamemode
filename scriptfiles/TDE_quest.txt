//Global TextDraws: 


new Text:TDEditor_TD[3];

TDEditor_TD[0] = TextDrawCreate(441.667358, 305.318237, "Daugiau informacijos");
TextDrawLetterSize(TDEditor_TD[0], 0.150665, 1.052443);
TextDrawAlignment(TDEditor_TD[0], 2);
TextDrawColor(TDEditor_TD[0], -1);
TextDrawSetShadow(TDEditor_TD[0], 1);
TextDrawSetOutline(TDEditor_TD[0], 0);
TextDrawBackgroundColor(TDEditor_TD[0], 255);
TextDrawFont(TDEditor_TD[0], 2);
TextDrawSetProportional(TDEditor_TD[0], 1);
TextDrawSetShadow(TDEditor_TD[0], 1);
TextDrawSetSelectable(TDEditor_TD[0], true);

TDEditor_TD[1] = TextDrawCreate(489.666656, 288.155700, "LD_BEAT:cross");
TextDrawLetterSize(TDEditor_TD[1], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[1], 12.000000, 12.000000);
TextDrawAlignment(TDEditor_TD[1], 1);
TextDrawColor(TDEditor_TD[1], -1);
TextDrawSetShadow(TDEditor_TD[1], 0);
TextDrawSetOutline(TDEditor_TD[1], 0);
TextDrawBackgroundColor(TDEditor_TD[1], 255);
TextDrawFont(TDEditor_TD[1], 4);
TextDrawSetProportional(TDEditor_TD[1], 0);
TextDrawSetShadow(TDEditor_TD[1], 0);
TextDrawSetSelectable(TDEditor_TD[1], true);

TDEditor_TD[2] = TextDrawCreate(511.333282, 288.725860, "esc");
TextDrawLetterSize(TDEditor_TD[2], 0.201000, 1.089778);
TextDrawAlignment(TDEditor_TD[2], 2);
TextDrawColor(TDEditor_TD[2], -1);
TextDrawSetShadow(TDEditor_TD[2], 0);
TextDrawSetOutline(TDEditor_TD[2], -1);
TextDrawBackgroundColor(TDEditor_TD[2], 47);
TextDrawFont(TDEditor_TD[2], 2);
TextDrawSetProportional(TDEditor_TD[2], 1);
TextDrawSetShadow(TDEditor_TD[2], 0);
TextDrawSetSelectable(TDEditor_TD[2], true);



//Player TextDraws: 


new PlayerText:TDEditor_PTD[MAX_PLAYERS][6];

TDEditor_PTD[playerid][0] = CreatePlayerTextDraw(playerid, 169.333541, 275.710937, "");
PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][0], 0.000000, 0.000000);
PlayerTextDrawTextSize(playerid, TDEditor_PTD[playerid][0], 158.000000, 170.000000);
PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][0], 1);
PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][0], -1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][0], 0);
PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][0], 0);
PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][0], 0);
PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][0], 5);
PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][0], 0);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][0], 0);
PlayerTextDrawSetPreviewModel(playerid, TDEditor_PTD[playerid][0], 292);
PlayerTextDrawSetPreviewRot(playerid, TDEditor_PTD[playerid][0], 0.000000, 0.000000, 20.000000, 1.000000);

TDEditor_PTD[playerid][1] = CreatePlayerTextDraw(playerid, 271.999908, 332.696319, "E ciuvas, gal zoles parupintum, atsiskaitau grynais.");
PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][1], 0.237663, 1.193480);
PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][1], 1);
PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][1], -1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][1], 0);
PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][1], -1);
PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][1], 100);
PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][1], 1);
PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][1], 1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][1], 0);

TDEditor_PTD[playerid][2] = CreatePlayerTextDraw(playerid, 317.666442, 360.903839, "Ble bbzn kaip cia bus su tavim, turiu savu reikalu.");
PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][2], 0.271665, 1.288887);
PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][2], 1);
PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][2], -1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][2], 0);
PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][2], -1);
PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][2], 70);
PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][2], 1);
PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][2], 1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][2], 0);
PlayerTextDrawSetSelectable(playerid, TDEditor_PTD[playerid][2], true);

TDEditor_PTD[playerid][3] = CreatePlayerTextDraw(playerid, 318.666534, 379.155761, "Yo, seni, be problemu!");
PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][3], 0.271665, 1.288887);
PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][3], 1);
PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][3], -1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][3], 0);
PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][3], -1);
PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][3], 70);
PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][3], 1);
PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][3], 1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][3], 0);
PlayerTextDrawSetSelectable(playerid, TDEditor_PTD[playerid][3], true);

TDEditor_PTD[playerid][4] = CreatePlayerTextDraw(playerid, 319.333190, 396.992950, ">> Iduoti mentams");
PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][4], 0.271665, 1.288887);
PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][4], 1);
PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][4], -822083329);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][4], 0);
PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][4], -1);
PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][4], 70);
PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][4], 1);
PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][4], 1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][4], 0);
PlayerTextDrawSetSelectable(playerid, TDEditor_PTD[playerid][4], true);

TDEditor_PTD[playerid][5] = CreatePlayerTextDraw(playerid, 269.333465, 301.170471, "Samuel L. Jackson");
PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][5], 0.662333, 2.653628);
PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][5], 1);
PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][5], -1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][5], 0);
PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][5], -1);
PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][5], 255);
PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][5], 0);
PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][5], 1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][5], 0);




//Total textdraws exported: 9 (3 global textdraws / 6 player textdraws) ~ 18/12/2016 ~ 3:38:56
TDEditor V1.17 BY ADRI1