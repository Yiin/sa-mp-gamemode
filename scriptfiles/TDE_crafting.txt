//Global TextDraws: 


new Text:TDEditor_TD[42];

TDEditor_TD[0] = TextDrawCreate(167.000030, 104.807327, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[0], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[0], 354.000000, 296.000000);
TextDrawAlignment(TDEditor_TD[0], 1);
TextDrawColor(TDEditor_TD[0], 4569);
TextDrawSetShadow(TDEditor_TD[0], 0);
TextDrawSetOutline(TDEditor_TD[0], 0);
TextDrawBackgroundColor(TDEditor_TD[0], 255);
TextDrawFont(TDEditor_TD[0], 4);
TextDrawSetProportional(TDEditor_TD[0], 0);
TextDrawSetShadow(TDEditor_TD[0], 0);

TDEditor_TD[1] = TextDrawCreate(168.333404, 106.996284, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[1], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[1], 351.000000, 16.000000);
TextDrawAlignment(TDEditor_TD[1], 1);
TextDrawColor(TDEditor_TD[1], -250);
TextDrawSetShadow(TDEditor_TD[1], 0);
TextDrawSetOutline(TDEditor_TD[1], 0);
TextDrawBackgroundColor(TDEditor_TD[1], 255);
TextDrawFont(TDEditor_TD[1], 4);
TextDrawSetProportional(TDEditor_TD[1], 0);
TextDrawSetShadow(TDEditor_TD[1], 0);

TDEditor_TD[2] = TextDrawCreate(313.666687, 109.111114, "Gaminimas");
TextDrawLetterSize(TDEditor_TD[2], 0.191666, 1.015110);
TextDrawAlignment(TDEditor_TD[2], 1);
TextDrawColor(TDEditor_TD[2], -7681);
TextDrawSetShadow(TDEditor_TD[2], 0);
TextDrawSetOutline(TDEditor_TD[2], 0);
TextDrawBackgroundColor(TDEditor_TD[2], 255);
TextDrawFont(TDEditor_TD[2], 2);
TextDrawSetProportional(TDEditor_TD[2], 1);
TextDrawSetShadow(TDEditor_TD[2], 0);

TDEditor_TD[3] = TextDrawCreate(505.000030, 109.940750, "esc");
TextDrawLetterSize(TDEditor_TD[3], 0.191666, 1.015110);
TextDrawAlignment(TDEditor_TD[3], 2);
TextDrawColor(TDEditor_TD[3], -1523963137);
TextDrawSetShadow(TDEditor_TD[3], 0);
TextDrawSetOutline(TDEditor_TD[3], 0);
TextDrawBackgroundColor(TDEditor_TD[3], 255);
TextDrawFont(TDEditor_TD[3], 2);
TextDrawSetProportional(TDEditor_TD[3], 1);
TextDrawSetShadow(TDEditor_TD[3], 0);

TDEditor_TD[4] = TextDrawCreate(178.333312, 142.970336, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[4], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[4], 64.000000, 18.000000);
TextDrawAlignment(TDEditor_TD[4], 1);
TextDrawColor(TDEditor_TD[4], 168434687);
TextDrawSetShadow(TDEditor_TD[4], 0);
TextDrawSetOutline(TDEditor_TD[4], 0);
TextDrawBackgroundColor(TDEditor_TD[4], 255);
TextDrawFont(TDEditor_TD[4], 4);
TextDrawSetProportional(TDEditor_TD[4], 0);
TextDrawSetShadow(TDEditor_TD[4], 0);
TextDrawSetSelectable(TDEditor_TD[4], true);

TDEditor_TD[5] = TextDrawCreate(243.666625, 142.970382, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[5], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[5], 64.000000, 18.000000);
TextDrawAlignment(TDEditor_TD[5], 1);
TextDrawColor(TDEditor_TD[5], 336864767);
TextDrawSetShadow(TDEditor_TD[5], 0);
TextDrawSetOutline(TDEditor_TD[5], 0);
TextDrawBackgroundColor(TDEditor_TD[5], 255);
TextDrawFont(TDEditor_TD[5], 4);
TextDrawSetProportional(TDEditor_TD[5], 0);
TextDrawSetShadow(TDEditor_TD[5], 0);
TextDrawSetSelectable(TDEditor_TD[5], true);

TDEditor_TD[6] = TextDrawCreate(309.333526, 142.970367, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[6], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[6], 64.000000, 18.000000);
TextDrawAlignment(TDEditor_TD[6], 1);
TextDrawColor(TDEditor_TD[6], 168434687);
TextDrawSetShadow(TDEditor_TD[6], 0);
TextDrawSetOutline(TDEditor_TD[6], 0);
TextDrawBackgroundColor(TDEditor_TD[6], 255);
TextDrawFont(TDEditor_TD[6], 4);
TextDrawSetProportional(TDEditor_TD[6], 0);
TextDrawSetShadow(TDEditor_TD[6], 0);
TextDrawSetSelectable(TDEditor_TD[6], true);

TDEditor_TD[7] = TextDrawCreate(375.333557, 142.970397, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[7], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[7], 64.000000, 18.000000);
TextDrawAlignment(TDEditor_TD[7], 1);
TextDrawColor(TDEditor_TD[7], 168434687);
TextDrawSetShadow(TDEditor_TD[7], 0);
TextDrawSetOutline(TDEditor_TD[7], 0);
TextDrawBackgroundColor(TDEditor_TD[7], 255);
TextDrawFont(TDEditor_TD[7], 4);
TextDrawSetProportional(TDEditor_TD[7], 0);
TextDrawSetShadow(TDEditor_TD[7], 0);
TextDrawSetSelectable(TDEditor_TD[7], true);

TDEditor_TD[8] = TextDrawCreate(441.000305, 142.970413, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[8], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[8], 64.000000, 18.000000);
TextDrawAlignment(TDEditor_TD[8], 1);
TextDrawColor(TDEditor_TD[8], 168434687);
TextDrawSetShadow(TDEditor_TD[8], 0);
TextDrawSetOutline(TDEditor_TD[8], 0);
TextDrawBackgroundColor(TDEditor_TD[8], 255);
TextDrawFont(TDEditor_TD[8], 4);
TextDrawSetProportional(TDEditor_TD[8], 0);
TextDrawSetShadow(TDEditor_TD[8], 0);
TextDrawSetSelectable(TDEditor_TD[8], true);

TDEditor_TD[9] = TextDrawCreate(217.000030, 126.533340, "Receptu kategorijos:");
TextDrawLetterSize(TDEditor_TD[9], 0.156332, 0.878220);
TextDrawAlignment(TDEditor_TD[9], 2);
TextDrawColor(TDEditor_TD[9], -1061109505);
TextDrawSetShadow(TDEditor_TD[9], 0);
TextDrawSetOutline(TDEditor_TD[9], 0);
TextDrawBackgroundColor(TDEditor_TD[9], 255);
TextDrawFont(TDEditor_TD[9], 2);
TextDrawSetProportional(TDEditor_TD[9], 1);
TextDrawSetShadow(TDEditor_TD[9], 0);

TDEditor_TD[10] = TextDrawCreate(210.000030, 146.859283, "Ginklai");
TextDrawLetterSize(TDEditor_TD[10], 0.165665, 0.869925);
TextDrawAlignment(TDEditor_TD[10], 2);
TextDrawColor(TDEditor_TD[10], -126);
TextDrawSetShadow(TDEditor_TD[10], 0);
TextDrawSetOutline(TDEditor_TD[10], 0);
TextDrawBackgroundColor(TDEditor_TD[10], 255);
TextDrawFont(TDEditor_TD[10], 2);
TextDrawSetProportional(TDEditor_TD[10], 1);
TextDrawSetShadow(TDEditor_TD[10], 0);

TDEditor_TD[11] = TextDrawCreate(275.666748, 146.444473, "Vaistai");
TextDrawLetterSize(TDEditor_TD[11], 0.165665, 0.869925);
TextDrawAlignment(TDEditor_TD[11], 2);
TextDrawColor(TDEditor_TD[11], -1);
TextDrawSetShadow(TDEditor_TD[11], 0);
TextDrawSetOutline(TDEditor_TD[11], 0);
TextDrawBackgroundColor(TDEditor_TD[11], 255);
TextDrawFont(TDEditor_TD[11], 2);
TextDrawSetProportional(TDEditor_TD[11], 1);
TextDrawSetShadow(TDEditor_TD[11], 0);

TDEditor_TD[12] = TextDrawCreate(339.666687, 146.444473, "Narkotikai");
TextDrawLetterSize(TDEditor_TD[12], 0.165665, 0.869925);
TextDrawAlignment(TDEditor_TD[12], 2);
TextDrawColor(TDEditor_TD[12], -126);
TextDrawSetShadow(TDEditor_TD[12], 0);
TextDrawSetOutline(TDEditor_TD[12], 0);
TextDrawBackgroundColor(TDEditor_TD[12], 255);
TextDrawFont(TDEditor_TD[12], 2);
TextDrawSetProportional(TDEditor_TD[12], 1);
TextDrawSetShadow(TDEditor_TD[12], 0);

TDEditor_TD[13] = TextDrawCreate(406.999938, 146.444458, "Mechanika");
TextDrawLetterSize(TDEditor_TD[13], 0.165665, 0.869925);
TextDrawAlignment(TDEditor_TD[13], 2);
TextDrawColor(TDEditor_TD[13], -126);
TextDrawSetShadow(TDEditor_TD[13], 0);
TextDrawSetOutline(TDEditor_TD[13], 0);
TextDrawBackgroundColor(TDEditor_TD[13], 255);
TextDrawFont(TDEditor_TD[13], 2);
TextDrawSetProportional(TDEditor_TD[13], 1);
TextDrawSetShadow(TDEditor_TD[13], 0);

TDEditor_TD[14] = TextDrawCreate(471.999908, 146.444427, "Baldai");
TextDrawLetterSize(TDEditor_TD[14], 0.165665, 0.869925);
TextDrawAlignment(TDEditor_TD[14], 2);
TextDrawColor(TDEditor_TD[14], -126);
TextDrawSetShadow(TDEditor_TD[14], 0);
TextDrawSetOutline(TDEditor_TD[14], 0);
TextDrawBackgroundColor(TDEditor_TD[14], 255);
TextDrawFont(TDEditor_TD[14], 2);
TextDrawSetProportional(TDEditor_TD[14], 1);
TextDrawSetShadow(TDEditor_TD[14], 0);

TDEditor_TD[15] = TextDrawCreate(178.999984, 167.859252, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[15], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[15], 228.000000, 215.000000);
TextDrawAlignment(TDEditor_TD[15], 1);
TextDrawColor(TDEditor_TD[15], 2625);
TextDrawSetShadow(TDEditor_TD[15], 0);
TextDrawSetOutline(TDEditor_TD[15], 0);
TextDrawBackgroundColor(TDEditor_TD[15], 255);
TextDrawFont(TDEditor_TD[15], 4);
TextDrawSetProportional(TDEditor_TD[15], 0);
TextDrawSetShadow(TDEditor_TD[15], 0);

TDEditor_TD[16] = TextDrawCreate(178.999984, 167.859252, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[16], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[16], 228.000000, 215.000000);
TextDrawAlignment(TDEditor_TD[16], 1);
TextDrawColor(TDEditor_TD[16], -926351606);
TextDrawSetShadow(TDEditor_TD[16], 0);
TextDrawSetOutline(TDEditor_TD[16], 0);
TextDrawBackgroundColor(TDEditor_TD[16], 255);
TextDrawFont(TDEditor_TD[16], 4);
TextDrawSetProportional(TDEditor_TD[16], 0);
TextDrawSetShadow(TDEditor_TD[16], 0);

TDEditor_TD[17] = TextDrawCreate(180.000000, 169.103698, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[17], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[17], 226.000000, 15.000000);
TextDrawAlignment(TDEditor_TD[17], 1);
TextDrawColor(TDEditor_TD[17], 2685);
TextDrawSetShadow(TDEditor_TD[17], 0);
TextDrawSetOutline(TDEditor_TD[17], 0);
TextDrawBackgroundColor(TDEditor_TD[17], 255);
TextDrawFont(TDEditor_TD[17], 4);
TextDrawSetProportional(TDEditor_TD[17], 0);
TextDrawSetShadow(TDEditor_TD[17], 0);
TextDrawSetSelectable(TDEditor_TD[17], true);

TDEditor_TD[18] = TextDrawCreate(179.666656, 184.451812, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[18], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[18], 226.000000, 15.000000);
TextDrawAlignment(TDEditor_TD[18], 1);
TextDrawColor(TDEditor_TD[18], 2685);
TextDrawSetShadow(TDEditor_TD[18], 0);
TextDrawSetOutline(TDEditor_TD[18], 0);
TextDrawBackgroundColor(TDEditor_TD[18], 255);
TextDrawFont(TDEditor_TD[18], 4);
TextDrawSetProportional(TDEditor_TD[18], 0);
TextDrawSetShadow(TDEditor_TD[18], 0);
TextDrawSetSelectable(TDEditor_TD[18], true);

TDEditor_TD[19] = TextDrawCreate(183.666656, 171.592590, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[19], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[19], 10.000000, 10.000000);
TextDrawAlignment(TDEditor_TD[19], 1);
TextDrawColor(TDEditor_TD[19], 2815);
TextDrawSetShadow(TDEditor_TD[19], 0);
TextDrawSetOutline(TDEditor_TD[19], 0);
TextDrawBackgroundColor(TDEditor_TD[19], 255);
TextDrawFont(TDEditor_TD[19], 4);
TextDrawSetProportional(TDEditor_TD[19], 0);
TextDrawSetShadow(TDEditor_TD[19], 0);

TDEditor_TD[20] = TextDrawCreate(184.666641, 172.837020, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[20], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[20], 8.000000, 8.000000);
TextDrawAlignment(TDEditor_TD[20], 1);
TextDrawColor(TDEditor_TD[20], 168435455);
TextDrawSetShadow(TDEditor_TD[20], 0);
TextDrawSetOutline(TDEditor_TD[20], 0);
TextDrawBackgroundColor(TDEditor_TD[20], 255);
TextDrawFont(TDEditor_TD[20], 4);
TextDrawSetProportional(TDEditor_TD[20], 0);
TextDrawSetShadow(TDEditor_TD[20], 0);

TDEditor_TD[21] = TextDrawCreate(184.666519, 170.503707, "+");
TextDrawLetterSize(TDEditor_TD[21], 0.359333, 1.268146);
TextDrawAlignment(TDEditor_TD[21], 1);
TextDrawColor(TDEditor_TD[21], 8388863);
TextDrawSetShadow(TDEditor_TD[21], 0);
TextDrawSetOutline(TDEditor_TD[21], 0);
TextDrawBackgroundColor(TDEditor_TD[21], -1);
TextDrawFont(TDEditor_TD[21], 1);
TextDrawSetProportional(TDEditor_TD[21], 1);
TextDrawSetShadow(TDEditor_TD[21], 0);

TDEditor_TD[22] = TextDrawCreate(409.333465, 224.274078, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[22], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[22], 107.000000, 159.000000);
TextDrawAlignment(TDEditor_TD[22], 1);
TextDrawColor(TDEditor_TD[22], 2625);
TextDrawSetShadow(TDEditor_TD[22], 0);
TextDrawSetOutline(TDEditor_TD[22], 0);
TextDrawBackgroundColor(TDEditor_TD[22], 255);
TextDrawFont(TDEditor_TD[22], 4);
TextDrawSetProportional(TDEditor_TD[22], 0);
TextDrawSetShadow(TDEditor_TD[22], 0);

TDEditor_TD[23] = TextDrawCreate(409.333465, 224.274078, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[23], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[23], 107.000000, 159.000000);
TextDrawAlignment(TDEditor_TD[23], 1);
TextDrawColor(TDEditor_TD[23], 2815);
TextDrawSetShadow(TDEditor_TD[23], 0);
TextDrawSetOutline(TDEditor_TD[23], 0);
TextDrawBackgroundColor(TDEditor_TD[23], 255);
TextDrawFont(TDEditor_TD[23], 4);
TextDrawSetProportional(TDEditor_TD[23], 0);
TextDrawSetShadow(TDEditor_TD[23], 0);

TDEditor_TD[24] = TextDrawCreate(410.999908, 167.444488, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[24], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[24], 26.000000, 26.000000);
TextDrawAlignment(TDEditor_TD[24], 1);
TextDrawColor(TDEditor_TD[24], 252649215);
TextDrawSetShadow(TDEditor_TD[24], 0);
TextDrawSetOutline(TDEditor_TD[24], 0);
TextDrawBackgroundColor(TDEditor_TD[24], 255);
TextDrawFont(TDEditor_TD[24], 4);
TextDrawSetProportional(TDEditor_TD[24], 0);
TextDrawSetShadow(TDEditor_TD[24], 0);

TDEditor_TD[25] = TextDrawCreate(411.333496, 225.103713, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[25], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[25], 105.000000, 24.000000);
TextDrawAlignment(TDEditor_TD[25], 1);
TextDrawColor(TDEditor_TD[25], 336865535);
TextDrawSetShadow(TDEditor_TD[25], 0);
TextDrawSetOutline(TDEditor_TD[25], 0);
TextDrawBackgroundColor(TDEditor_TD[25], 255);
TextDrawFont(TDEditor_TD[25], 4);
TextDrawSetProportional(TDEditor_TD[25], 0);
TextDrawSetShadow(TDEditor_TD[25], 0);

TDEditor_TD[26] = TextDrawCreate(412.666656, 226.763015, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[26], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[26], 21.000000, 21.000000);
TextDrawAlignment(TDEditor_TD[26], 1);
TextDrawColor(TDEditor_TD[26], 252649215);
TextDrawSetShadow(TDEditor_TD[26], 0);
TextDrawSetOutline(TDEditor_TD[26], 0);
TextDrawBackgroundColor(TDEditor_TD[26], 255);
TextDrawFont(TDEditor_TD[26], 4);
TextDrawSetProportional(TDEditor_TD[26], 0);
TextDrawSetShadow(TDEditor_TD[26], 0);

TDEditor_TD[27] = TextDrawCreate(435.333282, 226.762939, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[27], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[27], 80.000000, 21.000000);
TextDrawAlignment(TDEditor_TD[27], 1);
TextDrawColor(TDEditor_TD[27], 168434175);
TextDrawSetShadow(TDEditor_TD[27], 0);
TextDrawSetOutline(TDEditor_TD[27], 0);
TextDrawBackgroundColor(TDEditor_TD[27], 255);
TextDrawFont(TDEditor_TD[27], 4);
TextDrawSetProportional(TDEditor_TD[27], 0);
TextDrawSetShadow(TDEditor_TD[27], 0);

TDEditor_TD[28] = TextDrawCreate(411.000152, 249.992492, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[28], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[28], 105.000000, 24.000000);
TextDrawAlignment(TDEditor_TD[28], 1);
TextDrawColor(TDEditor_TD[28], 336865535);
TextDrawSetShadow(TDEditor_TD[28], 0);
TextDrawSetOutline(TDEditor_TD[28], 0);
TextDrawBackgroundColor(TDEditor_TD[28], 255);
TextDrawFont(TDEditor_TD[28], 4);
TextDrawSetProportional(TDEditor_TD[28], 0);
TextDrawSetShadow(TDEditor_TD[28], 0);

TDEditor_TD[29] = TextDrawCreate(179.000076, 385.636901, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[29], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[29], 73.000000, 13.000000);
TextDrawAlignment(TDEditor_TD[29], 1);
TextDrawColor(TDEditor_TD[29], 336865535);
TextDrawSetShadow(TDEditor_TD[29], 0);
TextDrawSetOutline(TDEditor_TD[29], 0);
TextDrawBackgroundColor(TDEditor_TD[29], 255);
TextDrawFont(TDEditor_TD[29], 4);
TextDrawSetProportional(TDEditor_TD[29], 0);
TextDrawSetShadow(TDEditor_TD[29], 0);
TextDrawSetSelectable(TDEditor_TD[29], true);

TDEditor_TD[30] = TextDrawCreate(349.333496, 385.222045, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[30], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[30], 58.000000, 13.000000);
TextDrawAlignment(TDEditor_TD[30], 1);
TextDrawColor(TDEditor_TD[30], 336865535);
TextDrawSetShadow(TDEditor_TD[30], 0);
TextDrawSetOutline(TDEditor_TD[30], 0);
TextDrawBackgroundColor(TDEditor_TD[30], 255);
TextDrawFont(TDEditor_TD[30], 4);
TextDrawSetProportional(TDEditor_TD[30], 0);
TextDrawSetShadow(TDEditor_TD[30], 0);
TextDrawSetSelectable(TDEditor_TD[30], true);

TDEditor_TD[31] = TextDrawCreate(378.666412, 387.451812, "Gaminti visus");
TextDrawLetterSize(TDEditor_TD[31], 0.133665, 0.911406);
TextDrawAlignment(TDEditor_TD[31], 2);
TextDrawColor(TDEditor_TD[31], -926373121);
TextDrawSetShadow(TDEditor_TD[31], 0);
TextDrawSetOutline(TDEditor_TD[31], 0);
TextDrawBackgroundColor(TDEditor_TD[31], 255);
TextDrawFont(TDEditor_TD[31], 2);
TextDrawSetProportional(TDEditor_TD[31], 1);
TextDrawSetShadow(TDEditor_TD[31], 0);

TDEditor_TD[32] = TextDrawCreate(411.000213, 385.221984, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[32], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[32], 58.000000, 13.000000);
TextDrawAlignment(TDEditor_TD[32], 1);
TextDrawColor(TDEditor_TD[32], 336865535);
TextDrawSetShadow(TDEditor_TD[32], 0);
TextDrawSetOutline(TDEditor_TD[32], 0);
TextDrawBackgroundColor(TDEditor_TD[32], 255);
TextDrawFont(TDEditor_TD[32], 4);
TextDrawSetProportional(TDEditor_TD[32], 0);
TextDrawSetShadow(TDEditor_TD[32], 0);
TextDrawSetSelectable(TDEditor_TD[32], true);

TDEditor_TD[33] = TextDrawCreate(411.666717, 386.051849, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[33], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[33], 11.000000, 11.000000);
TextDrawAlignment(TDEditor_TD[33], 1);
TextDrawColor(TDEditor_TD[33], 84219135);
TextDrawSetShadow(TDEditor_TD[33], 0);
TextDrawSetOutline(TDEditor_TD[33], 0);
TextDrawBackgroundColor(TDEditor_TD[33], 255);
TextDrawFont(TDEditor_TD[33], 4);
TextDrawSetProportional(TDEditor_TD[33], 0);
TextDrawSetShadow(TDEditor_TD[33], 0);
TextDrawSetSelectable(TDEditor_TD[33], true);

TDEditor_TD[34] = TextDrawCreate(457.333618, 386.051849, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[34], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[34], 11.000000, 11.000000);
TextDrawAlignment(TDEditor_TD[34], 1);
TextDrawColor(TDEditor_TD[34], 84219135);
TextDrawSetShadow(TDEditor_TD[34], 0);
TextDrawSetOutline(TDEditor_TD[34], 0);
TextDrawBackgroundColor(TDEditor_TD[34], 255);
TextDrawFont(TDEditor_TD[34], 4);
TextDrawSetProportional(TDEditor_TD[34], 0);
TextDrawSetShadow(TDEditor_TD[34], 0);
TextDrawSetSelectable(TDEditor_TD[34], true);

TDEditor_TD[35] = TextDrawCreate(413.666656, 385.377838, "-");
TextDrawLetterSize(TDEditor_TD[35], 0.441998, 1.131260);
TextDrawAlignment(TDEditor_TD[35], 1);
TextDrawColor(TDEditor_TD[35], -1);
TextDrawSetShadow(TDEditor_TD[35], 0);
TextDrawSetOutline(TDEditor_TD[35], 0);
TextDrawBackgroundColor(TDEditor_TD[35], 255);
TextDrawFont(TDEditor_TD[35], 1);
TextDrawSetProportional(TDEditor_TD[35], 1);
TextDrawSetShadow(TDEditor_TD[35], 0);

TDEditor_TD[36] = TextDrawCreate(459.666748, 385.377838, "+");
TextDrawLetterSize(TDEditor_TD[36], 0.324665, 1.297186);
TextDrawAlignment(TDEditor_TD[36], 1);
TextDrawColor(TDEditor_TD[36], -1);
TextDrawSetShadow(TDEditor_TD[36], 0);
TextDrawSetOutline(TDEditor_TD[36], 0);
TextDrawBackgroundColor(TDEditor_TD[36], 255);
TextDrawFont(TDEditor_TD[36], 1);
TextDrawSetProportional(TDEditor_TD[36], 1);
TextDrawSetShadow(TDEditor_TD[36], 0);

TDEditor_TD[37] = TextDrawCreate(423.666961, 386.051879, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[37], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[37], 33.000000, 11.000000);
TextDrawAlignment(TDEditor_TD[37], 1);
TextDrawColor(TDEditor_TD[37], 84219135);
TextDrawSetShadow(TDEditor_TD[37], 0);
TextDrawSetOutline(TDEditor_TD[37], 0);
TextDrawBackgroundColor(TDEditor_TD[37], 255);
TextDrawFont(TDEditor_TD[37], 4);
TextDrawSetProportional(TDEditor_TD[37], 0);
TextDrawSetShadow(TDEditor_TD[37], 0);
TextDrawSetSelectable(TDEditor_TD[37], true);

TDEditor_TD[38] = TextDrawCreate(470.667175, 385.222045, "LD_SPAC:white");
TextDrawLetterSize(TDEditor_TD[38], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[38], 46.000000, 13.000000);
TextDrawAlignment(TDEditor_TD[38], 1);
TextDrawColor(TDEditor_TD[38], 336865535);
TextDrawSetShadow(TDEditor_TD[38], 0);
TextDrawSetOutline(TDEditor_TD[38], 0);
TextDrawBackgroundColor(TDEditor_TD[38], 255);
TextDrawFont(TDEditor_TD[38], 4);
TextDrawSetProportional(TDEditor_TD[38], 0);
TextDrawSetShadow(TDEditor_TD[38], 0);
TextDrawSetSelectable(TDEditor_TD[38], true);

TDEditor_TD[39] = TextDrawCreate(493.666473, 387.451873, "Gaminti");
TextDrawLetterSize(TDEditor_TD[39], 0.133665, 0.911406);
TextDrawAlignment(TDEditor_TD[39], 2);
TextDrawColor(TDEditor_TD[39], -926373121);
TextDrawSetShadow(TDEditor_TD[39], 0);
TextDrawSetOutline(TDEditor_TD[39], 0);
TextDrawBackgroundColor(TDEditor_TD[39], 255);
TextDrawFont(TDEditor_TD[39], 2);
TextDrawSetProportional(TDEditor_TD[39], 1);
TextDrawSetShadow(TDEditor_TD[39], 0);

TDEditor_TD[40] = TextDrawCreate(278.000213, 387.296203, "LD_BEAT:left");
TextDrawLetterSize(TDEditor_TD[40], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[40], 10.000000, 10.000000);
TextDrawAlignment(TDEditor_TD[40], 1);
TextDrawColor(TDEditor_TD[40], -1);
TextDrawSetShadow(TDEditor_TD[40], 0);
TextDrawSetOutline(TDEditor_TD[40], 0);
TextDrawBackgroundColor(TDEditor_TD[40], 255);
TextDrawFont(TDEditor_TD[40], 4);
TextDrawSetProportional(TDEditor_TD[40], 0);
TextDrawSetShadow(TDEditor_TD[40], 0);
TextDrawSetSelectable(TDEditor_TD[40], true);

TDEditor_TD[41] = TextDrawCreate(311.000305, 386.881378, "LD_BEAT:right");
TextDrawLetterSize(TDEditor_TD[41], 0.000000, 0.000000);
TextDrawTextSize(TDEditor_TD[41], 10.000000, 10.000000);
TextDrawAlignment(TDEditor_TD[41], 1);
TextDrawColor(TDEditor_TD[41], -1);
TextDrawSetShadow(TDEditor_TD[41], 0);
TextDrawSetOutline(TDEditor_TD[41], 0);
TextDrawBackgroundColor(TDEditor_TD[41], 255);
TextDrawFont(TDEditor_TD[41], 4);
TextDrawSetProportional(TDEditor_TD[41], 0);
TextDrawSetShadow(TDEditor_TD[41], 0);
TextDrawSetSelectable(TDEditor_TD[41], true);



//Player TextDraws: 


new PlayerText:TDEditor_PTD[MAX_PLAYERS][13];

TDEditor_PTD[playerid][0] = CreatePlayerTextDraw(playerid, 218.333282, 171.748168, "Morfinas");
PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][0], 0.156332, 0.869925);
PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][0], 1);
PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][0], -926373121);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][0], 0);
PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][0], 0);
PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][0], 255);
PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][0], 2);
PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][0], 1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][0], 0);

TDEditor_PTD[playerid][1] = CreatePlayerTextDraw(playerid, 378.332824, 171.748168, "18");
PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][1], 0.156332, 0.869925);
PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][1], 3);
PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][1], -926373121);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][1], 0);
PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][1], 0);
PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][1], 255);
PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][1], 2);
PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][1], 1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][1], 0);

TDEditor_PTD[playerid][2] = CreatePlayerTextDraw(playerid, 443.333312, 166.770339, "Morfinas");
PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][2], 0.177999, 0.919703);
PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][2], 1);
PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][2], -5963521);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][2], 0);
PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][2], 0);
PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][2], 255);
PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][2], 2);
PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][2], 1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][2], 0);

TDEditor_PTD[playerid][3] = CreatePlayerTextDraw(playerid, 407.333343, 159.977783, "");
PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][3], 0.000000, 0.000000);
PlayerTextDrawTextSize(playerid, TDEditor_PTD[playerid][3], 32.000000, 32.000000);
PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][3], 1);
PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][3], -1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][3], 0);
PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][3], 0);
PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][3], 0);
PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][3], 5);
PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][3], 0);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][3], 0);
PlayerTextDrawSetPreviewModel(playerid, TDEditor_PTD[playerid][3], 1575);
PlayerTextDrawSetPreviewRot(playerid, TDEditor_PTD[playerid][3], 152.000000, 172.000000, 130.000000, 0.974973);

TDEditor_PTD[playerid][4] = CreatePlayerTextDraw(playerid, 442.999908, 182.533309, "~y~10~w~~h~~h~g.");
PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][4], 0.202996, 1.006814);
PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][4], 1);
PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][4], -1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][4], 0);
PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][4], 0);
PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][4], 255);
PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][4], 2);
PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][4], 1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][4], 0);

TDEditor_PTD[playerid][5] = CreatePlayerTextDraw(playerid, 413.999786, 197.466598, "Vaistas skirtas skausmui numalsinti. Labai stipraus poveikio.");
PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][5], 0.160330, 0.903110);
PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][5], 1);
PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][5], -7681);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][5], 0);
PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][5], 0);
PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][5], 255);
PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][5], 2);
PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][5], 1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][5], 0);

TDEditor_PTD[playerid][6] = CreatePlayerTextDraw(playerid, 408.999877, 221.785247, "");
PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][6], 0.000000, 0.000000);
PlayerTextDrawTextSize(playerid, TDEditor_PTD[playerid][6], 27.000000, 27.000000);
PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][6], 1);
PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][6], -1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][6], 0);
PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][6], 0);
PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][6], 0);
PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][6], 5);
PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][6], 0);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][6], 0);
PlayerTextDrawSetPreviewModel(playerid, TDEditor_PTD[playerid][6], 1575);
PlayerTextDrawSetPreviewRot(playerid, TDEditor_PTD[playerid][6], 152.000000, 172.000000, 130.000000, 0.974973);

TDEditor_PTD[playerid][7] = CreatePlayerTextDraw(playerid, 438.666625, 226.918518, "Chem. m. NZ-578");
PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][7], 0.134999, 0.840888);
PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][7], 1);
PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][7], -1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][7], 0);
PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][7], 0);
PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][7], 255);
PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][7], 2);
PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][7], 1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][7], 0);

TDEditor_PTD[playerid][8] = CreatePlayerTextDraw(playerid, 510.666290, 237.703582, "~y~5~w~~h~~h~g./~y~216~w~~h~~h~g.");
PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][8], 0.134999, 0.840888);
PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][8], 3);
PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][8], -1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][8], 0);
PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][8], 0);
PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][8], 255);
PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][8], 2);
PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][8], 1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][8], 0);

TDEditor_PTD[playerid][9] = CreatePlayerTextDraw(playerid, 215.666656, 387.451843, "Rodyti tik galimus");
PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][9], 0.133665, 0.911406);
PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][9], 2);
PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][9], -926373121);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][9], 0);
PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][9], 0);
PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][9], 255);
PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][9], 2);
PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][9], 1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][9], 0);

TDEditor_PTD[playerid][10] = CreatePlayerTextDraw(playerid, 439.333343, 386.207397, "15");
PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][10], 0.169999, 0.998517);
PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][10], 2);
PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][10], -1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][10], 0);
PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][10], 0);
PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][10], 255);
PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][10], 2);
PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][10], 1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][10], 0);

TDEditor_PTD[playerid][11] = CreatePlayerTextDraw(playerid, 300.333465, 386.622222, "15");
PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][11], 0.169999, 0.998517);
PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][11], 2);
PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][11], -1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][11], 0);
PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][11], 0);
PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][11], 255);
PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][11], 2);
PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][11], 1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][11], 0);

TDEditor_PTD[playerid][12] = CreatePlayerTextDraw(playerid, 512.000122, 126.533325, "~w~Vaistu gaminimo lygis: ~y~16~w~, patirtis: ~y~24 321");
PlayerTextDrawLetterSize(playerid, TDEditor_PTD[playerid][12], 0.156332, 0.878220);
PlayerTextDrawAlignment(playerid, TDEditor_PTD[playerid][12], 3);
PlayerTextDrawColor(playerid, TDEditor_PTD[playerid][12], -1061109505);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][12], 0);
PlayerTextDrawSetOutline(playerid, TDEditor_PTD[playerid][12], 0);
PlayerTextDrawBackgroundColor(playerid, TDEditor_PTD[playerid][12], 255);
PlayerTextDrawFont(playerid, TDEditor_PTD[playerid][12], 2);
PlayerTextDrawSetProportional(playerid, TDEditor_PTD[playerid][12], 1);
PlayerTextDrawSetShadow(playerid, TDEditor_PTD[playerid][12], 0);




//Total textdraws exported: 55 (42 global textdraws / 13 player textdraws) ~ 16/12/2016 ~ 1:35:12
TDEditor V1.17 BY ADRI1