#pragma warning disable 213

#include <a_samp>

// #define _DEBUG 5
#define RUN_TESTS

#include "../include/vehicleutil.inc"

#include <crashdetect>

#include <YSI\y_testing>
#include <YSI\y_va>
#include <YSI\y_iterate>
#include <YSI\y_inline>
#include <YSI\y_timers>
#include <YSI\y_commands>
#include <YSI\y_dialog>


#include <noclass>
#include <streamer>
#include <colours>
#include <attachments-fix>
#include <formatex>
// #include <CTime>
#include <strlib>
#include <djson>
#include <whirlpool>
#include <sscanf2>
#include <timestampToDate>
#include <colandreas>
#include <extended_vehicle_functions>
#include <mapandreas>
#include <PathFinder>
// #include <profiler>
// #include <rnpc>
#include <YSF>
#include <SKY>
#include <FCNPC>
// #include <RAKTOOLS>
#include <items_and_containers>

#include "..\gamemodes\lib\hooks.inc"
#include "..\gamemodes\lib\macros.inc"
#include "..\gamemodes\lib\samp.inc"
#include "..\gamemodes\lib\util.inc"
#include "..\gamemodes\lib\positions.inc"
#include "..\gamemodes\lib\events-definitions.inc"

#include "..\gamemodes\modules\server\config.pwn"
#include "..\gamemodes\modules\server\database.pwn"

#include <weapon-config>

#include "..\gamemodes\modules\game\jobs\data.pwn"
#include "..\gamemodes\modules\player\data.pwn"
#include "..\gamemodes\modules\game\vehicles\data.pwn"

#include "..\gamemodes\modules\game\managers\entrance-manager.pwn"
#include "..\gamemodes\modules\game\managers\fade-manager.pwn"
#include "..\gamemodes\modules\game\managers\command-manager.pwn"
#include "..\gamemodes\modules\game\managers\permission-manager.pwn"

// npcs
#include "..\gamemodes\modules\game\npcs\base.pwn"
#include "..\gamemodes\modules\game\npcs\damage-handler.pwn"
#include "..\gamemodes\modules\game\npcs\guardians.pwn"

#include "..\gamemodes\modules\game\places\snake-farm.pwn"

#include "..\gamemodes\modules\player\background-audio.pwn"
#include "..\gamemodes\modules\player\auth.pwn"
#include "..\gamemodes\modules\player\character-selection.pwn"
#include "..\gamemodes\modules\player\gamestart.pwn"
#include "..\gamemodes\modules\player\general.pwn"
#include "..\gamemodes\modules\player\tutorial.pwn"
#include "..\gamemodes\modules\player\experience.pwn"
#include "..\gamemodes\modules\player\notifications.pwn"
#include "..\gamemodes\modules\player\targetmenu.pwn"
#include "..\gamemodes\modules\player\ui.pwn" // user interface
#include "..\gamemodes\modules\player\ui\level_up.pwn"
#include "..\gamemodes\modules\player\ui\inventory_crafting_container.pwn"
#include "..\gamemodes\modules\player\ui\player_health.pwn"
#include "..\gamemodes\modules\player\ui\enemy_health.pwn"
#include "..\gamemodes\modules\player\ui\cash_update.pwn"
#include "..\gamemodes\modules\player\ui\y_menu.pwn"
#include "..\gamemodes\modules\player\ui\progress.pwn"
#include "..\gamemodes\modules\player\ui\character_selection.pwn"

#include "..\gamemodes\modules\game\items\general.pwn"

// locations and positions
// #include "..\gamemodes\modules\game\locations_and_positions\business.pwn"

#include "..\gamemodes\modules\game\vehicles\shop.pwn"
#include "..\gamemodes\modules\game\vehicles\special.pwn"
#include "..\gamemodes\modules\game\vehicles\utils.pwn"
#include "..\gamemodes\modules\game\vehicles\errors_and_upgrades.pwn"
#include "..\gamemodes\modules\game\vehicles\speedometer.pwn"
#include "..\gamemodes\modules\game\vehicles\camera_target.pwn"
#include "..\gamemodes\modules\game\vehicles\garage.pwn"
#include "..\gamemodes\modules\game\vehicles\player.pwn"
#include "..\gamemodes\modules\game\vehicles\car_lot.pwn"

#include "..\gamemodes\modules\game\houses\general.pwn"
#include "..\gamemodes\modules\game\houses\permissions.pwn"
#include "..\gamemodes\modules\game\houses\ownership.pwn"
#include "..\gamemodes\modules\game\houses\interiors.pwn"
#include "..\gamemodes\modules\game\houses\management.pwn"

// Verslai
#include "..\gamemodes\modules\game\businesses\data.pwn"
#include "..\gamemodes\modules\game\businesses\pickups.pwn"
#include "..\gamemodes\modules\game\businesses\labels.pwn"
#include "..\gamemodes\modules\game\businesses\areas.pwn"
#include "..\gamemodes\modules\game\businesses\textdraws.pwn"

// quests
#include "..\gamemodes\modules\game\quests\general.pwn"

#include "..\gamemodes\modules\game\quests\another\data.pwn"
#include "..\gamemodes\modules\game\quests\another\storyline.pwn"

// darbai
#include "..\gamemodes\modules\game\jobs\leader_menu.pwn"
#include "..\gamemodes\modules\game\jobs\management.pwn"
#include "..\gamemodes\modules\game\jobs\general.pwn"

#include "..\gamemodes\modules\game\jobs\taxi.pwn"

#include "..\gamemodes\modules\game\jobs\medics.pwn"
#include "..\gamemodes\modules\game\jobs\medics\hospital\inventory-control.pwn"

#include "..\gamemodes\modules\game\jobs\mechanics.pwn"

#include "..\gamemodes\modules\game\jobs\firefighters.pwn"
#include "..\gamemodes\modules\game\jobs\firefighters\fire.pwn"

#include "..\gamemodes\modules\game\jobs\police.pwn"
#include "..\gamemodes\modules\game\jobs\police\commands.pwn"
#include "..\gamemodes\modules\game\jobs\police\jail.pwn"
#include "..\gamemodes\modules\game\jobs\police\crime_bullets.pwn"
#include "..\gamemodes\modules\game\jobs\police\manage_passengers.pwn"
#include "..\gamemodes\modules\game\jobs\police\manage_reasons.pwn"
#include "..\gamemodes\modules\game\jobs\police\target_menu.pwn"

#include "..\gamemodes\modules\game\gangs\player-data.pwn"
#include "..\gamemodes\modules\game\gangs\general.pwn"
#include "..\gamemodes\modules\game\gangs\commands.pwn"
#include "..\gamemodes\modules\game\gangs\gang-wars.pwn"
#include "..\gamemodes\modules\game\gangs\territories\general.pwn"
#include "..\gamemodes\modules\game\gangs\menu\create_gang.pwn"
#include "..\gamemodes\modules\game\gangs\menu\leader_menu.pwn"
#include "..\gamemodes\modules\game\gangs\menu\member_menu.pwn"

#include "..\gamemodes\modules\game\containers\general.pwn"
#include "..\gamemodes\modules\game\inventory\equipment.pwn"
#include "..\gamemodes\modules\game\items\weapons\general.pwn"
#include "..\gamemodes\modules\game\items\weapons\equipment.pwn"
#include "..\gamemodes\modules\game\inventory\ui.pwn"
#include "..\gamemodes\modules\game\inventory\general.pwn"

#include "..\gamemodes\modules\game\crafting\ui.pwn"

// u�si�mimai
#include "..\gamemodes\modules\game\activities\fishing.pwn"
#include "..\gamemodes\modules\game\activities\mining.pwn"

// apmokami
#include "..\gamemodes\modules\game\activities\paid\general.pwn"
#include "..\gamemodes\modules\game\activities\paid\pizza.pwn"

// testai
#include "..\gamemodes\modules\debug\tests.pwn"

#include "..\gamemodes\maps\MAP_Spawn.pwn"
#include "..\gamemodes\maps\MAP_Turgaviete.pwn"
#include "..\gamemodes\maps\MAP_Uostas.pwn"
#include "..\gamemodes\maps\MAP_Garazas.pwn"
#include "..\gamemodes\maps\MAP_Parduotuve.pwn"
#include "..\gamemodes\maps\MAP_Automobiliu-pardavimo-aikstele.pwn"
// #include "..\gamemodes\maps\MAP_VM.pwn" https://goo.gl\s2aEuj - trello card'as
// #include "..\gamemodes\maps\MAP_Restoranas.pwn" https://goo.gl\KRI1PS - trello 
                                      
// darb� map
#include "..\gamemodes\maps\MAP_Ligonine.pwn"
#include "..\gamemodes\maps\MAP_Ligonine-vidus.pwn"
#include "..\gamemodes\maps\MAP_Policija-vidus.pwn"
#include "..\gamemodes\maps\MAP_Taxi.pwn"
#include "..\gamemodes\maps\MAP_Furistai.pwn"
#include "..\gamemodes\maps\MAP_Gaisrine.pwn"
#include "..\gamemodes\maps\MAP_Valytojai.pwn"

#include "..\gamemodes\maps\MAP_Kasykla1.pwn"
#include "..\gamemodes\maps\MAP_Kasykla2.pwn"
#include "..\gamemodes\maps\MAP_Naftotiekis.pwn"
#include "..\gamemodes\maps\MAP_Savartynas.pwn"

#include "..\gamemodes\maps\MAP_Gamykla-baldai.pwn"
#include "..\gamemodes\maps\MAP_Gamykla-baldai-vidus.pwn"

#include "..\gamemodes\maps\MAP_House_1f_medium.pwn"

#include "..\gamemodes\maps\MAP_Gang1.pwn"
#include "..\gamemodes\maps\MAP_Gang2.pwn"
#include "..\gamemodes\maps\MAP_Gang3.pwn"

#include "..\gamemodes\modules\debug\admin.pwn"
#include "..\gamemodes\modules\debug\general.pwn"
#include "..\gamemodes\modules\debug\ninja.pwn"
#include "..\gamemodes\modules\debug\vehicle.pwn"
#include "..\gamemodes\modules\debug\npc.pwn"


/*public OnCheatDetected(playerid, ip_address[], type, code) {
	M:P:E(playerid, "Serveris pasteb�jo neleistin� veikl�, saugumo sumetimais esi i�metamas i� serverio.");
	M:P:I(playerid, "Prie�astis: %s", ACReason[code]);

	defer KickPlayer(playerid);

	return false;
}*/

hook OnScriptInit() {
	CA_Init();
}

hook OnGameModeInit() {
	DisableInteriorEnterExits();
	EnableStuntBonusForAll(false);
	SetWeather(24);
	SetGameModeText("GameMode 6");

	// YSF
	AllowNickNameCharacters(true, '�',
		'�','�','�','�','�','�','�','�','�',
		'�','�','�','�','�','�','�','�','�',
		':'
	);

	// SKY
	SetVehiclePassengerDamage(true);
	SetCustomFallDamage(true, 640.0, -0.75);
	SetDisableSyncBugs(true);
}

main() {
}

public OnGameModeExit() {
	//Profiler_Stop();

	return true;
}

// CMD:w(playerid, weather[]) {
// 	
// 		� 24 - Laba �viesu
// 		� 91 - labai tamsu
// 	
// 	SetWeather(strval(weather));
// 	return true;
// }

public OnPlayerRequestClass(playerid, classid) return 1;
public OnPlayerConnect(playerid) return 1;
public OnPlayerDisconnect(playerid, reason) return 1;
public OnPlayerSpawn(playerid) return 1;
public OnVehicleSpawn(vehicleid) return 1;
public OnVehicleDeath(vehicleid, killerid) return 1;
public OnPlayerText(playerid, text[]) return 1;
public OnPlayerCommandText(playerid, cmdtext[]) return 0;
public OnPlayerEnterVehicle(playerid, vehicleid, ispassenger) return 1;
public OnPlayerExitVehicle(playerid, vehicleid) return 1;
public OnPlayerStateChange(playerid, newstate, oldstate) return 1;
public OnPlayerEnterCheckpoint(playerid) return 1;
public OnPlayerLeaveCheckpoint(playerid) return 1;
public OnPlayerEnterRaceCheckpoint(playerid) return 1;
public OnPlayerLeaveRaceCheckpoint(playerid) return 1;
public OnRconCommand(cmd[]) return true;
public OnPlayerRequestSpawn(playerid) return 1;
public OnObjectMoved(objectid) return 1;
public OnPlayerObjectMoved(playerid, objectid) return 1;
public OnPlayerPickUpPickup(playerid, pickupid) return 1;
public OnVehicleMod(playerid, vehicleid, componentid) return 1;
public OnVehiclePaintjob(playerid, vehicleid, paintjobid) return 1;
public OnVehicleRespray(playerid, vehicleid, color1, color2) return 1;
public OnPlayerSelectedMenuRow(playerid, row) return 1;
public OnPlayerExitedMenu(playerid) return 1;
public OnPlayerInteriorChange(playerid, newinteriorid, oldinteriorid) return 1;
public OnPlayerKeyStateChange(playerid, newkeys, oldkeys) return 1;
public OnRconLoginAttempt(ip[], password[], success) return 1;
public OnPlayerUpdate(playerid) return true;
public OnPlayerStreamIn(playerid, forplayerid) return 1;
public OnPlayerStreamOut(playerid, forplayerid) return 1;
public OnVehicleStreamIn(vehicleid, forplayerid) return 1;
public OnVehicleStreamOut(vehicleid, forplayerid) return 1;
public OnDialogResponse(playerid, dialogid, response, listitem, inputtext[]) return 1;
public OnPlayerClickPlayer(playerid, clickedplayerid, source) return 1;

#pragma unused ContainerRow_ItemName_PTD
#pragma unused ContainerRow_TD
#pragma unused CraftingResult_ItemName_PTD
#pragma unused CraftingRow_ItemName_PTD
#pragma unused CraftingSuccessRate_PTD
#pragma unused GetItemDefaultPrice
#pragma unused GetMaxWarsPerGang
#pragma unused GetPlayerMiningLevel
#pragma unused HasItemEquiped
#pragma unused HideContainer
#pragma unused HideCrafting
#pragma unused SetItemDefaultPrice
#pragma unused SetItemDefaultType
#pragma unused SetItemDefaultWeight
#pragma unused ShowContainer
#pragma unused ShowCrafting
#pragma unused job_IsVehicleAllowed
#pragma unused job_RemoveVehicle
#pragma unused SetPlayerJobExp
#pragma unused player_SqlIdToPlayerID
#pragma unused police_WantedByType
#pragma unused vehicle_DamageStatus
#pragma unused vehicle_GamePrice
#pragma unused vehicle_GetParam
#pragma unused vehicle_RealPrice
#pragma unused vehicle_TrunkSize
#pragma unused vehicle_Weight
#pragma unused SetEntranceText
