#include <YSI\y_hooks>

#define vartai1_pos 86.57730, -223.46629, 1.98900
#define vartai1_rot_open 0.00000, 0.0, -180.00000
#define vartai1_rot_closed 0.00000, 90.0, -180.00000
#define vartai1_pos_full vartai1_pos, vartai1_rot_closed

#define vartai2_pos 68.68340, -222.83189, 1.98900
#define vartai2_rot_open 0.00000, 0.0, 0.00000
#define vartai2_rot_closed 0.00000, 90.0, 0.00000
#define vartai2_pos_full vartai2_pos, vartai2_rot_closed

#define vartai3_pos 197.7316, -317.0508, 1.2263
#define vartai3_rot_open 0.00000, 0.0, 96.00000
#define vartai3_rot_closed 0.00000, -90.0, 96.00000
#define vartai3_pos_full vartai3_pos, vartai3_rot_closed

static const CLOSED = 0;
static const OPEN = 1;

static vartai1, vartai2, vartai3;

hook OnPlayerKeyStateChange(playerid, newkeys, oldkeys) {
	if(IsPlayerInAnyVehicle(playerid)) {
		if(PRESSED(KEY_FIRE)) {
			new Float:dist1, Float:dist2, Float:dist3, Float:x, Float:y, Float:z;

			GetPlayerPos(playerid, x, y, z);

			Streamer_GetDistanceToItem(x, y, z, STREAMER_TYPE_OBJECT, vartai1, dist1);
			Streamer_GetDistanceToItem(x, y, z, STREAMER_TYPE_OBJECT, vartai2, dist2);
			Streamer_GetDistanceToItem(x, y, z, STREAMER_TYPE_OBJECT, vartai3, dist3);

			if(dist1 < dist2 && dist1 < 10.0) {
				if( ! IsDynamicObjectMoving(vartai1)) {
					if(Streamer_GetIntData(STREAMER_TYPE_OBJECT, vartai1, E_STREAMER_EXTRA_ID)) {
						MoveDynamicObject(vartai1, vartai1_pos, 0.02, vartai1_rot_closed);
						Streamer_SetIntData(STREAMER_TYPE_OBJECT, vartai1, E_STREAMER_EXTRA_ID, CLOSED);

						GameTextForPlayer(playerid, "Uzdaroma", 2000, 3);
					}
					else {
						MoveDynamicObject(vartai1, vartai1_pos + 0.05, 0.02, vartai1_rot_open);
						Streamer_SetIntData(STREAMER_TYPE_OBJECT, vartai1, E_STREAMER_EXTRA_ID, OPEN);

						GameTextForPlayer(playerid, "Atidaroma", 2000, 3);
					}
				}
			}
			else if(dist2 < 10.0) {
				if( ! IsDynamicObjectMoving(vartai2)) {
					if(Streamer_GetIntData(STREAMER_TYPE_OBJECT, vartai2, E_STREAMER_EXTRA_ID)) {
						MoveDynamicObject(vartai2, vartai2_pos, 0.02, vartai2_rot_closed);
						Streamer_SetIntData(STREAMER_TYPE_OBJECT, vartai2, E_STREAMER_EXTRA_ID, CLOSED);

						GameTextForPlayer(playerid, "Uzdaroma", 2000, 3);
					}
					else {
						MoveDynamicObject(vartai2, vartai2_pos + 0.05, 0.02, vartai2_rot_open);
						Streamer_SetIntData(STREAMER_TYPE_OBJECT, vartai2, E_STREAMER_EXTRA_ID, OPEN);

						GameTextForPlayer(playerid, "Atidaroma", 2000, 3);
					}
				}
			}
			else if(dist3 < 10.0) {
				if( ! IsDynamicObjectMoving(vartai3)) {
					if(Streamer_GetIntData(STREAMER_TYPE_OBJECT, vartai3, E_STREAMER_EXTRA_ID)) {
						MoveDynamicObject(vartai3, vartai3_pos, 0.02, vartai3_rot_closed);
						Streamer_SetIntData(STREAMER_TYPE_OBJECT, vartai3, E_STREAMER_EXTRA_ID, CLOSED);

						GameTextForPlayer(playerid, "Uzdaroma", 2000, 3);
					}
					else {
						MoveDynamicObject(vartai3, vartai3_pos + 0.05, 0.02, vartai3_rot_open);
						Streamer_SetIntData(STREAMER_TYPE_OBJECT, vartai3, E_STREAMER_EXTRA_ID, OPEN);

						GameTextForPlayer(playerid, "Atidaroma", 2000, 3);
					}
				}
			}
		}
	}
}

hook OnGameModeInit() {
	vartai1 = CreateDynamicObject(968, vartai1_pos_full);
	vartai2 = CreateDynamicObject(968, vartai2_pos_full);
	vartai3 = CreateDynamicObject(968, vartai3_pos_full);

	CreateDynamicObject(1438, 160.37163, -305.74310, 0.55868,   3.14159, 0.00000, 1.67610);
	CreateDynamicObject(1440, 79.50009, -329.47375, 1.08542,   356.85840, 0.00000, 90.00000);
	CreateDynamicObject(1438, 58.05225, -243.43915, 0.55905,   356.85840, 0.00000, 180.00000);
	CreateDynamicObject(1431, 107.81128, -241.17371, 1.11869,   356.85840, 0.00000, 90.00000);
	CreateDynamicObject(1426, 47.65000, -306.15356, 1.01079,   356.85840, 0.00000, 0.00000);
	CreateDynamicObject(3627, 123.19936, -278.93524, 3.34200,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3624, 34.03430, -325.02322, 5.36930,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(3626, 92.73390, -224.74290, 2.00000,   0.00000, 0.00000, -1.57080);
	CreateDynamicObject(3569, 170.08279, -280.66226, 1.57319,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(5131, 138.58307, -267.65631, 6.91370,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(5129, 143.25450, -268.12329, 5.58740,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(3627, 146.48840, -335.32089, 4.39360,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(7496, 87.05410, -328.76331, 4.07890,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(8058, 39.56427, -274.77625, 5.43284,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(5268, 148.66820, -265.80020, 8.53550,   0.00000, 0.00000, -135.03400);
	CreateDynamicObject(5259, 145.12900, -298.92828, 2.29206,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(5132, 145.69493, -291.46060, 2.38587,   0.00000, 0.00000, 270.00000);
	CreateDynamicObject(3577, 155.82629, -269.46759, 1.26091,   0.00000, 0.00000, 270.00000);
	CreateDynamicObject(3577, 138.04422, -257.66296, 2.72938,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3631, 139.53810, -250.45741, 2.59965,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(5130, 148.04903, -260.30179, 4.93670,   0.00000, 0.00000, -135.03400);
	CreateDynamicObject(3574, 64.76384, -336.96167, 3.23567,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(3287, 194.72818, -341.35474, 5.15970,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(3286, 170.24591, -341.06198, 5.05856,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3258, 126.38306, -237.54558, 1.79850,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(0, 155.94830, -279.91629, 3.47610,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(5260, 143.48607, -274.35181, 2.23939,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3631, 148.17390, -258.45654, 2.59965,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3577, 156.04150, -248.67490, 2.72940,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(3287, 192.49438, -329.63068, 5.15972,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3255, 179.63191, -333.34015, 1.35477,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3465, 172.41054, -332.97928, 1.85374,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(18257, 55.32111, -241.46448, 0.49620,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3568, 90.20992, -272.11368, 2.95580,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3569, 86.06417, -272.18405, 2.95408,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3573, 22.62951, -232.98683, 3.78196,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(3575, 51.20434, -227.38058, 3.22774,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(3722, 184.30818, -301.08380, 4.94943,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(8041, 77.68623, -222.72020, 6.03920,   0.00000, 0.00000, -271.57080);
	CreateDynamicObject(8650, 60.90022, -186.49287, 1.63648,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(8674, 92.71979, -221.84709, 2.03140,   0.00000, 0.00000, -1.57080);
	CreateDynamicObject(8674, 52.44750, -220.44031, 2.03140,   0.00000, 0.00000, -2.36000);
	CreateDynamicObject(8674, 62.72321, -220.89021, 2.03140,   0.00000, 0.00000, -2.64800);
	CreateDynamicObject(8674, 103.00962, -222.08618, 2.03140,   0.00000, 0.00000, -1.10980);
	CreateDynamicObject(5262, 24.67730, -324.81100, 4.19920,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(3630, 39.39944, -334.61377, 2.43223,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3630, 36.18538, -315.27289, 2.43220,   0.00000, 0.00000, 180.00000);
	CreateDynamicObject(939, 45.44732, -317.32880, 3.24800,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(1426, 27.80901, -306.16846, 1.01079,   356.85840, 0.00000, 0.00000);
	CreateDynamicObject(1426, 38.85953, -311.22198, 1.20237,   356.85840, 0.00000, 0.00000);
	CreateDynamicObject(2991, 127.24240, -331.82977, 1.12551,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(3465, 180.97519, -327.06033, 1.85370,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(12929, 161.85220, -231.39931, 0.50269,   0.00000, 0.00000, -1.57080);
	CreateDynamicObject(12930, 166.42619, -231.20016, 1.39707,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(1438, 133.69513, -233.86526, 0.56676,   3.14159, 0.00000, 1.65806);
	CreateDynamicObject(2975, 70.84502, -331.15140, 0.49685,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2974, 48.95454, -315.94458, 0.59711,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1440, 60.49398, -277.49170, 1.02900,   356.85840, 0.00000, 89.58000);
	CreateDynamicObject(1493, 149.26379, -250.67570, 7.81870,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(1493, 161.44260, -244.54710, 7.81870,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(1438, 159.06169, -236.65829, 0.55656,   356.85840, 0.00000, -3.11541);
	CreateDynamicObject(967, 196.19701, -316.08255, 0.66440,   0.00000, 0.00000, 186.00000);
	CreateDynamicObject(966, 197.73688, -317.13739, 0.52210,   0.00000, 0.00000, 96.00000);
	CreateDynamicObject(2934, 137.46309, -307.56522, 1.95982,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(2935, 157.16095, -280.40817, 2.02020,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3577, 136.53712, -291.56821, 4.28565,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3577, 140.07397, -291.91653, 1.30982,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(1685, 139.05864, -290.86459, 2.80077,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2934, 157.17941, -280.29233, 4.91963,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1271, 30.32336, -314.55685, 1.63878,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2912, 24.67353, -326.77829, 1.34051,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(3722, 184.35382, -308.49194, 4.94943,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(2912, 24.67729, -325.94949, 1.34051,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2912, 24.61307, -325.21115, 1.34051,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2912, 25.75719, -326.44281, 1.34051,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2912, 25.76376, -325.63382, 1.34051,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2912, 26.80178, -326.03564, 1.34051,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1271, 29.44022, -314.52765, 1.63878,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1271, 28.53480, -314.45959, 1.63878,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1271, 30.81669, -315.47015, 1.63878,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1271, 29.91243, -315.42209, 1.63878,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1271, 30.32589, -316.33984, 1.63878,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2912, 29.49336, -335.63037, 1.34051,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2912, 30.30181, -335.67148, 1.34051,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(2912, 31.16499, -335.73862, 1.34051,   0.00000, 0.00000, 0.00000);
}

hook OnPlayerConnect(playerid) {
	RemoveBuildingForPlayer(playerid, 13191, 65.2578, -303.9844, 14.4531, 0.25);
	RemoveBuildingForPlayer(playerid, 13192, 164.7109, -234.1875, 0.4766, 0.25);
	RemoveBuildingForPlayer(playerid, 13193, 173.5156, -323.8203, 0.5156, 0.25);
	RemoveBuildingForPlayer(playerid, 13194, 140.5938, -305.3906, 5.5938, 0.25);
	RemoveBuildingForPlayer(playerid, 13195, 36.8281, -256.2266, 0.4688, 0.25);
	RemoveBuildingForPlayer(playerid, 12858, 272.0625, -359.7656, 8.9531, 0.25);
	RemoveBuildingForPlayer(playerid, 1426, 29.1719, -292.2734, 1.4063, 0.25);
	RemoveBuildingForPlayer(playerid, 1431, 36.4297, -291.0625, 1.5703, 0.25);
	RemoveBuildingForPlayer(playerid, 1426, 24.5938, -291.7578, 1.4063, 0.25);
	RemoveBuildingForPlayer(playerid, 1438, 29.2344, -286.0547, 1.2188, 0.25);
	RemoveBuildingForPlayer(playerid, 1440, 32.4063, -289.2188, 1.6484, 0.25);
	RemoveBuildingForPlayer(playerid, 1438, 33.6016, -279.3516, 1.1172, 0.25);
	RemoveBuildingForPlayer(playerid, 12861, 36.8281, -256.2266, 0.4688, 0.25);
	RemoveBuildingForPlayer(playerid, 1450, 43.4844, -252.5703, 1.2031, 0.25);
	RemoveBuildingForPlayer(playerid, 1449, 43.1094, -254.9609, 1.2188, 0.25);
	RemoveBuildingForPlayer(playerid, 12859, 173.5156, -323.8203, 0.5156, 0.25);
	RemoveBuildingForPlayer(playerid, 12805, 65.2578, -303.9844, 14.4531, 0.25);
	RemoveBuildingForPlayer(playerid, 13198, 140.5938, -305.3906, 5.5938, 0.25);
	RemoveBuildingForPlayer(playerid, 12956, 96.3281, -261.1953, 3.8594, 0.25);
	RemoveBuildingForPlayer(playerid, 12860, 164.7109, -234.1875, 0.4766, 0.25);
}