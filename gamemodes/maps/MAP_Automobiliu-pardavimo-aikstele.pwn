#include <YSI\y_hooks>

hook OnGameModeInit() {
	CreateDynamicObject(769, 1286.09937, 207.32990, 18.42035,   357.00000, 0.00000, -193.00000);
	CreateDynamicObject(8406, 1268.96179, 178.63329, 23.81263,   0.00000, 0.00000, 66.45100);
	CreateDynamicObject(966, 1269.05566, 178.60606, 18.35738,   0.00000, 0.00000, 246.24500);
	CreateDynamicObject(968, 1269.06494, 178.62711, 19.04810,   0.00000, 0.00000, 246.24500);
	CreateDynamicObject(967, 1267.88672, 177.48146, 18.35626,   0.00000, 0.00000, -23.97900);
	CreateDynamicObject(997, 1272.77332, 187.93520, 18.98800,   0.00000, 0.00000, 246.24500);
	CreateDynamicObject(997, 1266.90417, 174.62302, 18.95748,   0.00000, 0.00000, 246.24500);
	CreateDynamicObject(3626, 1294.24939, 171.19009, 20.92570,   0.00000, 0.00000, -109.00000);
	CreateDynamicObject(2008, 1293.58032, 169.67776, 19.68938,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1663, 1294.20020, 167.93646, 20.12213,   0.00000, 0.00000, -178.00000);
	CreateDynamicObject(3425, 1271.16345, 158.40575, 29.86581,   0.00000, 0.00000, 0.00000);
}

hook OnPlayerConnect(playerid) {
	RemoveBuildingForPlayer(playerid, 3339, 1297.2813, 173.5781, 19.4609, 0.25);
	RemoveBuildingForPlayer(playerid, 3340, 1308.3984, 168.1406, 19.4063, 0.25);
	RemoveBuildingForPlayer(playerid, 3341, 1281.2266, 158.0938, 19.3438, 0.25);
	RemoveBuildingForPlayer(playerid, 3343, 1295.9844, 158.7422, 19.3828, 0.25);
	RemoveBuildingForPlayer(playerid, 3343, 1305.0391, 184.9141, 19.3438, 0.25);
	RemoveBuildingForPlayer(playerid, 3345, 1298.6094, 194.2656, 19.3281, 0.25);
	RemoveBuildingForPlayer(playerid, 3345, 1305.2656, 152.7734, 19.2969, 0.25);
	RemoveBuildingForPlayer(playerid, 3344, 1299.7109, 138.7344, 19.3438, 0.25);
	RemoveBuildingForPlayer(playerid, 3344, 1313.9688, 179.8438, 19.3594, 0.25);
	RemoveBuildingForPlayer(playerid, 3594, 1265.5703, 153.3828, 19.4609, 0.25);
	RemoveBuildingForPlayer(playerid, 3170, 1281.2266, 158.0938, 19.3438, 0.25);
	RemoveBuildingForPlayer(playerid, 3594, 1280.9297, 147.3125, 20.0078, 0.25);
	RemoveBuildingForPlayer(playerid, 3168, 1295.9844, 158.7422, 19.3828, 0.25);
	RemoveBuildingForPlayer(playerid, 3171, 1299.7109, 138.7344, 19.3438, 0.25);
	RemoveBuildingForPlayer(playerid, 3172, 1305.2656, 152.7734, 19.2969, 0.25);
	RemoveBuildingForPlayer(playerid, 1308, 1301.2578, 147.3828, 18.8203, 0.25);
	RemoveBuildingForPlayer(playerid, 3594, 1271.3125, 163.3906, 19.4375, 0.25);
	RemoveBuildingForPlayer(playerid, 1294, 1264.5469, 172.2188, 23.0078, 0.25);
	RemoveBuildingForPlayer(playerid, 1308, 1281.5156, 165.5313, 18.8203, 0.25);
	RemoveBuildingForPlayer(playerid, 1440, 1297.0703, 179.2266, 19.9453, 0.25);
	RemoveBuildingForPlayer(playerid, 1440, 1293.5547, 163.8828, 19.9453, 0.25);
	RemoveBuildingForPlayer(playerid, 3168, 1305.0391, 184.9141, 19.3438, 0.25);
	RemoveBuildingForPlayer(playerid, 3169, 1297.2813, 173.5781, 19.4609, 0.25);
	RemoveBuildingForPlayer(playerid, 775, 1301.2734, 164.2656, 19.3516, 0.25);
	RemoveBuildingForPlayer(playerid, 3171, 1313.9688, 179.8438, 19.3594, 0.25);
	RemoveBuildingForPlayer(playerid, 3167, 1308.3984, 168.1406, 19.4063, 0.25);
	RemoveBuildingForPlayer(playerid, 1308, 1308.2656, 174.2891, 18.8203, 0.25);
	RemoveBuildingForPlayer(playerid, 1440, 1313.3203, 167.0313, 19.9453, 0.25);
	RemoveBuildingForPlayer(playerid, 3172, 1298.6094, 194.2656, 19.3281, 0.25);
	RemoveBuildingForPlayer(playerid, 1308, 1274.1875, 191.4609, 18.8203, 0.25);
	RemoveBuildingForPlayer(playerid, 1438, 1276.1172, 190.2266, 18.6094, 0.25);
	RemoveBuildingForPlayer(playerid, 1440, 1318.3828, 196.0547, 19.6094, 0.25);
	RemoveBuildingForPlayer(playerid, 705, 1290.4766, 198.6797, 18.7656, 0.25);
	RemoveBuildingForPlayer(playerid, 13441, 1338.0625, 198.7344, 30.0234, 0.25);
}