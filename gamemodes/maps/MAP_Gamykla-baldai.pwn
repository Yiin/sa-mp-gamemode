#include <YSI_Coding\y_hooks>

hook OnGameModeInit() {
	CreateDynamicObject(13435, 308.36365, -254.28282, 3.52400,   0.00000, 0.00000, -4.00000);
	CreateDynamicObject(14872, 318.39471, -237.76860, 0.95374,   0.00000, 0.00000, 142.00000);
	CreateDynamicObject(846, 320.11688, -236.71556, 1.05294,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1345, 303.63510, -253.49416, 1.25780,   356.85840, 0.00000, 90.00000);
	CreateDynamicObject(1450, 303.45044, -225.12727, 1.14063,   3.14159, 0.00000, 1.57080);
	CreateDynamicObject(1440, 321.69266, -227.83897, 1.02344,   356.85840, 0.00000, -1.57080);
	CreateDynamicObject(1431, 303.26669, -230.10577, 1.08590,   356.85840, 0.00000, 90.00000);
	CreateDynamicObject(669, 327.43869, -223.95230, 1.01472,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(933, 303.71283, -246.53912, 0.51670,   0.00000, 0.00000, 0.00000);
	CreateDynamicObject(1345, 303.63510, -250.91960, 1.25780,   356.85840, 0.00000, 90.00000);
	CreateDynamicObject(910, 303.41861, -234.72676, 1.75495,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(1462, 303.30170, -232.77769, 0.56310,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(1415, 303.38177, -223.14653, 0.61695,   0.00000, 0.00000, 90.00000);
	CreateDynamicObject(1412, 305.41754, -221.95840, 1.84380,   3.14160, 0.00000, 0.00000);
	CreateDynamicObject(1412, 310.59015, -221.95773, 1.84380,   3.14160, 0.00000, 0.00000);
	CreateDynamicObject(1412, 322.81613, -225.32774, 1.85156,   356.85840, 0.00000, -1.57044);
}

hook OnPlayerConnect(playerid) {
	RemoveBuildingForPlayer(playerid, 1345, 306.0625, -255.1016, 1.2578, 0.25);
	RemoveBuildingForPlayer(playerid, 1450, 303.3906, -252.1563, 1.1406, 0.25);
	RemoveBuildingForPlayer(playerid, 1431, 303.6172, -247.8750, 1.0859, 0.25);
	RemoveBuildingForPlayer(playerid, 1345, 308.6719, -255.0938, 1.2578, 0.25);
	RemoveBuildingForPlayer(playerid, 1431, 303.2031, -245.7422, 1.0859, 0.25);
	RemoveBuildingForPlayer(playerid, 1426, 304.0938, -231.2578, 0.6797, 0.25);
	RemoveBuildingForPlayer(playerid, 1519, 304.1641, -223.2813, 1.6797, 0.25);
	RemoveBuildingForPlayer(playerid, 1521, 304.1328, -223.5703, 4.0234, 0.25);
	RemoveBuildingForPlayer(playerid, 1431, 304.0000, -228.7500, 1.0859, 0.25);
	RemoveBuildingForPlayer(playerid, 1440, 305.9063, -232.1719, 1.0234, 0.25);
}