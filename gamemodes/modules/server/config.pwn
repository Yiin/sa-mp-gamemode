/*
 * server/config
 *
 * Serverio konfiguracijos valdymas
 */

#include <YSI_Coding\y_hooks>

static configFileName[] = "config.json";

hook OnGameModeInit() {
    DJSON_cache_ReloadFile(configFileName);
}

config(key[], value[] = "", keyLength = sizeof key, valueLength = sizeof value) {
	// syntax-sugar, kad b�t� galima ra�yti pvz "mysql.password" vietoj "mysql/password"
	strreplace(key, ".", "/", .maxlength = keyLength);

	if(!isnull(value) || valueLength > 1) {
		format(value, valueLength, "%s", dj(configFileName, key));
		return 1;
	}
	return djInt(configFileName, key);
}