/*
 * server/database
 *
 * Serverio duomen� baz�
 */

#include <a_mysql>
#include <YSI_Coding\y_hooks>

new MySQL:database; 
new query[2000];

static database_error;

hook OnGameModeInit() { 
    mysql_log();

    new 
        host[YSI_MAX_STRING], 
        database_name[YSI_MAX_STRING], 
        user[YSI_MAX_STRING], 
        password[YSI_MAX_STRING]
    ;

    config("mysql.host", host);
    config("mysql.database", database_name);
    config("mysql.user", user);
    config("mysql.password", password);

    database = mysql_connect(host, user, password, database_name);
    
    if(!(database_error = mysql_errno())) {
        printf("Successfully connected to database");
    }
}

hook OnGameModeExit() {
    mysql_close(database);
}

CountInTable(table_name[], const where[], va_args<>) {
    static _where[500];
    va_formatex(_where, _, where, va_start<2>);

    format_query("SELECT * FROM %s %s", table_name, _where);
    new Cache:cache = mysql_query(database, query);

    static count =: cache_num_rows();
    cache_delete(cache);

    return count;
}

ExistsInTable(table_name[], const where[], va_args<>) {
    static _where[500];
    va_formatex(_where, _, where, va_start<2>);

    return CountInTable(table_name, _where) > 0;
}

Test:Database() {
    TEST_REPORT("*** Test Database start");
    ASSERT(database && !database_error);
    return true;
}