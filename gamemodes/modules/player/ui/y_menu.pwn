#include "YSI\y_hooks"

static const 
	gang = 1,
	bussiness = 2,
	information = 4
;

static Text:TD_YMenu[5];
static showing[MAX_PLAYERS char];
static can[MAX_PLAYERS char];

static show(playerid) {
	showing{playerid} = 1;
	can{playerid} = 0;

	SelectTextDraw(playerid, 0xFF0000FF);

	if(GetPlayerGang(playerid)) {
		TextDrawColor(TD_YMenu[3], -176);
		TextDrawBackgroundColor(TD_YMenu[3], 5);
		can{playerid} |= gang;
	}
	else {
		TextDrawColor(TD_YMenu[3], -1);
		TextDrawBackgroundColor(TD_YMenu[3], 51);
	}

	if(FALSE/* && HasBussiness(playerid)*/) {
		TextDrawColor(TD_YMenu[3], -176);
		TextDrawBackgroundColor(TD_YMenu[3], 5);
		can{playerid} |= bussiness;
	}
	else {
		TextDrawColor(TD_YMenu[3], -1);
		TextDrawBackgroundColor(TD_YMenu[3], 51);
	}
	
	foreach(new i : Array(TD_YMenu)) {
		TextDrawShowForPlayer(playerid, Text:i);
	}
}

static hide(playerid, cancel = true) {
	showing{playerid} = 0;

	if(cancel) {
		CancelSelectTextDraw(playerid);
	}
	
	foreach(new i : Array(TD_YMenu)) {
		TextDrawHideForPlayer(playerid, Text:i);
	}
}

hook OnPlayerConnect(playerid) {
	showing{playerid} = 0;
}

hook OnPlayerDeath(playerid) {
	hide(playerid);
}

hook OnCharacterDespawn(playerid) {
	hide(playerid);
}

hook OnPlayerKeyStateChange(playerid, newkeys, oldkeys) {
	if(PRESSED(KEY_YES)) {
		show(playerid);
	}
}

hook OnPlayerClickTextDraw(playerid, Text:clickedid) {
	if(clickedid == Text:INVALID_TEXT_DRAW) {
		if(showing{playerid}) {
			hide(playerid);
		}
	}
	if(clickedid == TD_YMenu[0]) {
		hide(playerid, false);

		OpenInventory(playerid);
	}
	if(clickedid == TD_YMenu[2] && can{playerid} & information) {
		hide(playerid);

		// Player information
	}
	if(clickedid == TD_YMenu[3] && can{playerid} & gang) {
		hide(playerid);

		OpenGangMenu(playerid);
	}
	if(clickedid == TD_YMenu[4] && can{playerid} & bussiness) {
		hide(playerid);

		// Player bussiness menu
	}
}

hook OnGameModeInit() {
	TD_YMenu[0] = TextDrawCreate(216.000045, 155.166671, "Inventorius");
	TextDrawLetterSize(TD_YMenu[0], 0.313058, 1.722499);
	TextDrawTextSize(TD_YMenu[0], 35.294132, 120.750007);
	TextDrawAlignment(TD_YMenu[0], 2);
	TextDrawColor(TD_YMenu[0], -1);
	TextDrawUseBox(TD_YMenu[0], true);
	TextDrawBoxColor(TD_YMenu[0], 4);
	TextDrawSetShadow(TD_YMenu[0], 0);
	TextDrawSetOutline(TD_YMenu[0], 1);
	TextDrawBackgroundColor(TD_YMenu[0], 51);
	TextDrawFont(TD_YMenu[0], 2);
	TextDrawSetProportional(TD_YMenu[0], 1);
	TextDrawSetSelectable(TD_YMenu[0], true);

	TD_YMenu[1] = TextDrawCreate(406.647216, 155.000015, "Nustatymai");
	TextDrawLetterSize(TD_YMenu[1], 0.313058, 1.722499);
	TextDrawTextSize(TD_YMenu[1], 35.294132, 120.750007);
	TextDrawAlignment(TD_YMenu[1], 2);
	TextDrawColor(TD_YMenu[1], -1);
	TextDrawUseBox(TD_YMenu[1], true);
	TextDrawBoxColor(TD_YMenu[1], 4);
	TextDrawSetShadow(TD_YMenu[1], 0);
	TextDrawSetOutline(TD_YMenu[1], 1);
	TextDrawBackgroundColor(TD_YMenu[1], 51);
	TextDrawFont(TD_YMenu[1], 2);
	TextDrawSetProportional(TD_YMenu[1], 1);
	TextDrawSetSelectable(TD_YMenu[1], true);

	TD_YMenu[2] = TextDrawCreate(320.117767, 210.833343, "Zaidejas");
	TextDrawLetterSize(TD_YMenu[2], 0.313058, 1.722499);
	TextDrawTextSize(TD_YMenu[2], 35.294132, 120.750007);
	TextDrawAlignment(TD_YMenu[2], 2);
	TextDrawColor(TD_YMenu[2], -1);
	TextDrawUseBox(TD_YMenu[2], true);
	TextDrawBoxColor(TD_YMenu[2], 4);
	TextDrawSetShadow(TD_YMenu[2], 0);
	TextDrawSetOutline(TD_YMenu[2], 1);
	TextDrawBackgroundColor(TD_YMenu[2], 51);
	TextDrawFont(TD_YMenu[2], 2);
	TextDrawSetProportional(TD_YMenu[2], 1);
	TextDrawSetSelectable(TD_YMenu[2], true);

	TD_YMenu[3] = TextDrawCreate(214.764831, 264.916748, "Gauja");
	TextDrawLetterSize(TD_YMenu[3], 0.313058, 1.722499);
	TextDrawTextSize(TD_YMenu[3], 35.294132, 120.750007);
	TextDrawAlignment(TD_YMenu[3], 2);
	TextDrawColor(TD_YMenu[3], -1);
	TextDrawUseBox(TD_YMenu[3], true);
	TextDrawBoxColor(TD_YMenu[3], 4);
	TextDrawSetShadow(TD_YMenu[3], 0);
	TextDrawSetOutline(TD_YMenu[3], 1);
	TextDrawBackgroundColor(TD_YMenu[3], 51);
	TextDrawFont(TD_YMenu[3], 2);
	TextDrawSetProportional(TD_YMenu[3], 1);
	TextDrawSetSelectable(TD_YMenu[3], true);

	TD_YMenu[4] = TextDrawCreate(408.235443, 270.000152, "Verslas");
	TextDrawLetterSize(TD_YMenu[4], 0.313058, 1.722499);
	TextDrawTextSize(TD_YMenu[4], 35.294132, 120.750007);
	TextDrawAlignment(TD_YMenu[4], 2);
	TextDrawColor(TD_YMenu[4], -176);
	TextDrawUseBox(TD_YMenu[4], true);
	TextDrawBoxColor(TD_YMenu[4], 4);
	TextDrawSetShadow(TD_YMenu[4], 0);
	TextDrawSetOutline(TD_YMenu[4], 1);
	TextDrawBackgroundColor(TD_YMenu[4], 5);
	TextDrawFont(TD_YMenu[4], 2);
	TextDrawSetProportional(TD_YMenu[4], 1);
	TextDrawSetSelectable(TD_YMenu[4], true);
}