#include <YSI\y_hooks>

new Text:TD_Progress[6];
new PlayerText:PlayerTD_Progress[MAX_PLAYERS][2];

hook OnGameModeInit() {
	TD_Progress[0] = TextDrawCreate(470.000000, 200.000000, "LD_SPAC:white");
	TextDrawLetterSize(TD_Progress[0], 0.000000, 0.000000);
	TextDrawTextSize(TD_Progress[0], 13.000000, 200.000000);
	TextDrawAlignment(TD_Progress[0], 1);
	TextDrawColor(TD_Progress[0], 255);
	TextDrawSetShadow(TD_Progress[0], 0);
	TextDrawSetOutline(TD_Progress[0], 0);
	TextDrawBackgroundColor(TD_Progress[0], 255);
	TextDrawFont(TD_Progress[0], 4);
	TextDrawSetProportional(TD_Progress[0], 0);
	TextDrawSetShadow(TD_Progress[0], 0);

	TD_Progress[1] = TextDrawCreate(471.000000, 201.000000, "LD_SPAC:white");
	TextDrawLetterSize(TD_Progress[1], 0.000000, 0.000000);
	TextDrawTextSize(TD_Progress[1], 11.000000, 198.000000);
	TextDrawAlignment(TD_Progress[1], 1);
	TextDrawColor(TD_Progress[1], -2147483393);
	TextDrawSetShadow(TD_Progress[1], 0);
	TextDrawSetOutline(TD_Progress[1], 0);
	TextDrawBackgroundColor(TD_Progress[1], 255);
	TextDrawFont(TD_Progress[1], 4);
	TextDrawSetProportional(TD_Progress[1], 0);
	TextDrawSetShadow(TD_Progress[1], 0);

	TD_Progress[2] = TextDrawCreate(488.000000, 199.000000, "100%");
	TextDrawLetterSize(TD_Progress[2], 0.286617, 1.489166);
	TextDrawAlignment(TD_Progress[2], 1);
	TextDrawColor(TD_Progress[2], -1);
	TextDrawSetShadow(TD_Progress[2], 0);
	TextDrawSetOutline(TD_Progress[2], -1);
	TextDrawBackgroundColor(TD_Progress[2], 255);
	TextDrawFont(TD_Progress[2], 2);
	TextDrawSetProportional(TD_Progress[2], 1);
	TextDrawSetShadow(TD_Progress[2], 0);

	TD_Progress[3] = TextDrawCreate(488.000000, 388.000000, "0%");
	TextDrawLetterSize(TD_Progress[3], 0.286617, 1.489166);
	TextDrawAlignment(TD_Progress[3], 1);
	TextDrawColor(TD_Progress[3], -1);
	TextDrawSetShadow(TD_Progress[3], 0);
	TextDrawSetOutline(TD_Progress[3], -1);
	TextDrawBackgroundColor(TD_Progress[3], 255);
	TextDrawFont(TD_Progress[3], 2);
	TextDrawSetProportional(TD_Progress[3], 1);
	TextDrawSetShadow(TD_Progress[3], 0);

	TD_Progress[4] = TextDrawCreate(488.000000, 248.000000, "p~n~r~n~o~n~g~n~r~n~e~n~s~n~a~n~s");
	TextDrawLetterSize(TD_Progress[4], 0.200878, 1.366667);
	TextDrawAlignment(TD_Progress[4], 2);
	TextDrawColor(TD_Progress[4], -1);
	TextDrawSetShadow(TD_Progress[4], 0);
	TextDrawSetOutline(TD_Progress[4], 1);
	TextDrawBackgroundColor(TD_Progress[4], 255);
	TextDrawFont(TD_Progress[4], 2);
	TextDrawSetProportional(TD_Progress[4], 1);
	TextDrawSetShadow(TD_Progress[4], 0);

	TD_Progress[5] = TextDrawCreate(162.000000, 410.000000, "Progresas_kyla_laikant_paspausta_peles_mygtuka.");
	TextDrawLetterSize(TD_Progress[5], 0.234143, 1.541666);
	TextDrawAlignment(TD_Progress[5], 1);
	TextDrawColor(TD_Progress[5], -1);
	TextDrawSetShadow(TD_Progress[5], 0);
	TextDrawSetOutline(TD_Progress[5], 1);
	TextDrawBackgroundColor(TD_Progress[5], 255);
	TextDrawFont(TD_Progress[5], 2);
	TextDrawSetProportional(TD_Progress[5], 1);
	TextDrawSetShadow(TD_Progress[5], 0);
}

hook OnPlayerConnect(playerid) {
	PlayerTD_Progress[playerid][0] = CreatePlayerTextDraw(playerid, 471.000000, 198.0 + 201.000000, "LD_SPAC:white");
	PlayerTextDrawLetterSize(playerid, PlayerTD_Progress[playerid][0], 0.000000, 0.000000);
	PlayerTextDrawTextSize(playerid, PlayerTD_Progress[playerid][0], 11.000000, -198.000000);
	PlayerTextDrawAlignment(playerid, PlayerTD_Progress[playerid][0], 1);
	PlayerTextDrawColor(playerid, PlayerTD_Progress[playerid][0], -1523963137);
	PlayerTextDrawSetShadow(playerid, PlayerTD_Progress[playerid][0], 0);
	PlayerTextDrawSetOutline(playerid, PlayerTD_Progress[playerid][0], 0);
	PlayerTextDrawBackgroundColor(playerid, PlayerTD_Progress[playerid][0], 255);
	PlayerTextDrawFont(playerid, PlayerTD_Progress[playerid][0], 4);
	PlayerTextDrawSetProportional(playerid, PlayerTD_Progress[playerid][0], 0);
	PlayerTextDrawSetShadow(playerid, PlayerTD_Progress[playerid][0], 0);

	PlayerTD_Progress[playerid][1] = CreatePlayerTextDraw(playerid, 320.000000, 360.000000, "Anglis");
	PlayerTextDrawLetterSize(playerid, PlayerTD_Progress[playerid][1], 0.598652, 2.819166);
	PlayerTextDrawAlignment(playerid, PlayerTD_Progress[playerid][1], 2);
	PlayerTextDrawColor(playerid, PlayerTD_Progress[playerid][1], -1);
	PlayerTextDrawSetShadow(playerid, PlayerTD_Progress[playerid][1], 0);
	PlayerTextDrawSetOutline(playerid, PlayerTD_Progress[playerid][1], -1);
	PlayerTextDrawBackgroundColor(playerid, PlayerTD_Progress[playerid][1], 255);
	PlayerTextDrawFont(playerid, PlayerTD_Progress[playerid][1], 0);
	PlayerTextDrawSetProportional(playerid, PlayerTD_Progress[playerid][1], 1);
	PlayerTextDrawSetShadow(playerid, PlayerTD_Progress[playerid][1], 0);
}

enum _:e_PROGRESS_CALLBACK_TYPES {
	PROGRESS_CALLBACK_START,
	PROGRESS_CALLBACK_TICK,
	PROGRESS_CALLBACK_SUCCESS,
	PROGRESS_CALLBACK_FAIL,
	PROGRESS_CALLBACK_END
};

new progress_callback[MAX_PLAYERS][e_PROGRESS_CALLBACK_TYPES][E_CALLBACK_DATA];
static Float:current_progress[MAX_PLAYERS];
static Timer:progress_timer[MAX_PLAYERS];
static ignore_tick = false;
static allowed_state[MAX_PLAYERS][10];

hook OnPlayerDataLoaded(playerid) {
	current_progress[playerid] = 0.0;
	progress_timer[playerid] = Timer:NONE;
}

hook OnPlayerDespawn(playerid) {
	HideProgress(playerid, false);
}

timer ProgressTimer[100](playerid) {
	TickProgress(playerid, .raw = 5.0);
}

StartProgress(playerid, duration = NONE) {
	ShowProgress(playerid);

	current_progress[playerid] = 0.0;

	if(duration != NONE) {
		if(progress_timer[playerid] != Timer:NONE) {
			stop progress_timer[playerid];
		}
		new time_between_steps = floatround(1000.0 * duration / 40.0, floatround_ceil);

		progress_timer[playerid] = repeat ProgressTimer[time_between_steps](playerid);
	}
}

// amount: %
TickProgress(playerid, Float:amount = 10.0, Float:raw = FLOAT_INFINITY) {
	if(current_progress[playerid] == 0.0) {
		if(progress_callback[playerid][PROGRESS_CALLBACK_START][E_CALLBACK_DATA_POINTER]) {
			Callback_Call(progress_callback[playerid][PROGRESS_CALLBACK_START]);
		}
	}
	
	if(progress_callback[playerid][PROGRESS_CALLBACK_TICK][E_CALLBACK_DATA_POINTER]) {
		Callback_Call(progress_callback[playerid][PROGRESS_CALLBACK_TICK]);
	}

	if(current_progress[playerid] && ignore_tick) {
		ignore_tick = false;
		return true;
	}

	raw = (raw == FLOAT_INFINITY ? (amount / 100.0 * 198.0) : raw);

	current_progress[playerid] -= raw;

	if(current_progress[playerid] < -198.0) {
		current_progress[playerid] = -198.0;
		StopProgress(playerid);
	}
	PlayerTextDrawTextSize(playerid, PlayerTD_Progress[playerid][0], 11.0, current_progress[playerid]);
	PlayerTextDrawShow(playerid, PlayerTD_Progress[playerid][0]);

	return true;
}

StopProgress(playerid, bool:complete = true) {
	if(complete) {
		if(progress_callback[playerid][PROGRESS_CALLBACK_SUCCESS][E_CALLBACK_DATA_POINTER]) {
			Callback_Call(progress_callback[playerid][PROGRESS_CALLBACK_SUCCESS]);
			Callback_Release(progress_callback[playerid][PROGRESS_CALLBACK_SUCCESS]);
		}
	}
	else {
		if(progress_callback[playerid][PROGRESS_CALLBACK_FAIL][E_CALLBACK_DATA_POINTER]) {
			Callback_Call(progress_callback[playerid][PROGRESS_CALLBACK_FAIL]);
			Callback_Release(progress_callback[playerid][PROGRESS_CALLBACK_FAIL]);
		}
	}
	if(progress_callback[playerid][PROGRESS_CALLBACK_END][E_CALLBACK_DATA_POINTER]) {
		Callback_Call(progress_callback[playerid][PROGRESS_CALLBACK_END]);
		Callback_Release(progress_callback[playerid][PROGRESS_CALLBACK_END]);
	}
	stop progress_timer[playerid];

	ResetProgressCallbacks(playerid);
	
	progress_timer[playerid] = Timer:NONE;
	current_progress[playerid] = 0.0;

	HideProgress(playerid, .delay = complete);

	return true;
}

ShowProgress(playerid) {
	foreach(new i : Array(TD_Progress)) {
		TextDrawShowForPlayer(playerid, Text:i);
	}
	foreach(new i : Array(PlayerTD_Progress[playerid])) {
		PlayerTextDrawShow(playerid, PlayerText:i);
	}
}

timer HideProgress[1000](playerid, bool:delay) {
	if(delay) {
		defer HideProgress(playerid, false);
	}
	else {
		foreach(new i : Array(TD_Progress)) {
			TextDrawHideForPlayer(playerid, Text:i);
		}
		foreach(new i : Array(PlayerTD_Progress[playerid])) {
			PlayerTextDrawHide(playerid, PlayerText:i);
		}
	}
}

SetProgressTitle(playerid, title[]) {
	PlayerTextDrawSetString(playerid, PlayerTD_Progress[playerid][1], title);
}

ResetProgressCallbacks(playerid) {
	foreach(new i : Limit(sizeof progress_callback[])) {
		Callback_Release(progress_callback[playerid][i]);
	}
}

SetProgressCallback(playerid, type, callback:callback) {
	new	data[E_CALLBACK_DATA];

	if (Callback_Get(callback, data)) {
		progress_callback[playerid][type] = data;
	}
}

SetProgressAllowedStates(playerid, ...) {
	memset(allowed_state[playerid]);

	for(new i, j = numargs(); i < j; ++i) {
		allowed_state[playerid][getarg(i)] = true;
	}
}

IgnoreProgressTick() {
	ignore_tick = true;
}

IsInProgress(playerid) {
	return progress_timer[playerid] != Timer:NONE;
}

hook OnPlayerStateChange(playerid, newstate, oldstate) {
	if(IsInProgress(playerid) && ! allowed_state[playerid][newstate]) {
		StopProgress(playerid, false);
	}
}