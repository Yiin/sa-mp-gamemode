// player/class-selection

#include <YSI_Coding\y_hooks>

#define MAX_CHARACTERS (sizeof ActorsPositions)

const STATE_SAME = UNIQUE_SYMBOL + 1;
const STATE_NONE = UNIQUE_SYMBOL + 2;
const STATE_SELECTING_NAME = UNIQUE_SYMBOL + 3;
const STATE_SELECTING_CHARACTER = UNIQUE_SYMBOL + 4;
const STATE_SELECTING_SKIN = UNIQUE_SYMBOL + 5;
const STATE_WAITING_FOR_SPECTATE = UNIQUE_SYMBOL + 6;

new lastTimeInFight[MAX_PLAYERS];

static createCharacter_ClassModels[14] = {58, 190, 202, 151, 101, 12, 35, 55, 168, 298, 37, 41, 249, 152};
static Float:ActorsPositions[][4] = {
	 {-564.5436,2369.6616,77.7350,114.7205}
	,{-562.2312,2363.0566,77.5467,133.5208}
	,{-554.6909,2360.1243,76.2686,206.0059}
	,{-550.1262,2363.6936,75.9591,216.8682}
	// ,{-543.7626,2358.2278,75.1132,221.9860}
	// ,{-549.1489,2358.9683,75.4656,204.7525}
	// ,{-553.1791,2356.3665,76.1099,181.6701}
	// ,{-543.2001,2355.3569,74.8150,212.6092}
	// ,{-574.2022,2370.3555,79.1887,121.9506}
};
static ActorsIds[MAX_PLAYERS][MAX_CHARACTERS];
static Text3D:ActorsInfo[MAX_PLAYERS][MAX_CHARACTERS];
static ActorsNames[MAX_PLAYERS][MAX_CHARACTERS][MAX_PLAYER_NAME];
static AmountOfActors[MAX_PLAYERS];

static CurrentSelectedSkin[MAX_PLAYERS char];
static CurrentSelectedCharacter[MAX_PLAYERS char];
static ShouldCameraCut[MAX_PLAYERS char];

CMD:veikejai(playerid, params[]) {
	if(CurrentState(playerid) != STATE_NONE) {
		return 1;
	}
	if(lastTimeInFight[playerid] + 10 < gettime()) {
		cleanResetToClassSelection(playerid, .update_pos = true);
	}
	else {
		M:P:E(playerid, "Turi palaukti nekovodamas dar [number]%i[] sec.", lastTimeInFight[playerid] + 10 - gettime());
	}
	return 1;
}

CMD:info(playerid, params[]) {
	if(CurrentState(playerid) == STATE_SELECTING_CHARACTER) {
		// ToDo: parodyti daugiau info apie veik�j� kai jos bus
	}
	return 1;
}

CMD:zaisti(playerid, params[]) {
	if(CurrentState(playerid) == STATE_SELECTING_CHARACTER) {
		SetPlayerName(playerid, ActorsNames[playerid][ CurrentSelectedCharacter{playerid} ]);
		CurrentState(playerid, STATE_NONE);
		playerData_LoadChar(playerid);
	}
	else {
		M:P:E(playerid, "Tu jau esi �aidime.");
	}
	return 1;
}

CMD:trinti(playerid, params[]) {
	if(CurrentState(playerid) == STATE_SELECTING_CHARACTER) {
		if(CurrentState(playerid) == STATE_SELECTING_CHARACTER && CurrentSelectedCharacter{playerid} != 255) {
			M:P:G(playerid, "Veik�jas \"[name]%s[]\" i�trintas!", ActorsNames[playerid][ CurrentSelectedCharacter{playerid} ]);
			format_query("DELETE FROM chars WHERE name = '%s'", ActorsNames[playerid][ CurrentSelectedCharacter{playerid} ]);
			cache_delete(mysql_query(database, query));
			ResetActors(playerid);
			ShowCharacterSelection(playerid);
		}
	}
	return 1;
}

CMD:kurti(playerid, params[]) {
	if(CurrentState(playerid) == STATE_SELECTING_CHARACTER) {
		if(CurrentState(playerid) == STATE_SELECTING_SKIN && CurrentSelectedSkin{playerid} < sizeof createCharacter_ClassModels) {
			LoadCreatedCharacter(playerid);
		}
		else {
			ResetActors(playerid);
			CreateCharacter(playerid);
		}
	}
	return 1;
}

static CurrentState(playerid, currentState = STATE_SAME) {
	static states[MAX_PLAYERS];

	if(currentState != STATE_SAME) {
		states[playerid] = currentState;
		call OnCharSelectStateUpdate(playerid, currentState);
	}

	return states[playerid];
}

hook OnCharSelectStateUpdate(playerid, newstate) {
	M:P:I(playerid, "new state: %i", newstate);
}

// Kai �aid�jas prisijungia prie �aidimo
hook OnPlayerConnect(playerid) {
	if(IsPlayerNPC(playerid)) {
		return;
	}
	// resetinam aktori� informacij�
	for(new i; i < MAX_CHARACTERS; ++i) {
		ActorsIds[playerid][i] = INVALID_ACTOR_ID;
		ActorsInfo[playerid][i] = Text3D:INVALID_3DTEXT_ID;
	}
	// paruo�iam kameros pozicij� v�lesniam laikui kai rinksimes veik�j�
	// SetCameraInFrontOfActor(playerid, 0, .update_skin = false);

	TogglePlayerSpectating(playerid, true);

	call OnPlayerEvent(playerid, EVENT_LOADING_PREVIEW_SKIN);

	CurrentState(playerid, STATE_WAITING_FOR_SPECTATE);
}

hook OnPlayerStateChange(playerid, newstate, oldstate) {
	if(CurrentState(playerid) == STATE_WAITING_FOR_SPECTATE && newstate == PLAYER_STATE_SPECTATING) {
		call OnPlayerEvent(playerid, EVENT_LOADED_PREVIEW_SKIN);

		// Parodom ferrie wheel i� cinematic pozicijos
		SetPlayerCameraPos(playerid, 362.1511, -2048.9016, 8.7793);
		SetPlayerCameraLookAt(playerid, 362.8162, -2048.1565, 9.0893);

		CurrentState(playerid, STATE_NONE);

		call OnPlayerEvent(playerid, EVENT_CONNECTED);
	}
}

// Kai �aid�jas u�siregistruoja, rodom jam veik�jo k�rimo lang�
hook OnPlayerRegister(playerid) {
	PrepareForCharacterSelection(playerid);

	CreateCharacter(playerid);
}

// Jeigu �aid�jas prisijungia prie acc, leid�iam jam pasirinkti norim� veik�j�
hook OnPlayerLogin(playerid) {
	PrepareForCharacterSelection(playerid);

	ShowCharacterSelection(playerid);
}

// Minimaliai paruo�iam �aid�j� veik�jo pasirinkimui
static PrepareForCharacterSelection(playerid) {
	// Nukeliam �aid�j� � jam unikal� virtual world
	new uniqueVirtualWorld = playerid + VW_OFFSET_CHARSELECT;

	if(GetPlayerVirtualWorld(playerid) != uniqueVirtualWorld) {
		SetPlayerVirtualWorld(playerid, uniqueVirtualWorld);
	}

	// Nustatom �aid�jo state � spectate
	if(GetPlayerState(playerid) != PLAYER_STATE_SPECTATING) {
		TogglePlayerSpectating(playerid, true);
	}
	// pakei�iam vard� � accounto, o ne veik�jo
	SetPlayerName(playerid, player_AccountName[playerid]);
}

static CreateCharacter(playerid) {
	// pasi�ymim, kad �aid�jas �iuo metu kuria nauj� veik�j�
	CurrentState(playerid, STATE_SELECTING_NAME);

	static characters_count =: CountInTable("characters", "where user_id = %i", player_AccountID[playerid]);

	// jeigu �aid�jas pasiek� turim� veik�j� limit�, gr��inam atgal � veik�j� pasirinkimo state 
	if(characters_count >= MAX_CHARACTERS) {
		M:P:E(playerid, "Pasiektas maksimalus veik�j� skai�ius - [number]%i[].", MAX_CHARACTERS);
		ShowCharacterSelection(playerid);
		return;
	}

	inline response(re, li) {
		#pragma unused li
		if(! re) {
			// jeigu �aid�jas at�auk� veik�jo k�rim�, ta�iau neturi suk�r�s joki� kit� veik�j�, papra�om susikurti arba i�eiti
			if( ! AmountOfActors[playerid]) {
				WarnAboutExit(playerid);
			}
			else {
				// kitu atveju tiesiog gr��inam � veik�j� pasirinkimo screen
				ShowCharacterSelection(playerid);
			}
		}
		else {
			// Jeigu blogas nick, rodom dialog� i� naujo
			if(InvalidNick(playerid, dialog_Input)) {
				CreateCharacter(playerid);
			}
			else {
				// patikrinam ar veik�jo vardas yra u�imtas
				if(ExistsInTable("chars", "WHERE name = '%s' LIMIT 1", dialog_Input)) {
					ClearChat(playerid, .everything = false);

					M:P:E(playerid, "�is vardas jau u�imtas.");
					// gr��inam � k�rimo lang�
					CreateCharacter(playerid);
					return;
				}
				// nustatom �aid�jui veik�jo vard�
				SetPlayerName(playerid, dialog_Input);

				ClearChat(playerid, .everything = false);
				M:P:G(playerid, "Vardas \"[name]%s[]\" yra laisvas! Dabar i�sirink savo veik�jo i�vaizd�.", dialog_Input);

				// i�saugom vard� naujam aktoriui
				strcpy(ActorsNames[playerid][characters_count], dialog_Input);
				
				// duodam pasirinkti skin�
				CurrentState(playerid, STATE_SELECTING_SKIN);
				call OnPlayerEvent(playerid, EVENT_LOADING_PREVIEW_SKIN);

				CurrentSelectedSkin{playerid} = 255;

				M:P:I(playerid, "Pasirinkus norim� i�vaizd�, ra�yk [highlight]/kurti[] arba spausk [highlight]ENTER[].");
			}
		}
	}

	dialogShow(playerid, using inline response, DIALOG_STYLE_INPUT, 
		"�ra�yk veik�jo vard� bei pavard�", 
		"Vard� ir pavard� gali sudaryti ma�iausiai 3 simboliai.\n\
		 Vard� ir pavard� reikia atskirti simboliu {ffffff}_", 
		 "Kurti");
}

static SelectingSkinTick(playerid) {
	// Jeigu dar nerodom jokio skino, parodom pat� pirm�
	if(CurrentSelectedSkin{playerid} == 255) {
		CurrentSelectedSkin{playerid} = 0;

		// toje vietoje kurioje bus naujas veik�jas
		SetCameraInFrontOfActor(playerid, AmountOfActors[playerid]);
	}
	static keys, lr, ud;
	static lastState[MAX_PLAYERS];
	GetPlayerKeys(playerid, keys, ud, lr);

	// Jei paspaud�ia enter
	if(keys & KEY_SECONDARY_ATTACK) {
		LoadCreatedCharacter(playerid);
		return;
	}

	if(lastState[playerid] != lr) {
		if(lr) {
			if(lr > 0) { // RIGHT = NEXT
				if(++CurrentSelectedSkin{playerid} == sizeof createCharacter_ClassModels) {
					CurrentSelectedSkin{playerid} = 0;
				}
			}
			else if(lr < 0) { // LEFT = PREV
				// 255 o ne -1, nes char tipo kintamieji saugo reik�mes tik nuo 0 iki 255
				if(--CurrentSelectedSkin{playerid} == 255) {
					CurrentSelectedSkin{playerid} = (sizeof createCharacter_ClassModels) - 1;
				}
			}
			SetCameraInFrontOfActor(playerid, AmountOfActors[playerid]);
		}
	}
	lastState[playerid] = lr;
}

static SelectingCharacterTick(playerid) {
	if(CurrentSelectedCharacter{playerid} == 254) {
		CurrentSelectedCharacter{playerid} = 0;

		SetCameraInFrontOfActor(playerid, 0, false);
	}
	if(CurrentSelectedCharacter{playerid} == 255) {
		CurrentSelectedCharacter{playerid} = 254;
		return;
	}
	static keys, lr, ud;
	static lastState[MAX_PLAYERS];
	GetPlayerKeys(playerid, keys, ud, lr);

	if(keys & KEY_NO || keys & KEY_WALK) {
		ResetActors(playerid);
		CreateCharacter(playerid);
		return;
	}
	if(keys & KEY_SECONDARY_ATTACK) {
		SetPlayerName(playerid, ActorsNames[playerid][ CurrentSelectedCharacter{playerid} ]);
		CurrentState(playerid, STATE_NONE);

		playerData_LoadChar(playerid);
		return;
	}

	if(lastState[playerid] != lr) {
		if(lr) {
			if(lr > 0) { // RIGHT
				if(++CurrentSelectedCharacter{playerid} == AmountOfActors[playerid]) {
					CurrentSelectedCharacter{playerid} = 0;
				}
			}
			else if(lr < 0) { // LEFT
				if(--CurrentSelectedCharacter{playerid} == 255) {
					CurrentSelectedCharacter{playerid} = AmountOfActors[playerid] - 1;
				}
			}
			SetCameraInFrontOfActor(playerid, CurrentSelectedCharacter{playerid}, false);
		}
	}
	lastState[playerid] = lr;
}

static WarnAboutExit(playerid) {
	inline response(re, li) {
		#pragma unused li
		if(re) {
			CreateCharacter(playerid);
		}
		else {
			Kick(playerid);
		}
	}
	dialogShow(playerid, using inline response, DIALOG_STYLE_MSGBOX, 
		"Ar tikrai nori palikti server�?", 
		"Norint �aisti serveryje, privaloma susikurti veik�j�, \n\
		 ta�iau jeigu nori palikti server� - paspausk mygtuk� \"I�eiti\"",
		"Kurti veik�j�", "I�eiti");
}

static LoadCreatedCharacter(playerid) {
	// pasi�ymim �aid�jo skin�
	player_CurrentSkin[playerid] = player_DefaultSkin[playerid] = createCharacter_ClassModels[CurrentSelectedSkin{playerid}];

	// pasi�ymim t� skin� prie available skin�
	player_AvailableSkins[playerid]{ player_DefaultSkin[playerid] } = true;
	CurrentState(playerid, STATE_NONE);

	// sukuriam veik�j� (player/data.pwn)
	playerData_LoadChar(playerid);
}

ShowCharacterSelection(playerid) {
	// jeigu �aid�jas neturi susik�r�s joki� veik�j�, pradedam veik�jo k�rim�
	if(ActorsIds[playerid][0] == INVALID_ACTOR_ID) {
		if(PopulateActors(playerid) == 0) {
			CreateCharacter(playerid);
			return;
		}
	}
	CurrentState(playerid, STATE_SELECTING_CHARACTER);

	CurrentSelectedCharacter{playerid} = 255;

	if(GetPlayerState(playerid) != PLAYER_STATE_SPECTATING) {
		TogglePlayerSpectating(playerid, true);
	}

	call OnPlayerEvent(playerid, EVENT_LOADING_PREVIEW_SKIN);

	ClearChat(playerid);
	
	M:P:X(playerid, "Pasirinkus norim� veik�j�, ra�yk arba spausk");
	M:P:X(playerid, "[highlight]/zaisti[] (arba [highlight]ENTER[] arba [highlight]SPACE[]) nor�damas prad�ti �aidim�,");
	M:P:X(playerid, "[highlight]/kurti[] (arba [highlight]ALT[]) nor�damas kurti nauj� veik�j�,");
	M:P:X(playerid, "[highlight]/trinti[] i�trinti pasirinkt� veik�j�.");
}

forward OnCharacterCreated(playerid);
public OnCharacterCreated(playerid) {
	new spawn_idx = random(sizeof Position_Spawn);
	player_PosX[playerid] = Position_Spawn[spawn_idx][0];
	player_PosY[playerid] = Position_Spawn[spawn_idx][1];
	player_PosZ[playerid] = Position_Spawn[spawn_idx][2];
	player_PosA[playerid] = Position_Spawn[spawn_idx][3];

	M:P:G(playerid, "Veik�jas \"[name]%s[]\" s�kmingai sukurtas!", player_Name[playerid]);

	player_CreationTimestamp[playerid] = gettime();
	
	cleanResetToClassSelection(playerid);
}

cleanResetToClassSelection(playerid, update_pos = false) {
	playerData_SaveChar(playerid, .update_pos = update_pos); // i�saugom veik�jo pradin� informacij�

	orm_destroy(GetPlayerORM(playerid));
	
	resetPlayerVars(playerid);

	PrepareForCharacterSelection(playerid);

	ShowCharacterSelection(playerid);
}

static ResetActors(playerid) {
	for(new i; i < MAX_CHARACTERS; ++i) {
		if(IsValidActor(ActorsIds[playerid][i])) {
			DestroyActor(ActorsIds[playerid][i]);
			ActorsIds[playerid][i] = INVALID_ACTOR_ID;

			DestroyDynamic3DTextLabel(ActorsInfo[playerid][i]);
			ActorsInfo[playerid][i] = Text3D:INVALID_3DTEXT_ID;
		}
	}
}

static PopulateActors(playerid) {
	format_query("SELECT * FROM chars WHERE user_id = %i", player_AccountID[playerid]);

	new Cache:cache = mysql_query(database, query);
	AmountOfActors[playerid] = cache_num_rows();

	new i = 0;
	if(AmountOfActors[playerid]) do {
		static text[500], name[MAX_PLAYER_NAME];
		cache_get_value_name(i, "name", name);
		if( ! isnull(name)) {
			ActorsNames[playerid][i][0] = EOS;
			strcat(ActorsNames[playerid][i], name);
		}
		static characterexp =: cache_get_value_name_int(i, "experience");
		static charactermoney =: cache_get_value_name_int(i, "cash");
		static charactermodel =: cache_get_value_name_int(i, "current_skin");
		static characterjob =: cache_get_value_name_int(i, "job");
		static characterjobrank =: cache_get_value_name_int(i, "job_rank");
		static characterwanted =: cache_get_value_name_int(i, "wanted");

		format(text, 500, "\
			{00FF00}%s\n\
			{ffffff}Darbas: {33ff33}%s (%s)\n\
			{ffffff}Patirtis: {33ff33}%i\n\
			{ffffff}Pinigai: {33ff33}%i\n\
			{ffffff}Ie�komumo lygis: {33ff33}%c (%i)\
		", ActorsNames[playerid][i],
		   GetJobName(characterjob), 
		   job_GetRankName(characterjobrank, characterjob), 
		   characterexp, charactermoney, 
		   police_TypeByWanted(characterwanted), 
		   characterwanted
		);
		
		ActorsInfo[playerid][i] = CreateDynamic3DTextLabel(text, 0xFFFFFFFF, 
			ActorsPositions[i][0],
			ActorsPositions[i][1],
			ActorsPositions[i][2] + 1.2, 30, .playerid = playerid
		);

		new current_actor = ActorsIds[playerid][i] = CreateActor(charactermodel, 
			ActorsPositions[i][0],
			ActorsPositions[i][1],
			ActorsPositions[i][2],
			ActorsPositions[i][3]
		);
		SetActorVirtualWorld(current_actor, playerid + VW_OFFSET_CHARSELECT);
		SetActorInvulnerable(current_actor);
	}
	while(++i < AmountOfActors[playerid] && i < MAX_CHARACTERS);

	Streamer_Update(playerid, STREAMER_TYPE_3D_TEXT_LABEL);

	cache_delete(cache);
	return AmountOfActors[playerid];
}

SetCameraInFrontOfActor(playerid, i, update_skin = true) {
	if(update_skin) {
		if(ActorsIds[playerid][i] != INVALID_ACTOR_ID) {
			DestroyActor(ActorsIds[playerid][i]);
		}
		ActorsIds[playerid][i] = CreateActor(createCharacter_ClassModels[CurrentSelectedSkin{playerid}],
			ActorsPositions[i][0], ActorsPositions[i][1], ActorsPositions[i][2], ActorsPositions[i][3]);
		SetActorVirtualWorld(ActorsIds[playerid][i], VW_OFFSET_CHARSELECT + playerid);

		call OnPlayerEvent(playerid, EVENT_LOADING_PREVIEW_SKIN);
	}

	static Float:cameraPosition[4];
	cameraPosition = ActorsPositions[i];

	GetXYInFrontOfPoint(
		cameraPosition[0], cameraPosition[1], cameraPosition[2], cameraPosition[3], update_skin ? 3.2 : 4.5);
	SetPlayerCameraPos(playerid,
		cameraPosition[0], cameraPosition[1], cameraPosition[2] + 1.3);
	SetPlayerCameraLookAt(playerid, 
		ActorsPositions[i][0], ActorsPositions[i][1], ActorsPositions[i][2], ShouldCameraCut{playerid} ? CAMERA_CUT : CAMERA_MOVE);
	ShouldCameraCut{playerid} = false;
}

hook OnActorStreamIn(actorid, forplayerid) {
	switch(CurrentState(forplayerid)) {
		case STATE_SELECTING_SKIN: {
			new newCharacterIndex = AmountOfActors[forplayerid];

			if(actorid == ActorsIds[forplayerid][newCharacterIndex]) {
				call OnPlayerEvent(forplayerid, EVENT_LOADED_PREVIEW_SKIN);
			}
		}
		case STATE_SELECTING_CHARACTER: {
			new currentSelectedCharacterIndex = CurrentSelectedCharacter{forplayerid};

			if(currentSelectedCharacterIndex < sizeof (ActorsIds[])) {
				if(actorid == ActorsIds[forplayerid][currentSelectedCharacterIndex]) {
					call OnPlayerEvent(forplayerid, EVENT_LOADED_PREVIEW_SKIN);
				}
			}
		}
	}
	return true;
}

hook OnPlayerEvent(playerid, event) {
	M:P:X(playerid, "OnPlayerEvent: %i", event);
	switch(event) {
		case EVENT_GAMESTART_SPAWN: {
			ResetActors(playerid);
			CurrentState(playerid, STATE_NONE);
		}
	}
}

hook OnPlayerDisconnect(playerid, reason) {
	ResetActors(playerid);
	ShouldCameraCut{playerid} = false;
}

hook OnPlayerUpdate(playerid) {
	if(CurrentState(playerid) == STATE_SELECTING_CHARACTER) {
		SelectingCharacterTick(playerid);
	}
	if(CurrentState(playerid) == STATE_SELECTING_SKIN) {
		SelectingSkinTick(playerid);
	}
	return true;
}

public OnPlayerDamageDone(playerid, Float:amount, issuerid, weapon, bodypart) {
	if(issuerid != INVALID_PLAYER_ID) {
		lastTimeInFight[playerid] = gettime();
	}
	#if defined C0_OnPlayerDamageDone
		return C0_OnPlayerDamageDone(playerid, amount, issuerid, weapon, bodypart);
	#else
		return 1;
	#endif
}

#if defined _ALS_OnPlayerDamageDone
	#undef OnPlayerDamageDone
#else
	#define _ALS_OnPlayerDamageDone
#endif
#define OnPlayerDamageDone C0_OnPlayerDamageDone
#if defined C0_OnPlayerDamageDone
	forward C0_OnPlayerDamageDone(playerid, Float:amount, issuerid, weapon, bodypart);
#endif