#include <YSI\y_hooks>

forward OnPlayerEquipItem(playerid, item_name[], Item:uiid);
forward Item:Equipment_GetItemInSlot(playerid, slot);

new Equipment = UNIQUE_SYMBOL;

new Item:EquipmentSlots[MAX_PLAYERS][E_EQUIPMENT_SLOTS];

hook OnResetPlayerVars(playerid) {
	memset(_:EquipmentSlots[playerid], NONE);
}

hook OnInventoryLoaded(playerid) {
	new filename[32];
	format(filename, _, "%i_%s", GetPlayerCharID(playerid), player_Name[playerid]);
	Container_Load(Equipment, playerid, "scriptfiles/equipment", filename);
}

public OnPlayerDeathFinished(playerid, bool:cancelable) {
	if( ! cancelable) {
		TakeOffItems(playerid);
	}
	#if defined C0_OnPlayerDeathFinished
		return C0_OnPlayerDeathFinished(playerid, cancelable);
	#else
		return 1;
	#endif
}

#if defined _ALS_OnPlayerDeathFinished
	#undef OnPlayerDeathFinished
#else
	#define _ALS_OnPlayerDeathFinished
#endif
#define OnPlayerDeathFinished C0_OnPlayerDeathFinished
#if defined C0_OnPlayerDeathFinished
	forward C0_OnPlayerDeathFinished(playerid, bool:cancelable);
#endif

hook OnPlayerSpawn(playerid) {
	ReEquipItems(playerid);
}

hook OnCharacterDespawn(playerid) {
	new filename[32];
	format(filename, _, "%i_%s", GetPlayerCharID(playerid), player_Name[playerid]);
	Container_Save(Equipment, playerid, "scriptfiles/equipment", filename);

	TakeOffItems(playerid);

	Container_Clear(Equipment, playerid);
}

hook OnPlayerUseItem(playerid, item_name[], Item:uiid) {
	if( ! IsSearchingForItem(playerid)) {
		if(IsItemOnPlayer(playerid, uiid)) {
			TakeOffItem(playerid, uiid);
		}
		else {
			EquipItem(playerid, uiid);
		}
	}
}

Item:Equipment_GetItemInSlot(playerid, slot) {
	return EquipmentSlots[playerid][slot];
}

static GetDefaultSlot(Item:item) {
	static item_name[32];
	GetItemName(item, item_name);
	return GetItemDefaultEquipmentSlot(item_name);
}

ReEquipItems(playerid) {
	foreach(new i : Container(Equipment, playerid)) {
		EquipItem(playerid, Item:i, .already_equiped = true);
	}
}

HasItemEquiped(playerid, item_name[]) {
	return Container_HasItem(Equipment, playerid, item_name);
}

IsItemOnPlayer(playerid, Item:uiid) {
	return Container_GetItemSlot(Equipment, playerid, uiid) != NONE;
}

EquipItem(playerid, Item:item, slot = NONE, bool:already_equiped = false) {
	if(slot == NONE) {
		slot = GetDefaultSlot(item);
	}
	if(slot == Slot_None) {
		return false;
	}

	M:P:D(playerid, "EquipItem: playerid %i, item %i, slot %i, already_equiped %i", playerid, _:item, slot, _:already_equiped);

	new Item:prev_item;

	if( ! already_equiped) {
		TakeOffItem(playerid, (prev_item = Equipment_GetItemInSlot(playerid, slot)));

		Container_AddItem(Equipment, playerid, item);
		
		SetItemInfo(item, "equipment_slot", slot);
	}

	new item_name[32];
	GetItemName(item, item_name);

	if(call OnPlayerEquipItem(playerid, item_name, _:item)) {
		EquipmentSlots[playerid][slot] = item;
	}
	else {
		if(prev_item != Item:NONE && prev_item) {
			EquipItem(playerid, prev_item, slot);
		}
		else {
			TakeOffItem(playerid, item);
		}
	}

	if(IsInventoryOpen(playerid)) {
		Inv_Update(playerid);
	}
	return true;
}

TakeOffItems(playerid) {
	foreach(new i : Container(Equipment, playerid)) {
		TakeOffItem(playerid, Item:i, .just_call = true);
	}
}

TakeOffItem(playerid, Item:item = Item:NONE, slot = NONE, just_call = false) {
	M:P:D(playerid, "TakeOffItem: playerid %i, item %i, slot %i, just_call %i", playerid, _:item, slot, just_call);

	new item_name[32];

	GetItemName(item, item_name);

	if(slot == NONE) {
		slot = GetItemDefaultEquipmentSlot(item_name);
	}
	else {
		item = Equipment_GetItemInSlot(playerid, slot);
	}
	if(item == Item:NONE || ! item) {
		return false;
	}
	if( ! IsItemOnPlayer(playerid, item)) {
		return false;
	}

	if( ! just_call) {
		Container_AddItem(Player, playerid, item);
		DeleteItemInfo(item, "equipment_slot");
		EquipmentSlots[playerid][slot] = Item:NONE;
	}

	call OnPlayerTakeOffItem(playerid, item_name, _:item);

	if(IsInventoryOpen(playerid)) {
		Inv_Update(playerid);
	}
	return true;
}