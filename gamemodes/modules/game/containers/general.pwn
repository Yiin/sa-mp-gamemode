AddItemToContainer(containerType, containerId, Item:uiid, amount = 1, playerid = INVALID_PLAYER_ID) {
	static item_name[32];

	GetItemName(uiid, item_name);

	static Item:e_uiid; // existing uiid

	if( // Daikt� galima tur�ti daugiau negu 1 vienoje eilut�je
		GetItemDefaultFlags(item_name) & Stackable
	) {
		if( // Inventoriuje jau yra toks daiktas
			(e_uiid = Container_FindItem(containerType, containerId, item_name)) && e_uiid != Item:NONE
		) {
			// Ir i� to konteinerio norime perduoti ne visus daiktus
			if(amount < GetItemInfo(uiid, "count")) {
				AdjustItemInfo(uiid, "count", -amount);

				SetItemInfo(e_uiid, "count", max(GetItemInfo(e_uiid, "count"), 1) + amount);
			}
			else {
				// Norim perd�ti visus daiktus, bet kadangi pas mus inventoriuje toks jau yra
				// tai negalim tiesiog pakeisti daikto konteinerio
				// Reikia pakeisti esamo daikto kiek�
				SetItemInfo(e_uiid, "count", max(GetItemInfo(e_uiid, "count"), 1) + amount);

				// Ir i�trinti daikt� i� seno konteinerio
				DeleteItem(uiid);
			}
			
			if(playerid != INVALID_PLAYER_ID) {
				if(amount > 1) {
					M:P:G(playerid, "[highlight]%s[] (x[number]%i[]) �d�tas � inventori�. (Dabar yra: [number]%i[])", item_name, amount, GetItemInfo(e_uiid, "count"));
				}
				else {
					M:P:G(playerid, "[highlight]%s[] �d�tas � inventori�. (Dabar yra: [number]%i[])", item_name, GetItemInfo(e_uiid, "count"));
				}
			}
		}
		else { // Inventoriuje tokio daikto dar n�ra
			// Ir norime perduoti tik dal� daikt� i� preito inventoriaus
			if(amount < GetItemInfo(uiid, "count")) {
				AdjustItemInfo(uiid, "count", -amount);

				// Sukuriam nauj�
				new Item:n_uiid = CreateItem(item_name);

				// Nustatom kiek� kiek norime perduoti
				SetItemInfo(n_uiid, "count", amount);

				// J� �dedam � �� konteiner�
				Container_AddItem(containerType, containerId, n_uiid);
			}
			else {
				// Norim tiesiog perkelti visus daiktus � inventori�
				Container_AddItem(containerType, containerId, uiid);
			}
		}
	}
	else { // Daiktas nesistackina
		Container_AddItem(containerType, containerId, uiid);

		if(playerid != INVALID_PLAYER_ID) {
			M:P:G(playerid, "[highlight]%s[] �d�tas � inventori�.", item_name);
		}
	}
}