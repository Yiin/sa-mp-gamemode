#include <YSI_Coding\y_hooks>

#define MAX_STEPS 30

enum {
	GUARDIAN_TYPE_MARK_WAYNE,
	GUARDIAN_TYPE_VERY_WEAK,
	GUARDIAN_TYPE_WEAK,
	GUARDIAN_TYPE_NORMAL,
	GUARDIAN_TYPE_STRONG,
	GUARDIAN_TYPE_VERY_STRONG
};

enum _:Guardian_e_DATA {
	guardLevel,

	currentTarget, // � k� �audom?

	Timer:shootingTimer,

	shootingRate, // kokiu grei�iu �audom?

	bool:forceAttack,

	Float:securedPoint[3] // kok� ta�k� saugom?
};
static Guardian[MAX_PLAYERS][Guardian_e_DATA];

new Iterator:Guardians<MAX_PLAYERS>;

hook OnGameModeInit() {
	repeat CheckGuardians();
}

timer CheckGuardians[1000]() {
	foreach(new i : Guardians) {
		if(FCNPC_IsMoving(i)) {
			searchForTarget(i);
		}
		else if(FCNPC_IsAiming(i)) {
			attackTarget(i);
		}
		else if( ! FCNPC_IsMoving(i)) {
			if(GetPoint(i)) {
				SetGuardianPoint(i);
			}
		}
	}
}

hook FCNPC_OnTakeDamage(npcid, damagerid, weaponid, bodypart, Float:health_loss) {
	if(FCNPC_IsDead(npcid)) {
		return;
	}
	static Float:x, Float:y, Float:z;
	FCNPC_GetPosition(npcid, x, y, z);

	if( ! IsPlayerInRangeOfPoint(damagerid, 35.0, x, y, z)) {
		attackTarget(npcid, damagerid, true);
	}
}

hook FCNPC_OnDeath(npcid, killerid, weaponid) {
	Iter_Remove(Guardians, npcid);
	return true;
}

CreateGuardian(type, Float:x, Float:y, Float:z, skin, weapon) {
	new Float:health;
	new Float:shootRate_mult;
	GetGuardianStats(type, health, shootRate_mult);

	static uid = 1;	
	new gid = FCNPC_Create(F:0("%i:GUARDIAN", uid++));

	if(gid == INVALID_PLAYER_ID) {
		return 0;
	}

	FCNPC_Spawn(gid, skin, x, y, z);
	FCNPC_SetWeapon(gid, weapon);
	FCNPC_SetAmmo(gid, 1000);
	FCNPC_ToggleInfiniteAmmo(gid, true);
	FCNPC_ToggleReloading(gid, true);

	SetPlayerMaxHealth(gid, health);
	UpdatePlayerHealth(gid, health, true);

	Guardian[gid][guardLevel] = type;
	Guardian[gid][currentTarget] = INVALID_PLAYER_ID;
	Guardian[gid][forceAttack] = false;
	Guardian[gid][securedPoint][0] = 0.0;
	Guardian[gid][securedPoint][1] = 0.0;
	Guardian[gid][securedPoint][2] = 0.0;
	Guardian[gid][shootingTimer] = Timer:NONE;
	Guardian[gid][shootingRate] = floatround(shootRate_mult * GetWeaponShootRate(weapon));

	Iter_Add(Guardians, gid);

	return gid;
}

RemoveGuardian(gid) {
	Iter_Remove(Guardians, gid);
	FCNPC_Destroy(gid);
}

GetGuardianLevel(gid) {
	return Guardian[gid][guardLevel];
}

SetGuardianPoint(gid, Float:x = 0.0, Float:y = 0.0, Float:z = 0.0) {
	if(x || y || z) {
		Guardian[gid][securedPoint][0] = x;
		Guardian[gid][securedPoint][1] = y;
		Guardian[gid][securedPoint][2] = z;
	}
	else {
		x = Guardian[gid][securedPoint][0];
		y = Guardian[gid][securedPoint][1];
		z = Guardian[gid][securedPoint][2];
	}
	new Float:offsetX = floatrandom(15) - floatrandom(15);
	new Float:offsetY = floatrandom(15) - floatrandom(15);

	static Float:X, Float:Y, Float:Z;
	FCNPC_GetPosition(gid, X, Y, Z);

	FCNPC_GoTo(gid, x + offsetX, y + offsetY, z + 0.5, MOVE_TYPE_WALK, .UseMapAndreas = true);

	//PathFinder_FindWay(gid, X, Y, x + offsetX, y + offsetY, .stepLimit = MAX_STEPS);
}

hook FCNPC_OnReachDest(npcid) {
	SetGuardianPoint(npcid);
	return true;
}

timer tryToShoot[300](gid) {
	static Float:x, Float:y, Float:z;
	static Float:x2, Float:y2, Float:z2;
	GetPlayerPos(GetTarget(gid), x, y, z);
	FCNPC_GetPosition(gid, x2, y2, z2);

	new force = Guardian[gid][forceAttack];

	if(force || IsPlayerInRangeOfPoint(GetTarget(gid), 35.0, x2, y2, z2)) {
		new bool:shoot = bool:IsPlayerInRangeOfPoint(GetTarget(gid), 20.0, x2, y2, z2);
		new is_aiming;
		if((force || shoot) && ! FCNPC_IsShooting(gid)) {
			is_aiming = FCNPC_AimAtPlayer(gid, GetTarget(gid), shoot, GetShootingRate(gid));
		}
		else {
			if( ! shoot && FCNPC_IsShooting(gid)) {
				FCNPC_StopAim(gid);
			}
			is_aiming = FCNPC_AimAtPlayer(gid, GetTarget(gid), shoot, GetShootingRate(gid));
		}
		if( ! is_aiming) {
			ResetGuardian(gid);
		}
	}
	else {
		ResetGuardian(gid);
	}
}

static ResetGuardian(gid) {
	Guardian[gid][forceAttack] = false;
	SetShootingTimer(gid, Timer:NONE);
	FCNPC_StopAim(gid);
	FCNPC_StopAttack(gid);
	FCNPC_Stop(gid);
}

static GetGuardianStats(type, &Float:health, &Float:shootRate_mult) {
	switch(type) {
		case GUARDIAN_TYPE_MARK_WAYNE: {
			health = 50.0;
			shootRate_mult = 3.0;
		}
		case GUARDIAN_TYPE_VERY_WEAK: {
			health = 100.0;
			shootRate_mult = 2.5;
		}
		case GUARDIAN_TYPE_WEAK: {
			health = 200.0;
			shootRate_mult = 2.0;
		}
		case GUARDIAN_TYPE_NORMAL: {
			health = 400.0;
			shootRate_mult = 1.5;
		}
		case GUARDIAN_TYPE_STRONG: {
			health = 800.0;
			shootRate_mult = 1.0;
		}
		case GUARDIAN_TYPE_VERY_STRONG: {
			health = 2000.0;
			shootRate_mult = 0.6;
		}
	}
}

hook OnGuardianDetectTarget(gid, target) {
	return !! GetPlayerWeapon(target);
}

static searchForTarget(gid) {
	static Float:x, Float:y, Float:z;
	FCNPC_GetPosition(gid, x, y, z);

	foreach(new i : Player) {
		if(IsPlayerInRangeOfPoint(i, 30.0, x, y, z)) {
			static Float:tx, Float:ty, Float:tz;
			GetPlayerPos(i, tx, ty, tz);

			if(CA_RayCastLine(x, y, z, tx, ty, tz, tx, ty, tz)) {
				continue;
			}
			if(call OnGuardianDetectTarget(gid, i)) {
				attackTarget(gid, i);
				return false;
			}
		}
	}
	return true;
}

static attackTarget(gid, target = NONE, bool:force = false) {
	if(target != NONE) {
		if(FCNPC_IsMoving(gid)) {
			FCNPC_Stop(gid);
		}
		SetTarget(gid, target, force);

		SetShootingTimer(gid, repeat tryToShoot(gid));
	}
	else if((target = GetTarget(gid)) != INVALID_PLAYER_ID) {
		static Float:x, Float:y, Float:z;
		FCNPC_GetPosition(gid, x, y, z);

		if( ! IsPlayerInRangeOfPoint(target, 30.0, x, y, z)) {
			ResetGuardian(gid);
		}
	}
}

static GetPoint(gid) {
	new Float:x = Guardian[gid][securedPoint][0];
	new Float:y = Guardian[gid][securedPoint][1];
	new Float:z = Guardian[gid][securedPoint][2];

	return x || y || z;
}
static ResetTarget(gid) {
	Guardian[gid][currentTarget] = INVALID_PLAYER_ID;
}
static SetTarget(gid, targetid, bool:force = false) {
	Guardian[gid][currentTarget] = targetid;
	Guardian[gid][forceAttack] = force;
}
static GetTarget(gid) {
	return Guardian[gid][currentTarget];
}
static GetShootingRate(gid) {
	return Guardian[gid][shootingRate];
}
static SetShootingTimer(gid, Timer:timerid) {
	if(Guardian[gid][shootingTimer] != Timer:NONE) {
		stop Guardian[gid][shootingTimer];
	}
	Guardian[gid][shootingTimer] = timerid;
}