#include <YSI\y_hooks>

hook OnGameModeInit() {
	MapAndreas_Init(MAP_ANDREAS_MODE_FULL);
	PathFinder_Init(MapAndreas_GetAddress());
	FCNPC_InitMapAndreas(MapAndreas_GetAddress());
}