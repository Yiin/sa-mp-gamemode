#include <YSI_Coding\y_hooks>

hook FCNPC_OnTakeDamage(npcid, damagerid, weaponid, bodypart, Float:health_loss) {
	if(FCNPC_IsDead(npcid)) {
		return;
	}
	UpdatePlayerHealth(npcid, -health_loss);
}

hook FCNPC_OnDeath(npcid, killerid, weaponid) {
	defer RemoveNpcAfterDeath(npcid);
}

timer RemoveNpcAfterDeath[2000](npcid) {
	FCNPC_Destroy(npcid);
}