#include <YSI\y_hooks>

hook OnPlOpenTargetVehMenu(playerid, vehicleid) {
	if(player_Job[playerid] == JOB_MECH || player_Admin[playerid] == YIIN) {
		// Jeigu ma�inos stabd�iai yra bent 10% susidev�j�, juos galima sutaisyti
		if(vehicle_ErrorState[vehicleid][eVehicleErrorState_Brakes] > MAX_VEHICLE_BRAKES_DAMAGE * 0.1) {
			dialogAddOption("Taisyti stabd�ius");
		}
		new Float:health; GetVehicleHealth(vehicleid, health);
		if(500.0 < health < 1000.0) {
			dialogAddOption("Tvarkyti varikl�");
		}
		else if(health <= 500.0) {
			dialogAddOption("Sutvarkyti varikl� (tik gara�e)");
		}
	}
}

hook OnPlSelectTargetVehMenu(playerid, vehicleid, li) {
	if(player_Job[playerid] == JOB_MECH) {
		dialog_Row("Taisyti stabd�ius") {
			fixBrakes(playerid, vehicleid);
		}
		dialog_Row("Tvarkyti varikl�") {
			fixEngine(playerid, vehicleid);
		}
	}
}

fixBrakes(playerid, vehicleid) {
	inline onStart() {
		M:P:I(playerid, "Prad�jai tvarkyti [highlight]%s[] stabd�ius..", vehicle_Name[GetVehicleModel(vehicleid) - 400]);
		
		ApplyAnimation(playerid, "MEDIC", "CPR", 4.0, 0, 0, 0, 0, 0, 1);
	}
	inline onTick() {
		confirmProcess(playerid);
	}
	inline onSuccess() {
		M:P:G(playerid, "S�kmingai sutvarkei tr. priemon�s stabd�ius!");
		vehicle_ErrorState[vehicleid][eVehicleErrorState_Brakes] = 0;
	}	
	inline onFail() {
		M:P:E(playerid, "Nepavyko sutvarkyti tr. priemon�s stabd�i�.");
	}
	inline onEnd() {
		ClearAnimations(playerid);
	}
	SetProgressTitle(playerid, "Tvarkai stabdzius");

	SetProgressCallback(playerid, PROGRESS_CALLBACK_START, using inline onStart);
	SetProgressCallback(playerid, PROGRESS_CALLBACK_TICK, using inline onTick);
	SetProgressCallback(playerid, PROGRESS_CALLBACK_SUCCESS, using inline onSuccess);
	SetProgressCallback(playerid, PROGRESS_CALLBACK_FAIL, using inline onFail);
	SetProgressCallback(playerid, PROGRESS_CALLBACK_END, using inline onEnd);

	StartProgress(playerid, DURATION(5 seconds));
}

fixEngine(playerid, vehicleid) {
	static Float:health; GetVehicleHealth(vehicleid, health);
	
	inline onStart() {
		M:P:I(playerid, "Prad�jai tvarkyti [highlight]%s[] varikl�..", vehicle_Name[GetVehicleModel(vehicleid) - 400]);

		ApplyAnimation(playerid, "MEDIC", "CPR", 4.0, 0, 0, 0, 0, 0, 1);
	}
	inline onTick() {
		confirmProcess(playerid);
	}
	inline onSuccess() {
		M:P:G(playerid, "S�kmingai sutvarkei tr. priemon�s varikl�!");
		SetVehicleHealth(vehicleid, 1000.0);
	}
	inline onFail() {
		M:P:E(playerid, "Nepavyko sutvarkyti tr. priemon�s variklio.");
	}
	inline onEnd() {
		ClearAnimations(playerid);
	}
	SetProgressTitle(playerid, "Tvarkai varikli");

	SetProgressCallback(playerid, PROGRESS_CALLBACK_START, using inline onStart);
	SetProgressCallback(playerid, PROGRESS_CALLBACK_TICK, using inline onTick);
	SetProgressCallback(playerid, PROGRESS_CALLBACK_SUCCESS, using inline onSuccess);
	SetProgressCallback(playerid, PROGRESS_CALLBACK_FAIL, using inline onFail);
	SetProgressCallback(playerid, PROGRESS_CALLBACK_END, using inline onEnd);
	StartProgress(playerid, DURATION(1 second) * floatround((1000.0 - health) / 50.0, floatround_ceil));
}

static confirmProcess(playerid) {
	static keys, unused;
	GetPlayerKeys(playerid, keys, unused, unused);

	static animlib[32], animname[32];
	GetAnimationName(GetPlayerAnimationIndex(playerid), animlib, sizeof animlib, animname, sizeof animname);

	if(strcmp(animname, "CPR") || ! (keys & KEY_FIRE)) {
		if(strcmp(animname, "CPR")) {
			ApplyAnimation(playerid, "MEDIC", "CPR", 4.0, 0, 0, 0, 0, 0, 1);
		}
		IgnoreProgressTick();
	}
}