/*
 * Hospital / Inventory control
 *
 * Neleid�iam �aid�jui naudoti joki� item� gulint ligonin�je.
 * Vienintel� i�imtis yra MEDICINE_HR itemas, kuris yra vaistas pagreitinantis gyjim� ligonin�je.
 */

#include <YSI\y_hooks>

// �aid�jo paguldymo � ligonin� laikas
static hospitalizedAt[MAX_PLAYERS];

// Jeigu �aid�jas bando panaudoti daikt�, patikrinam ar jam galima j� naudoti
hook OnPlayerTriesToUseItem(playerid, item_name[], Item:uiid) {
	return CanPlayerUseItem(playerid, item_name);
}

// Leid�iam daiktus naudoti tik jeigu nuo paguldymo � ligonin� pra�jo nustatytas laiko tarpas
static CanPlayerUseItem(playerid, item_name[]) {
	// pra�jo nustatytas laiko tarpas, �aid�jas gali naudoti k� nori
	if(gettime() > GetPlayerHospitalizationTime(playerid) + DURATION(15 minutes)) {
		return true;
	}
	switch(GetItemDefaultType(item_name)) {
		// jeigu itemas gali tur�ti koki� nors gyvybes regeneruojan�i� efekt�
		case Food, Medicine, Cure, Drug: {
			// ir jeigu itemas n�ra MEDICINE_HR
			if(strcmp(item_name, MEDICINE_HR)) {
				// neleid�iam jo naudoti
				return false;
			}
		}
	}
	// kitu atveju mums px
	return true;
}

// kad �aid�jo paguldymo laik� b�t� galima su�inoti ne tik �iame faile
GetPlayerHospitalizationTime(playerid) {
	return hospitalizedAt[playerid];
}

// Kai �aid�j� paguldo � ligonin�, i�sisaugom jo paguldymo laik�
hook OnPlayerEvent(playerid, event) {
	if(event == EVENT_HOSPITALIZED) {
		// jeigu �aid�jas vis dar guli ligonin�je, nenustatom laiko i� naujo
		// nes galimai �aid�jas k� tik prisijung� ir gul�jo ligonin�je jau anks�iau
		hospitalizedAt[playerid] = hospitalizedAt[playerid] || gettime();
	}
}

hook OnPlayerEvent(playerid, event) {
	if(event == EVENT_DISCHARGED_FROM_HOSPITAL) {
		hospitalizedAt[playerid] = 0;
	}
}

// Taip pat i�saugom �i� info prie kit� �aid�jo duomen�, kad infomacija nedingt� jam atsijungus 
hook OnCreatePlayerORM(ORM:ormid, playerid) {
	orm_addvar_int(ormid, hospitalizedAt[playerid], "hospitalized_at");
}