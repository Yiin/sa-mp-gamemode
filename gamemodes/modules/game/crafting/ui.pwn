//Global TextDraws: 

enum _:CATEGORIES_COUNT {
	CRAFTING_CATEGORY_WEAPONS,
	CRAFTING_CATEGORY_MEDICINE,
	CRAFTING_CATEGORY_DRUGS,
	CRAFTING_CATEGORY_MECH,
	CRAFTING_CATEGORY_FURNITURE
};
const LIST_ROW_COUNT = 13;
const COMPONENT_ROW_COUNT = 6;

const BG_COLOR_DEFAULT = 168434687;
const BG_COLOR_SELECTED = 336864767;
const TXT_COLOR_DEFAULT = -126;
const TXT_COLOR_SELECTED = -1;

static
	Text:TD_FixTransparentSprites,
	Text:TD_MainBackground,
	Text:TD_HeaderBackground,
	Text:TD_HeaderTitle,
	Text:TD_CloseEsc,
	Text:TD_CategoriesTitle,
	Text:TD_CategoryBtnBackground[CATEGORIES_COUNT],
	Text:TD_CategoryName[CATEGORIES_COUNT],
	Text:TD_ListBackground,
	Text:TD_ListForeground,
	Text:TD_ListRowBackground[LIST_ROW_COUNT],
	Text:TD_CheckBoxBackground[LIST_ROW_COUNT],
	Text:TD_CheckBoxForeground[LIST_ROW_COUNT],
	Text:TD_CheckBoxMark[LIST_ROW_COUNT],
	Text:TD_RecipeComponentsBackground,
	Text:TD_RecipeInfoModelBackground,
	Text:TD_ComponentBackground[COMPONENT_ROW_COUNT],
	Text:TD_ComponentModelBackground[COMPONENT_ROW_COUNT],
	Text:TD_ComponentTextBackground[COMPONENT_ROW_COUNT],
	Text:TD_FilterBtnBackground,
	Text:TD_CraftAllBtnBackground,
	Text:TD_CraftAllBtnText,
	Text:TD_AmountSelectionBackground,
	Text:TD_AmountDecreaseBtnBackground,
	Text:TD_AmountIncreaseBtnBackground,
	Text:TD_AmountDecreaseBtnText,
	Text:TD_AmountIncreaseBtnText,
	Text:TD_AmountToCraftTextBackground,
	Text:TD_CraftBtnBackground,
	Text:TD_CraftBtnText,
	Text:TD_ShowPreviousPageBtn,
	Text:TD_ShowNextPageBtn
;

static 
	PlayerText:PlayerTD_ListRowName[MAX_PLAYERS][LIST_ROW_COUNT],
	PlayerText:PlayerTD_ListRowRequiredLevel[MAX_PLAYERS][LIST_ROW_COUNT],
	PlayerText:PlayerTD_RecipeInfoName[MAX_PLAYERS],
	PlayerText:PlayerTD_RecipeInfoModel[MAX_PLAYERS],
	PlayerText:PlayerTD_RecipeInfoAmount[MAX_PLAYERS],
	PlayerText:PlayerTD_RecipeInfoDescription[MAX_PLAYERS],
	PlayerText:PlayerTD_RecipeComponentModel[MAX_PLAYERS][COMPONENT_ROW_COUNT],
	PlayerText:PlayerTD_RecipeComponentName[MAX_PLAYERS][COMPONENT_ROW_COUNT],
	PlayerText:PlayerTD_RecipeComponentAmount[MAX_PLAYERS][COMPONENT_ROW_COUNT],
	PlayerText:PlayerTD_FilterText[MAX_PLAYERS],
	PlayerText:PlayerTD_AmountOfItemsToCraft[MAX_PLAYERS],
	PlayerText:PlayerTD_CurrentPage[MAX_PLAYERS],
	PlayerText:PlayerTD_CategoryExperience[MAX_PLAYERS]
;

hook OnCraftingDisplay(playerid) {
	TextDrawShowForPlayer(playerid, TD_HeaderTitle);
	TextDrawShowForPlayer(playerid, TD_MainBackground);
	TextDrawShowForPlayer(playerid, TD_HeaderBackground);
	TextDrawShowForPlayer(playerid, TD_CloseEsc);
	TextDrawShowForPlayer(playerid, TD_CategoriesTitle);

	for(new i; i < CATEGORIES_COUNT; ++i) {
		TextDrawShowForPlayer(playerid, TD_CategoryBtnBackground[i]);
		TextDrawShowForPlayer(playerid, TD_CategoryName[i]);
	}

	TextDrawShowForPlayer(playerid, TD_ListBackground);
	TextDrawShowForPlayer(playerid, TD_ListForeground);

	for(new i; i < COMPONENT_ROW_COUNT; ++i) {
		TextDrawShowForPlayer(playerid, TD_ComponentBackground[i]);
		TextDrawShowForPlayer(playerid, TD_ComponentModelBackground[i]);
		TextDrawShowForPlayer(playerid, TD_ComponentTextBackground[i]);
		PlayerTextDrawShow(playerid, PlayerTD_RecipeComponentModel[playerid][i]);
		PlayerTextDrawShow(playerid, PlayerTD_RecipeComponentName[playerid][i]);
		PlayerTextDrawShow(playerid, PlayerTD_RecipeComponentAmount[playerid][i]);
	}
	TextDrawShowForPlayer(playerid, TD_FilterBtnBackground);
	TextDrawShowForPlayer(playerid, TD_CraftAllBtnBackground);
	TextDrawShowForPlayer(playerid, TD_CraftAllBtnText);
	TextDrawShowForPlayer(playerid, TD_AmountSelectionBackground);
	TextDrawShowForPlayer(playerid, TD_AmountDecreaseBtnBackground);
	TextDrawShowForPlayer(playerid, TD_AmountIncreaseBtnBackground);
	TextDrawShowForPlayer(playerid, TD_AmountDecreaseBtnText);
	TextDrawShowForPlayer(playerid, TD_AmountIncreaseBtnText);

	TextDrawShowForPlayer(playerid, TD_AmountToCraftTextBackground);
	TextDrawShowForPlayer(playerid, TD_CraftBtnBackground);
	TextDrawShowForPlayer(playerid, TD_CraftBtnText);
	TextDrawShowForPlayer(playerid, TD_ShowPreviousPageBtn);
	TextDrawShowForPlayer(playerid, TD_ShowNextPageBtn);
	PlayerTextDrawShow(playerid, PlayerTD_FilterText[playerid]);
	PlayerTextDrawShow(playerid, PlayerTD_AmountOfItemsToCraft[playerid]);
	PlayerTextDrawShow(playerid, PlayerTD_CurrentPage[playerid]);
	PlayerTextDrawShow(playerid, PlayerTD_CategoryExperience[playerid]);
}

ShowCraftingListRow(playerid, index, const name[], required_level = 0, has_required_materials) {
	// parodom list row background�
	TextDrawShowForPlayer(playerid, TD_ListRowBackground[index]);
	TextDrawShowForPlayer(playerid, TD_CheckBoxBackground[index]);

	// daikto pavadinim� ir reikaling� lyg�
	TextDrawSetString(PlayerTD_ListRowName[playerid][index], name);
	if(required_level) {
		TextDrawSetString(PlayerTD_ListRowRequiredLevel[playerid][index], F:0("%i", required_level));
	}
	PlayerTextDrawShow(playerid, PlayerTD_ListRowName[playerid][index]);
	PlayerTextDrawShow(playerid, PlayerTD_ListRowRequiredLevel[playerid][index]);

	// parodom checkboxo background
	TextDrawShowForPlayer(playerid, TD_CheckBoxForeground[index]);

	// ir kad galima craftinti jeigu turim visus amterials
	if(has_required_materials) {
		TextDrawShowForPlayer(playerid, TD_CheckBoxMark[index]);
	}
}

// pasl�piam visus eilutes
HideAllCraftingListRows(playerid) {
	for(new i; i < LIST_ROW_COUNT; ++i) {
		HideCraftingListRow(playerid, i);
	}
}

// pasl�piam specifin� eilut�
HideCraftingListRow(playerid, index) {
	TextDrawHideForPlayer(playerid, TD_ListRowBackground[index]);
	TextDrawHideForPlayer(playerid, TD_CheckBoxBackground[index]);
	PlayerTextDrawHide(playerid, PlayerTD_ListRowName[playerid][index]);
	PlayerTextDrawHide(playerid, PlayerTD_ListRowRequiredLevel[playerid][index]);
	TextDrawHideForPlayer(playerid, TD_CheckBoxForeground[index]);
	TextDrawHideForPlayer(playerid, TD_CheckBoxMark[index]);
}

ShowCraftingRecipeInfo(playerid, const name[], const amount[], const description[]) {
	TextDrawShowForPlayer(playerid, TD_RecipeComponentsBackground);
	TextDrawShowForPlayer(playerid, TD_RecipeInfoModelBackground);

	// recepto pavadinimas
	PlayerTextDrawSetString(playerid, PlayerTD_RecipeInfoName[playerid], name);
	PlayerTextDrawShow(playerid, PlayerTD_RecipeInfoName[playerid]);

	// pagaminamas ma�iausias kietis
	PlayerTextDrawSetString(playerid, PlayerTD_RecipeInfoAmount[playerid], amount);
	PlayerTextDrawShow(playerid, PlayerTD_RecipeInfoAmount[playerid]);

	// apra�ymas arba detalesn� informacija
	PlayerTextDrawSetString(playerid, PlayerTD_RecipeInfoDescription[playerid], description);
	PlayerTextDrawShow(playerid, PlayerTD_RecipeInfoDescription[playerid]);
}

HideCraftingRecipeInfo(playerid) {
	TextDrawHideForPlayer(playerid, TD_RecipeComponentsBackground);
	TextDrawHideForPlayer(playerid, TD_RecipeInfoModelBackground);
	PlayerTextDrawHide(playerid, PlayerTD_RecipeInfoName[playerid]);
	PlayerTextDrawHide(playerid, PlayerTD_RecipeInfoAmount[playerid]);
	PlayerTextDrawHide(playerid, PlayerTD_RecipeInfoDescription[playerid]);
}

UpdateCraftingListPage(playerid, page, arrows) {
	PlayerTextDrawSetString(playerid, PlayerTD_CurrentPage[playerid], F:0("%i", page));
	PlayerTextDrawShow(playerid, PlayerTD_CurrentPage[playerid]);
}

CMD:hh(playerid, params[]) {
	TextDrawHideForPlayer(playerid, TD_HeaderTitle);
	TextDrawHideForPlayer(playerid, TD_MainBackground);
	TextDrawHideForPlayer(playerid, TD_HeaderBackground);
	TextDrawHideForPlayer(playerid, TD_CloseEsc);
	TextDrawHideForPlayer(playerid, TD_CategoriesTitle);

	for(new i; i < CATEGORIES_COUNT; ++i) {
		TextDrawHideForPlayer(playerid, TD_CategoryBtnBackground[i]);
		TextDrawHideForPlayer(playerid, TD_CategoryName[i]);
	}

	TextDrawHideForPlayer(playerid, TD_ListBackground);
	TextDrawHideForPlayer(playerid, TD_ListForeground);

	for(new i; i < LIST_ROW_COUNT; ++i) {
		TextDrawHideForPlayer(playerid, TD_ListRowBackground[i]);
		TextDrawHideForPlayer(playerid, TD_CheckBoxBackground[i]);
		TextDrawHideForPlayer(playerid, TD_CheckBoxForeground[i]);
		TextDrawHideForPlayer(playerid, TD_CheckBoxMark[i]);
		PlayerTextDrawHide(playerid, PlayerTD_ListRowName[playerid][i]);
		PlayerTextDrawHide(playerid, PlayerTD_ListRowRequiredLevel[playerid][i]);
	}

	TextDrawHideForPlayer(playerid, TD_RecipeComponentsBackground);
	TextDrawHideForPlayer(playerid, TD_RecipeInfoModelBackground);
	PlayerTextDrawHide(playerid, PlayerTD_RecipeInfoAmount[playerid]);
	PlayerTextDrawHide(playerid, PlayerTD_RecipeInfoDescription[playerid]);

	for(new i; i < COMPONENT_ROW_COUNT; ++i) {
		TextDrawHideForPlayer(playerid, TD_ComponentBackground[i]);
		TextDrawHideForPlayer(playerid, TD_ComponentModelBackground[i]);
		TextDrawHideForPlayer(playerid, TD_ComponentTextBackground[i]);
		PlayerTextDrawHide(playerid, PlayerTD_RecipeComponentModel[playerid][i]);
		PlayerTextDrawHide(playerid, PlayerTD_RecipeComponentName[playerid][i]);
		PlayerTextDrawHide(playerid, PlayerTD_RecipeComponentAmount[playerid][i]);
	}
	TextDrawHideForPlayer(playerid, TD_FilterBtnBackground);
	TextDrawHideForPlayer(playerid, TD_CraftAllBtnBackground);
	TextDrawHideForPlayer(playerid, TD_CraftAllBtnText);
	TextDrawHideForPlayer(playerid, TD_AmountSelectionBackground);
	TextDrawHideForPlayer(playerid, TD_AmountDecreaseBtnBackground);
	TextDrawHideForPlayer(playerid, TD_AmountIncreaseBtnBackground);
	TextDrawHideForPlayer(playerid, TD_AmountDecreaseBtnText);
	TextDrawHideForPlayer(playerid, TD_AmountIncreaseBtnText);

	TextDrawHideForPlayer(playerid, TD_AmountToCraftTextBackground);
	TextDrawHideForPlayer(playerid, TD_CraftBtnBackground);
	TextDrawHideForPlayer(playerid, TD_CraftBtnText);
	TextDrawHideForPlayer(playerid, TD_ShowPreviousPageBtn);
	TextDrawHideForPlayer(playerid, TD_ShowNextPageBtn);
	PlayerTextDrawHide(playerid, PlayerTD_FilterText[playerid]);
	PlayerTextDrawHide(playerid, PlayerTD_AmountOfItemsToCraft[playerid]);
	PlayerTextDrawHide(playerid, PlayerTD_CurrentPage[playerid]);
	PlayerTextDrawHide(playerid, PlayerTD_CategoryExperience[playerid]);
	return true;
}

hook OnPlayerEvent(playerid, event) {
	if(event == EVENT_CONNECTED) {
		TextDrawShowForPlayer(playerid, TD_FixTransparentSprites);
	}
}

hook OnGameModeInit() {
	TD_FixTransparentSprites = TextDrawCreate(1.0, 1.0, "fix");
	TextDrawLetterSize(TD_FixTransparentSprites, 0.000000, 0.000000);

	TD_MainBackground = TextDrawCreate(167.000030, 104.807327, "LD_SPAC:white");
	TextDrawLetterSize(TD_MainBackground, 0.000000, 0.000000);
	TextDrawTextSize(TD_MainBackground, 354.000000, 296.000000);
	TextDrawAlignment(TD_MainBackground, 1);
	TextDrawColor(TD_MainBackground, 4569);
	TextDrawSetShadow(TD_MainBackground, 0);
	TextDrawSetOutline(TD_MainBackground, 0);
	TextDrawBackgroundColor(TD_MainBackground, 255);
	TextDrawFont(TD_MainBackground, 4);
	TextDrawSetProportional(TD_MainBackground, 0);
	TextDrawSetShadow(TD_MainBackground, 0);

	TD_HeaderBackground = TextDrawCreate(168.333404, 106.996284, "LD_SPAC:white");
	TextDrawLetterSize(TD_HeaderBackground, 0.000000, 0.000000);
	TextDrawTextSize(TD_HeaderBackground, 351.000000, 16.000000);
	TextDrawAlignment(TD_HeaderBackground, 1);
	TextDrawColor(TD_HeaderBackground, -250);
	TextDrawSetShadow(TD_HeaderBackground, 0);
	TextDrawSetOutline(TD_HeaderBackground, 0);
	TextDrawBackgroundColor(TD_HeaderBackground, 255);
	TextDrawFont(TD_HeaderBackground, 4);
	TextDrawSetProportional(TD_HeaderBackground, 0);
	TextDrawBoxColor(TD_HeaderBackground, 255);
	TextDrawUseBox(TD_HeaderBackground, 0);

	TD_HeaderTitle = TextDrawCreate(313.666687, 109.111114, "Gaminimas");
	TextDrawLetterSize(TD_HeaderTitle, 0.191666, 1.015110);
	TextDrawAlignment(TD_HeaderTitle, 1);
	TextDrawColor(TD_HeaderTitle, -7681);
	TextDrawSetShadow(TD_HeaderTitle, 0);
	TextDrawSetOutline(TD_HeaderTitle, 0);
	TextDrawBackgroundColor(TD_HeaderTitle, 0);
	TextDrawFont(TD_HeaderTitle, 2);
	TextDrawSetProportional(TD_HeaderTitle, 1);
	TextDrawSetShadow(TD_HeaderTitle, 0);

	TD_CloseEsc = TextDrawCreate(505.000030, 109.940750, "esc");
	TextDrawLetterSize(TD_CloseEsc, 0.191666, 1.015110);
	TextDrawAlignment(TD_CloseEsc, 2);
	TextDrawColor(TD_CloseEsc, -1523963137);
	TextDrawSetShadow(TD_CloseEsc, 0);
	TextDrawSetOutline(TD_CloseEsc, 0);
	TextDrawBackgroundColor(TD_CloseEsc, 255);
	TextDrawFont(TD_CloseEsc, 2);
	TextDrawSetProportional(TD_CloseEsc, 1);
	TextDrawSetShadow(TD_CloseEsc, 0);

	TD_CategoriesTitle = TextDrawCreate(217.000030, 126.533340, "Receptu kategorijos:");
	TextDrawLetterSize(TD_CategoriesTitle, 0.156332, 0.878220);
	TextDrawAlignment(TD_CategoriesTitle, 2);
	TextDrawColor(TD_CategoriesTitle, -1061109505);
	TextDrawSetShadow(TD_CategoriesTitle, 0);
	TextDrawSetOutline(TD_CategoriesTitle, 0);
	TextDrawBackgroundColor(TD_CategoriesTitle, 255);
	TextDrawFont(TD_CategoriesTitle, 2);
	TextDrawSetProportional(TD_CategoriesTitle, 1);
	TextDrawSetShadow(TD_CategoriesTitle, 0);

	static Float:offset = 66.0;

	new categories[CATEGORIES_COUNT][] = {
		"Ginklai",
		"Vaistai",
		"Narkotikai",
		"Mechanika",
		"Baldai"
	};

	for(new i; i < CATEGORIES_COUNT; ++i) {
		TD_CategoryBtnBackground[i] = TextDrawCreate(178 + (offset * i), 142.970336, "LD_SPAC:white");
		TextDrawLetterSize(TD_CategoryBtnBackground[i], 0.000000, 0.000000);
		TextDrawTextSize(TD_CategoryBtnBackground[i], 64.000000, 18.000000);
		TextDrawAlignment(TD_CategoryBtnBackground[i], 1);
		TextDrawColor(TD_CategoryBtnBackground[i], BG_COLOR_DEFAULT);
		TextDrawSetShadow(TD_CategoryBtnBackground[i], 0);
		TextDrawSetOutline(TD_CategoryBtnBackground[i], 0);
		TextDrawBackgroundColor(TD_CategoryBtnBackground[i], 255);
		TextDrawFont(TD_CategoryBtnBackground[i], 4);
		TextDrawSetProportional(TD_CategoryBtnBackground[i], 0);
		TextDrawSetShadow(TD_CategoryBtnBackground[i], 0);
		TextDrawSetSelectable(TD_CategoryBtnBackground[i], true);

		TD_CategoryName[i] = TextDrawCreate(210.000030 + (offset * i), 146.859283, categories[i]);
		TextDrawLetterSize(TD_CategoryName[i], 0.165665, 0.869925);
		TextDrawAlignment(TD_CategoryName[i], 2);
		TextDrawColor(TD_CategoryName[i], TXT_COLOR_DEFAULT);
		TextDrawSetShadow(TD_CategoryName[i], 0);
		TextDrawSetOutline(TD_CategoryName[i], 0);
		TextDrawBackgroundColor(TD_CategoryName[i], 255);
		TextDrawFont(TD_CategoryName[i], 2);
		TextDrawSetProportional(TD_CategoryName[i], 1);
		TextDrawSetShadow(TD_CategoryName[i], 0);
	}

	TD_ListBackground = TextDrawCreate(178.999984, 167.859252, "LD_SPAC:white");
	TextDrawLetterSize(TD_ListBackground, 0.000000, 0.000000);
	TextDrawTextSize(TD_ListBackground, 228.000000, 215.000000);
	TextDrawAlignment(TD_ListBackground, 1);
	TextDrawColor(TD_ListBackground, 2625);
	TextDrawSetShadow(TD_ListBackground, 0);
	TextDrawSetOutline(TD_ListBackground, 0);
	TextDrawBackgroundColor(TD_ListBackground, 255);
	TextDrawFont(TD_ListBackground, 4);
	TextDrawSetProportional(TD_ListBackground, 0);
	TextDrawSetShadow(TD_ListBackground, 0);

	TD_ListForeground = TextDrawCreate(178.999984, 167.859252, "LD_SPAC:white");
	TextDrawLetterSize(TD_ListForeground, 0.000000, 0.000000);
	TextDrawTextSize(TD_ListForeground, 228.000000, 215.000000);
	TextDrawAlignment(TD_ListForeground, 1);
	TextDrawColor(TD_ListForeground, -926351606);
	TextDrawSetShadow(TD_ListForeground, 0);
	TextDrawSetOutline(TD_ListForeground, 0);
	TextDrawBackgroundColor(TD_ListForeground, 255);
	TextDrawFont(TD_ListForeground, 4);
	TextDrawSetProportional(TD_ListForeground, 0);
	TextDrawSetShadow(TD_ListForeground, 0);

	offset = 16.0;

	for(new i; i < LIST_ROW_COUNT; ++i) {
		TD_ListRowBackground[i] = TextDrawCreate(180.000000, 169.0 + (offset * i), "LD_SPAC:white");
		TextDrawLetterSize(TD_ListRowBackground[i], 0.000000, 0.000000);
		TextDrawTextSize(TD_ListRowBackground[i], 226.000000, 15.000000);
		TextDrawAlignment(TD_ListRowBackground[i], 1);
		TextDrawColor(TD_ListRowBackground[i], 2685);
		TextDrawSetShadow(TD_ListRowBackground[i], 0);
		TextDrawSetOutline(TD_ListRowBackground[i], 0);
		TextDrawBackgroundColor(TD_ListRowBackground[i], 255);
		TextDrawFont(TD_ListRowBackground[i], 4);
		TextDrawSetProportional(TD_ListRowBackground[i], 0);
		TextDrawSetShadow(TD_ListRowBackground[i], 0);
		TextDrawSetSelectable(TD_ListRowBackground[i], true);

		TD_CheckBoxBackground[i] = TextDrawCreate(183.666656, 171.592590 + (offset * i), "LD_SPAC:white");
		TextDrawLetterSize(TD_CheckBoxBackground[i], 0.000000, 0.000000);
		TextDrawTextSize(TD_CheckBoxBackground[i], 10.000000, 10.000000);
		TextDrawAlignment(TD_CheckBoxBackground[i], 1);
		TextDrawColor(TD_CheckBoxBackground[i], 2815);
		TextDrawSetShadow(TD_CheckBoxBackground[i], 0);
		TextDrawSetOutline(TD_CheckBoxBackground[i], 0);
		TextDrawBackgroundColor(TD_CheckBoxBackground[i], 255);
		TextDrawFont(TD_CheckBoxBackground[i], 4);
		TextDrawSetProportional(TD_CheckBoxBackground[i], 0);
		TextDrawSetShadow(TD_CheckBoxBackground[i], 0);

		TD_CheckBoxForeground[i] = TextDrawCreate(184.666641, 172.837020 + (offset * i), "LD_SPAC:white");
		TextDrawLetterSize(TD_CheckBoxForeground[i], 0.000000, 0.000000);
		TextDrawTextSize(TD_CheckBoxForeground[i], 8.000000, 8.000000);
		TextDrawAlignment(TD_CheckBoxForeground[i], 1);
		TextDrawColor(TD_CheckBoxForeground[i], 168435455);
		TextDrawSetShadow(TD_CheckBoxForeground[i], 0);
		TextDrawSetOutline(TD_CheckBoxForeground[i], 0);
		TextDrawBackgroundColor(TD_CheckBoxForeground[i], 255);
		TextDrawFont(TD_CheckBoxForeground[i], 4);
		TextDrawSetProportional(TD_CheckBoxForeground[i], 0);
		TextDrawSetShadow(TD_CheckBoxForeground[i], 0);

		TD_CheckBoxMark[i] = TextDrawCreate(184.666519, 170.503707 + (offset * i), "+");
		TextDrawLetterSize(TD_CheckBoxMark[i], 0.359333, 1.268146);
		TextDrawAlignment(TD_CheckBoxMark[i], 1);
		TextDrawColor(TD_CheckBoxMark[i], 8388863);
		TextDrawSetShadow(TD_CheckBoxMark[i], 0);
		TextDrawSetOutline(TD_CheckBoxMark[i], 0);
		TextDrawBackgroundColor(TD_CheckBoxMark[i], -1);
		TextDrawFont(TD_CheckBoxMark[i], 1);
		TextDrawSetProportional(TD_CheckBoxMark[i], 1);
		TextDrawSetShadow(TD_CheckBoxMark[i], 0);
	}

	TD_RecipeComponentsBackground = TextDrawCreate(409.333465, 224.274078, "LD_SPAC:white");
	TextDrawLetterSize(TD_RecipeComponentsBackground, 0.000000, 0.000000);
	TextDrawTextSize(TD_RecipeComponentsBackground, 107.000000, 159.000000);
	TextDrawAlignment(TD_RecipeComponentsBackground, 1);
	TextDrawColor(TD_RecipeComponentsBackground, 2815);
	TextDrawSetShadow(TD_RecipeComponentsBackground, 0);
	TextDrawSetOutline(TD_RecipeComponentsBackground, 0);
	TextDrawBackgroundColor(TD_RecipeComponentsBackground, 255);
	TextDrawFont(TD_RecipeComponentsBackground, 4);
	TextDrawSetProportional(TD_RecipeComponentsBackground, 0);
	TextDrawSetShadow(TD_RecipeComponentsBackground, 0);

	TD_RecipeInfoModelBackground = TextDrawCreate(410.999908, 167.444488, "LD_SPAC:white");
	TextDrawLetterSize(TD_RecipeInfoModelBackground, 0.000000, 0.000000);
	TextDrawTextSize(TD_RecipeInfoModelBackground, 26.000000, 26.000000);
	TextDrawAlignment(TD_RecipeInfoModelBackground, 1);
	TextDrawColor(TD_RecipeInfoModelBackground, 252649215);
	TextDrawSetShadow(TD_RecipeInfoModelBackground, 0);
	TextDrawSetOutline(TD_RecipeInfoModelBackground, 0);
	TextDrawBackgroundColor(TD_RecipeInfoModelBackground, 255);
	TextDrawFont(TD_RecipeInfoModelBackground, 4);
	TextDrawSetProportional(TD_RecipeInfoModelBackground, 0);
	TextDrawSetShadow(TD_RecipeInfoModelBackground, 0);

	offset = 25.0;

	for(new i; i < COMPONENT_ROW_COUNT; ++i) {
		TD_ComponentBackground[i] = TextDrawCreate(411.0, 225.0 + (offset * i), "LD_SPAC:white");
		TextDrawLetterSize(TD_ComponentBackground[i], 0.000000, 0.000000);
		TextDrawTextSize(TD_ComponentBackground[i], 105.000000, 23.000000);
		TextDrawAlignment(TD_ComponentBackground[i], 1);
		TextDrawColor(TD_ComponentBackground[i], 336865535);
		TextDrawSetShadow(TD_ComponentBackground[i], 0);
		TextDrawSetOutline(TD_ComponentBackground[i], 0);
		TextDrawBackgroundColor(TD_ComponentBackground[i], 255);
		TextDrawFont(TD_ComponentBackground[i], 4);
		TextDrawSetProportional(TD_ComponentBackground[i], 0);
		TextDrawSetShadow(TD_ComponentBackground[i], 0);
		TextDrawSetSelectable(TD_ComponentBackground[i], true);

		TD_ComponentModelBackground[i] = TextDrawCreate(412.0, 226.0 + (offset * i), "LD_SPAC:white");
		TextDrawLetterSize(TD_ComponentModelBackground[i], 0.000000, 0.000000);
		TextDrawTextSize(TD_ComponentModelBackground[i], 22.000000, 21.000000);
		TextDrawAlignment(TD_ComponentModelBackground[i], 1);
		TextDrawColor(TD_ComponentModelBackground[i], 252649215);
		TextDrawSetShadow(TD_ComponentModelBackground[i], 0);
		TextDrawSetOutline(TD_ComponentModelBackground[i], 0);
		TextDrawBackgroundColor(TD_ComponentModelBackground[i], 255);
		TextDrawFont(TD_ComponentModelBackground[i], 4);
		TextDrawSetProportional(TD_ComponentModelBackground[i], 0);
		TextDrawSetShadow(TD_ComponentModelBackground[i], 0);

		TD_ComponentTextBackground[i] = TextDrawCreate(435.0, 226.0 + (offset * i), "LD_SPAC:white");
		TextDrawLetterSize(TD_ComponentTextBackground[i], 0.000000, 0.000000);
		TextDrawTextSize(TD_ComponentTextBackground[i], 80.000000, 21.000000);
		TextDrawAlignment(TD_ComponentTextBackground[i], 1);
		TextDrawColor(TD_ComponentTextBackground[i], 168434175);
		TextDrawSetShadow(TD_ComponentTextBackground[i], 0);
		TextDrawSetOutline(TD_ComponentTextBackground[i], 0);
		TextDrawBackgroundColor(TD_ComponentTextBackground[i], 255);
		TextDrawFont(TD_ComponentTextBackground[i], 4);
		TextDrawSetProportional(TD_ComponentTextBackground[i], 0);
		TextDrawSetShadow(TD_ComponentTextBackground[i], 0);
	}

	TD_FilterBtnBackground = TextDrawCreate(179.0, 385.0, "LD_SPAC:white");
	TextDrawLetterSize(TD_FilterBtnBackground, 0.000000, 0.000000);
	TextDrawTextSize(TD_FilterBtnBackground, 73.000000, 13.000000);
	TextDrawAlignment(TD_FilterBtnBackground, 1);
	TextDrawColor(TD_FilterBtnBackground, 336865535);
	TextDrawSetShadow(TD_FilterBtnBackground, 0);
	TextDrawSetOutline(TD_FilterBtnBackground, 0);
	TextDrawBackgroundColor(TD_FilterBtnBackground, 255);
	TextDrawFont(TD_FilterBtnBackground, 4);
	TextDrawSetProportional(TD_FilterBtnBackground, 0);
	TextDrawSetShadow(TD_FilterBtnBackground, 0);
	TextDrawSetSelectable(TD_FilterBtnBackground, true);

	TD_CraftAllBtnBackground = TextDrawCreate(349.0, 385.0, "LD_SPAC:white");
	TextDrawLetterSize(TD_CraftAllBtnBackground, 0.000000, 0.000000);
	TextDrawTextSize(TD_CraftAllBtnBackground, 58.000000, 13.000000);
	TextDrawAlignment(TD_CraftAllBtnBackground, 1);
	TextDrawColor(TD_CraftAllBtnBackground, 336865535);
	TextDrawSetShadow(TD_CraftAllBtnBackground, 0);
	TextDrawSetOutline(TD_CraftAllBtnBackground, 0);
	TextDrawBackgroundColor(TD_CraftAllBtnBackground, 255);
	TextDrawFont(TD_CraftAllBtnBackground, 4);
	TextDrawSetProportional(TD_CraftAllBtnBackground, 0);
	TextDrawSetShadow(TD_CraftAllBtnBackground, 0);
	TextDrawSetSelectable(TD_CraftAllBtnBackground, true);

	TD_CraftAllBtnText = TextDrawCreate(378.666412, 387.451812, "Gaminti visus");
	TextDrawLetterSize(TD_CraftAllBtnText, 0.133665, 0.911406);
	TextDrawAlignment(TD_CraftAllBtnText, 2);
	TextDrawColor(TD_CraftAllBtnText, -926373121);
	TextDrawSetShadow(TD_CraftAllBtnText, 0);
	TextDrawSetOutline(TD_CraftAllBtnText, 0);
	TextDrawBackgroundColor(TD_CraftAllBtnText, 255);
	TextDrawFont(TD_CraftAllBtnText, 2);
	TextDrawSetProportional(TD_CraftAllBtnText, 1);
	TextDrawSetShadow(TD_CraftAllBtnText, 0);

	TD_AmountSelectionBackground = TextDrawCreate(411.0, 385.0, "LD_SPAC:white");
	TextDrawLetterSize(TD_AmountSelectionBackground, 0.000000, 0.000000);
	TextDrawTextSize(TD_AmountSelectionBackground, 58.000000, 13.000000);
	TextDrawAlignment(TD_AmountSelectionBackground, 1);
	TextDrawColor(TD_AmountSelectionBackground, 336865535);
	TextDrawSetShadow(TD_AmountSelectionBackground, 0);
	TextDrawSetOutline(TD_AmountSelectionBackground, 0);
	TextDrawBackgroundColor(TD_AmountSelectionBackground, 255);
	TextDrawFont(TD_AmountSelectionBackground, 4);
	TextDrawSetProportional(TD_AmountSelectionBackground, 0);
	TextDrawSetShadow(TD_AmountSelectionBackground, 0);
	TextDrawSetSelectable(TD_AmountSelectionBackground, true);

	TD_AmountDecreaseBtnBackground = TextDrawCreate(412.0, 386.0, "LD_SPAC:white");
	TextDrawLetterSize(TD_AmountDecreaseBtnBackground, 0.000000, 0.000000);
	TextDrawTextSize(TD_AmountDecreaseBtnBackground, 11.000000, 11.000000);
	TextDrawAlignment(TD_AmountDecreaseBtnBackground, 1);
	TextDrawColor(TD_AmountDecreaseBtnBackground, 84219135);
	TextDrawSetShadow(TD_AmountDecreaseBtnBackground, 0);
	TextDrawSetOutline(TD_AmountDecreaseBtnBackground, 0);
	TextDrawBackgroundColor(TD_AmountDecreaseBtnBackground, 255);
	TextDrawFont(TD_AmountDecreaseBtnBackground, 4);
	TextDrawSetProportional(TD_AmountDecreaseBtnBackground, 0);
	TextDrawSetShadow(TD_AmountDecreaseBtnBackground, 0);
	TextDrawSetSelectable(TD_AmountDecreaseBtnBackground, true);

	TD_AmountIncreaseBtnBackground = TextDrawCreate(458.0, 386.0, "LD_SPAC:white");
	TextDrawLetterSize(TD_AmountIncreaseBtnBackground, 0.000000, 0.000000);
	TextDrawTextSize(TD_AmountIncreaseBtnBackground, 11.000000, 11.000000);
	TextDrawAlignment(TD_AmountIncreaseBtnBackground, 1);
	TextDrawColor(TD_AmountIncreaseBtnBackground, 84219135);
	TextDrawSetShadow(TD_AmountIncreaseBtnBackground, 0);
	TextDrawSetOutline(TD_AmountIncreaseBtnBackground, 0);
	TextDrawBackgroundColor(TD_AmountIncreaseBtnBackground, 255);
	TextDrawFont(TD_AmountIncreaseBtnBackground, 4);
	TextDrawSetProportional(TD_AmountIncreaseBtnBackground, 0);
	TextDrawSetShadow(TD_AmountIncreaseBtnBackground, 0);
	TextDrawSetSelectable(TD_AmountIncreaseBtnBackground, true);

	TD_AmountDecreaseBtnText = TextDrawCreate(413.666656, 385.0, "-");
	TextDrawLetterSize(TD_AmountDecreaseBtnText, 0.441998, 1.131260);
	TextDrawAlignment(TD_AmountDecreaseBtnText, 1);
	TextDrawColor(TD_AmountDecreaseBtnText, -1);
	TextDrawSetShadow(TD_AmountDecreaseBtnText, 0);
	TextDrawSetOutline(TD_AmountDecreaseBtnText, 0);
	TextDrawBackgroundColor(TD_AmountDecreaseBtnText, 255);
	TextDrawFont(TD_AmountDecreaseBtnText, 1);
	TextDrawSetProportional(TD_AmountDecreaseBtnText, 1);
	TextDrawSetShadow(TD_AmountDecreaseBtnText, 0);

	TD_AmountIncreaseBtnText = TextDrawCreate(460.0, 385.0, "+");
	TextDrawLetterSize(TD_AmountIncreaseBtnText, 0.324665, 1.297186);
	TextDrawAlignment(TD_AmountIncreaseBtnText, 1);
	TextDrawColor(TD_AmountIncreaseBtnText, -1);
	TextDrawSetShadow(TD_AmountIncreaseBtnText, 0);
	TextDrawSetOutline(TD_AmountIncreaseBtnText, 0);
	TextDrawBackgroundColor(TD_AmountIncreaseBtnText, 255);
	TextDrawFont(TD_AmountIncreaseBtnText, 1);
	TextDrawSetProportional(TD_AmountIncreaseBtnText, 1);
	TextDrawSetShadow(TD_AmountIncreaseBtnText, 0);

	TD_AmountToCraftTextBackground = TextDrawCreate(424.0, 386.0, "LD_SPAC:white");
	TextDrawLetterSize(TD_AmountToCraftTextBackground, 0.000000, 0.000000);
	TextDrawTextSize(TD_AmountToCraftTextBackground, 33.000000, 11.000000);
	TextDrawAlignment(TD_AmountToCraftTextBackground, 1);
	TextDrawColor(TD_AmountToCraftTextBackground, 84219135);
	TextDrawSetShadow(TD_AmountToCraftTextBackground, 0);
	TextDrawSetOutline(TD_AmountToCraftTextBackground, 0);
	TextDrawBackgroundColor(TD_AmountToCraftTextBackground, 255);
	TextDrawFont(TD_AmountToCraftTextBackground, 4);
	TextDrawSetProportional(TD_AmountToCraftTextBackground, 0);
	TextDrawSetShadow(TD_AmountToCraftTextBackground, 0);
	TextDrawSetSelectable(TD_AmountToCraftTextBackground, true);

	TD_CraftBtnBackground = TextDrawCreate(470.0, 385.0, "LD_SPAC:white");
	TextDrawLetterSize(TD_CraftBtnBackground, 0.000000, 0.000000);
	TextDrawTextSize(TD_CraftBtnBackground, 46.000000, 13.000000);
	TextDrawAlignment(TD_CraftBtnBackground, 1);
	TextDrawColor(TD_CraftBtnBackground, 336865535);
	TextDrawSetShadow(TD_CraftBtnBackground, 0);
	TextDrawSetOutline(TD_CraftBtnBackground, 0);
	TextDrawBackgroundColor(TD_CraftBtnBackground, 255);
	TextDrawFont(TD_CraftBtnBackground, 4);
	TextDrawSetProportional(TD_CraftBtnBackground, 0);
	TextDrawSetShadow(TD_CraftBtnBackground, 0);
	TextDrawSetSelectable(TD_CraftBtnBackground, true);

	TD_CraftBtnText = TextDrawCreate(493.0, 387.451873, "Gaminti");
	TextDrawLetterSize(TD_CraftBtnText, 0.133665, 0.911406);
	TextDrawAlignment(TD_CraftBtnText, 2);
	TextDrawColor(TD_CraftBtnText, -926373121);
	TextDrawSetShadow(TD_CraftBtnText, 0);
	TextDrawSetOutline(TD_CraftBtnText, 0);
	TextDrawBackgroundColor(TD_CraftBtnText, 255);
	TextDrawFont(TD_CraftBtnText, 2);
	TextDrawSetProportional(TD_CraftBtnText, 1);
	TextDrawSetShadow(TD_CraftBtnText, 0);

	TD_ShowPreviousPageBtn = TextDrawCreate(278.000213, 387.296203, "LD_BEAT:left");
	TextDrawLetterSize(TD_ShowPreviousPageBtn, 0.000000, 0.000000);
	TextDrawTextSize(TD_ShowPreviousPageBtn, 10.000000, 10.000000);
	TextDrawAlignment(TD_ShowPreviousPageBtn, 1);
	TextDrawColor(TD_ShowPreviousPageBtn, -1);
	TextDrawSetShadow(TD_ShowPreviousPageBtn, 0);
	TextDrawSetOutline(TD_ShowPreviousPageBtn, 0);
	TextDrawBackgroundColor(TD_ShowPreviousPageBtn, 255);
	TextDrawFont(TD_ShowPreviousPageBtn, 4);
	TextDrawSetProportional(TD_ShowPreviousPageBtn, 0);
	TextDrawSetShadow(TD_ShowPreviousPageBtn, 0);
	TextDrawSetSelectable(TD_ShowPreviousPageBtn, true);

	TD_ShowNextPageBtn = TextDrawCreate(311.000305, 386.881378, "LD_BEAT:right");
	TextDrawLetterSize(TD_ShowNextPageBtn, 0.000000, 0.000000);
	TextDrawTextSize(TD_ShowNextPageBtn, 10.000000, 10.000000);
	TextDrawAlignment(TD_ShowNextPageBtn, 1);
	TextDrawColor(TD_ShowNextPageBtn, -1);
	TextDrawSetShadow(TD_ShowNextPageBtn, 0);
	TextDrawSetOutline(TD_ShowNextPageBtn, 0);
	TextDrawBackgroundColor(TD_ShowNextPageBtn, 255);
	TextDrawFont(TD_ShowNextPageBtn, 4);
	TextDrawSetProportional(TD_ShowNextPageBtn, 0);
	TextDrawSetShadow(TD_ShowNextPageBtn, 0);
	TextDrawSetSelectable(TD_ShowNextPageBtn, true);
}

hook OnPlayerConnect(playerid) {
	new Float:offset = 16.0;

	for(new i; i < LIST_ROW_COUNT; ++i) {
		PlayerTD_ListRowName[playerid][i] = CreatePlayerTextDraw(playerid, 218.333282, 171.748168 + (offset * i), "Morfinas");
		PlayerTextDrawLetterSize(playerid, PlayerTD_ListRowName[playerid][i], 0.156332, 0.869925);
		PlayerTextDrawAlignment(playerid, PlayerTD_ListRowName[playerid][i], 1);
		PlayerTextDrawColor(playerid, PlayerTD_ListRowName[playerid][i], -926373121);
		PlayerTextDrawSetShadow(playerid, PlayerTD_ListRowName[playerid][i], 0);
		PlayerTextDrawSetOutline(playerid, PlayerTD_ListRowName[playerid][i], 0);
		PlayerTextDrawBackgroundColor(playerid, PlayerTD_ListRowName[playerid][i], 255);
		PlayerTextDrawFont(playerid, PlayerTD_ListRowName[playerid][i], 2);
		PlayerTextDrawSetProportional(playerid, PlayerTD_ListRowName[playerid][i], 1);
		PlayerTextDrawSetShadow(playerid, PlayerTD_ListRowName[playerid][i], 0);

		PlayerTD_ListRowRequiredLevel[playerid][i] = CreatePlayerTextDraw(playerid, 378.332824, 171.748168 + (offset * i), "18");
		PlayerTextDrawLetterSize(playerid, PlayerTD_ListRowRequiredLevel[playerid][i], 0.156332, 0.869925);
		PlayerTextDrawAlignment(playerid, PlayerTD_ListRowRequiredLevel[playerid][i], 3);
		PlayerTextDrawColor(playerid, PlayerTD_ListRowRequiredLevel[playerid][i], -926373121);
		PlayerTextDrawSetShadow(playerid, PlayerTD_ListRowRequiredLevel[playerid][i], 0);
		PlayerTextDrawSetOutline(playerid, PlayerTD_ListRowRequiredLevel[playerid][i], 0);
		PlayerTextDrawBackgroundColor(playerid, PlayerTD_ListRowRequiredLevel[playerid][i], 255);
		PlayerTextDrawFont(playerid, PlayerTD_ListRowRequiredLevel[playerid][i], 2);
		PlayerTextDrawSetProportional(playerid, PlayerTD_ListRowRequiredLevel[playerid][i], 1);
		PlayerTextDrawSetShadow(playerid, PlayerTD_ListRowRequiredLevel[playerid][i], 0);
	}


	PlayerTD_RecipeInfoName[playerid] = CreatePlayerTextDraw(playerid, 443.333312, 166.770339, "Morfinas");
	PlayerTextDrawLetterSize(playerid, PlayerTD_RecipeInfoName[playerid], 0.177999, 0.919703);
	PlayerTextDrawAlignment(playerid, PlayerTD_RecipeInfoName[playerid], 1);
	PlayerTextDrawColor(playerid, PlayerTD_RecipeInfoName[playerid], -5963521);
	PlayerTextDrawSetShadow(playerid, PlayerTD_RecipeInfoName[playerid], 0);
	PlayerTextDrawSetOutline(playerid, PlayerTD_RecipeInfoName[playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PlayerTD_RecipeInfoName[playerid], 255);
	PlayerTextDrawFont(playerid, PlayerTD_RecipeInfoName[playerid], 2);
	PlayerTextDrawSetProportional(playerid, PlayerTD_RecipeInfoName[playerid], 1);
	PlayerTextDrawSetShadow(playerid, PlayerTD_RecipeInfoName[playerid], 0);

	PlayerTD_RecipeInfoModel[playerid] = CreatePlayerTextDraw(playerid, 407.333343, 159.977783, "");
	PlayerTextDrawLetterSize(playerid, PlayerTD_RecipeInfoModel[playerid], 0.000000, 0.000000);
	PlayerTextDrawTextSize(playerid, PlayerTD_RecipeInfoModel[playerid], 32.000000, 32.000000);
	PlayerTextDrawAlignment(playerid, PlayerTD_RecipeInfoModel[playerid], 1);
	PlayerTextDrawColor(playerid, PlayerTD_RecipeInfoModel[playerid], -1);
	PlayerTextDrawSetShadow(playerid, PlayerTD_RecipeInfoModel[playerid], 0);
	PlayerTextDrawSetOutline(playerid, PlayerTD_RecipeInfoModel[playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PlayerTD_RecipeInfoModel[playerid], 0);
	PlayerTextDrawFont(playerid, PlayerTD_RecipeInfoModel[playerid], 5);
	PlayerTextDrawSetProportional(playerid, PlayerTD_RecipeInfoModel[playerid], 0);
	PlayerTextDrawSetShadow(playerid, PlayerTD_RecipeInfoModel[playerid], 0);
	PlayerTextDrawSetPreviewModel(playerid, PlayerTD_RecipeInfoModel[playerid], 1575);
	PlayerTextDrawSetPreviewRot(playerid, PlayerTD_RecipeInfoModel[playerid], 152.000000, 172.000000, 130.000000, 0.974973);

	PlayerTD_RecipeInfoAmount[playerid] = CreatePlayerTextDraw(playerid, 442.999908, 182.533309, "~y~10~w~~h~~h~g.");
	PlayerTextDrawLetterSize(playerid, PlayerTD_RecipeInfoAmount[playerid], 0.202996, 1.006814);
	PlayerTextDrawAlignment(playerid, PlayerTD_RecipeInfoAmount[playerid], 1);
	PlayerTextDrawColor(playerid, PlayerTD_RecipeInfoAmount[playerid], -1);
	PlayerTextDrawSetShadow(playerid, PlayerTD_RecipeInfoAmount[playerid], 0);
	PlayerTextDrawSetOutline(playerid, PlayerTD_RecipeInfoAmount[playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PlayerTD_RecipeInfoAmount[playerid], 255);
	PlayerTextDrawFont(playerid, PlayerTD_RecipeInfoAmount[playerid], 2);
	PlayerTextDrawSetProportional(playerid, PlayerTD_RecipeInfoAmount[playerid], 1);
	PlayerTextDrawSetShadow(playerid, PlayerTD_RecipeInfoAmount[playerid], 0);

	PlayerTD_RecipeInfoDescription[playerid] = CreatePlayerTextDraw(playerid, 413.999786, 197.466598, "Vaistas skirtas skausmui numalsinti. Labai stipraus poveikio.");
	PlayerTextDrawLetterSize(playerid, PlayerTD_RecipeInfoDescription[playerid], 0.160330, 0.903110);
	PlayerTextDrawTextSize(playerid, PlayerTD_RecipeInfoModel[playerid], 70.000000, 0.0);
	PlayerTextDrawAlignment(playerid, PlayerTD_RecipeInfoDescription[playerid], 1);
	PlayerTextDrawColor(playerid, PlayerTD_RecipeInfoDescription[playerid], -7681);
	PlayerTextDrawSetShadow(playerid, PlayerTD_RecipeInfoDescription[playerid], 0);
	PlayerTextDrawSetOutline(playerid, PlayerTD_RecipeInfoDescription[playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PlayerTD_RecipeInfoDescription[playerid], 255);
	PlayerTextDrawFont(playerid, PlayerTD_RecipeInfoDescription[playerid], 2);
	PlayerTextDrawSetProportional(playerid, PlayerTD_RecipeInfoDescription[playerid], 1);
	PlayerTextDrawSetShadow(playerid, PlayerTD_RecipeInfoDescription[playerid], 0);

	offset = 25.0;

	for(new i; i < COMPONENT_ROW_COUNT; ++i) {
		PlayerTD_RecipeComponentModel[playerid][i] = CreatePlayerTextDraw(playerid, 409.0, 221.785247 + (offset * i), "");
		PlayerTextDrawLetterSize(playerid, PlayerTD_RecipeComponentModel[playerid][i], 0.000000, 0.000000);
		PlayerTextDrawTextSize(playerid, PlayerTD_RecipeComponentModel[playerid][i], 27.000000, 27.000000);
		PlayerTextDrawAlignment(playerid, PlayerTD_RecipeComponentModel[playerid][i], 1);
		PlayerTextDrawColor(playerid, PlayerTD_RecipeComponentModel[playerid][i], -1);
		PlayerTextDrawSetShadow(playerid, PlayerTD_RecipeComponentModel[playerid][i], 0);
		PlayerTextDrawSetOutline(playerid, PlayerTD_RecipeComponentModel[playerid][i], 0);
		PlayerTextDrawBackgroundColor(playerid, PlayerTD_RecipeComponentModel[playerid][i], 0);
		PlayerTextDrawFont(playerid, PlayerTD_RecipeComponentModel[playerid][i], 5);
		PlayerTextDrawSetProportional(playerid, PlayerTD_RecipeComponentModel[playerid][i], 0);
		PlayerTextDrawSetShadow(playerid, PlayerTD_RecipeComponentModel[playerid][i], 0);
		PlayerTextDrawSetPreviewModel(playerid, PlayerTD_RecipeComponentModel[playerid][i], 1575);
		PlayerTextDrawSetPreviewRot(playerid, PlayerTD_RecipeComponentModel[playerid][i], 152.000000, 172.000000, 130.000000, 0.974973);

		PlayerTD_RecipeComponentName[playerid][i] = CreatePlayerTextDraw(playerid, 438.666625, 226.918518 + (offset * i), "Chem. m. NZ-578");
		PlayerTextDrawLetterSize(playerid, PlayerTD_RecipeComponentName[playerid][i], 0.134999, 0.840888);
		PlayerTextDrawAlignment(playerid, PlayerTD_RecipeComponentName[playerid][i], 1);
		PlayerTextDrawColor(playerid, PlayerTD_RecipeComponentName[playerid][i], -1);
		PlayerTextDrawSetShadow(playerid, PlayerTD_RecipeComponentName[playerid][i], 0);
		PlayerTextDrawSetOutline(playerid, PlayerTD_RecipeComponentName[playerid][i], 0);
		PlayerTextDrawBackgroundColor(playerid, PlayerTD_RecipeComponentName[playerid][i], 255);
		PlayerTextDrawFont(playerid, PlayerTD_RecipeComponentName[playerid][i], 2);
		PlayerTextDrawSetProportional(playerid, PlayerTD_RecipeComponentName[playerid][i], 1);
		PlayerTextDrawSetShadow(playerid, PlayerTD_RecipeComponentName[playerid][i], 0);

		PlayerTD_RecipeComponentAmount[playerid][i] = CreatePlayerTextDraw(playerid, 510.666290, 237.703582 + (offset * i), "~y~5~w~~h~~h~g./~y~216~w~~h~~h~g.");
		PlayerTextDrawLetterSize(playerid, PlayerTD_RecipeComponentAmount[playerid][i], 0.134999, 0.840888);
		PlayerTextDrawAlignment(playerid, PlayerTD_RecipeComponentAmount[playerid][i], 3);
		PlayerTextDrawColor(playerid, PlayerTD_RecipeComponentAmount[playerid][i], -1);
		PlayerTextDrawSetShadow(playerid, PlayerTD_RecipeComponentAmount[playerid][i], 0);
		PlayerTextDrawSetOutline(playerid, PlayerTD_RecipeComponentAmount[playerid][i], 0);
		PlayerTextDrawBackgroundColor(playerid, PlayerTD_RecipeComponentAmount[playerid][i], 255);
		PlayerTextDrawFont(playerid, PlayerTD_RecipeComponentAmount[playerid][i], 2);
		PlayerTextDrawSetProportional(playerid, PlayerTD_RecipeComponentAmount[playerid][i], 1);
		PlayerTextDrawSetShadow(playerid, PlayerTD_RecipeComponentAmount[playerid][i], 0);
	}

	PlayerTD_FilterText[playerid] = CreatePlayerTextDraw(playerid, 215.666656, 387.451843, "Rodyti tik galimus");
	PlayerTextDrawLetterSize(playerid, PlayerTD_FilterText[playerid], 0.133665, 0.911406);
	PlayerTextDrawAlignment(playerid, PlayerTD_FilterText[playerid], 2);
	PlayerTextDrawColor(playerid, PlayerTD_FilterText[playerid], -926373121);
	PlayerTextDrawSetShadow(playerid, PlayerTD_FilterText[playerid], 0);
	PlayerTextDrawSetOutline(playerid, PlayerTD_FilterText[playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PlayerTD_FilterText[playerid], 255);
	PlayerTextDrawFont(playerid, PlayerTD_FilterText[playerid], 2);
	PlayerTextDrawSetProportional(playerid, PlayerTD_FilterText[playerid], 1);
	PlayerTextDrawSetShadow(playerid, PlayerTD_FilterText[playerid], 0);

	PlayerTD_AmountOfItemsToCraft[playerid] = CreatePlayerTextDraw(playerid, 439.333343, 386.207397, "15");
	PlayerTextDrawLetterSize(playerid, PlayerTD_AmountOfItemsToCraft[playerid], 0.169999, 0.998517);
	PlayerTextDrawAlignment(playerid, PlayerTD_AmountOfItemsToCraft[playerid], 2);
	PlayerTextDrawColor(playerid, PlayerTD_AmountOfItemsToCraft[playerid], -1);
	PlayerTextDrawSetShadow(playerid, PlayerTD_AmountOfItemsToCraft[playerid], 0);
	PlayerTextDrawSetOutline(playerid, PlayerTD_AmountOfItemsToCraft[playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PlayerTD_AmountOfItemsToCraft[playerid], 255);
	PlayerTextDrawFont(playerid, PlayerTD_AmountOfItemsToCraft[playerid], 2);
	PlayerTextDrawSetProportional(playerid, PlayerTD_AmountOfItemsToCraft[playerid], 1);
	PlayerTextDrawSetShadow(playerid, PlayerTD_AmountOfItemsToCraft[playerid], 0);

	PlayerTD_CurrentPage[playerid] = CreatePlayerTextDraw(playerid, 300.333465, 386.622222, "15");
	PlayerTextDrawLetterSize(playerid, PlayerTD_CurrentPage[playerid], 0.169999, 0.998517);
	PlayerTextDrawAlignment(playerid, PlayerTD_CurrentPage[playerid], 2);
	PlayerTextDrawColor(playerid, PlayerTD_CurrentPage[playerid], -1);
	PlayerTextDrawSetShadow(playerid, PlayerTD_CurrentPage[playerid], 0);
	PlayerTextDrawSetOutline(playerid, PlayerTD_CurrentPage[playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PlayerTD_CurrentPage[playerid], 255);
	PlayerTextDrawFont(playerid, PlayerTD_CurrentPage[playerid], 2);
	PlayerTextDrawSetProportional(playerid, PlayerTD_CurrentPage[playerid], 1);
	PlayerTextDrawSetShadow(playerid, PlayerTD_CurrentPage[playerid], 0);

	PlayerTD_CategoryExperience[playerid] = CreatePlayerTextDraw(playerid, 512.000122, 126.533325, "~w~Vaistu gaminimo lygis: ~y~16~w~, patirtis: ~y~24 321");
	PlayerTextDrawLetterSize(playerid, PlayerTD_CategoryExperience[playerid], 0.156332, 0.878220);
	PlayerTextDrawAlignment(playerid, PlayerTD_CategoryExperience[playerid], 3);
	PlayerTextDrawColor(playerid, PlayerTD_CategoryExperience[playerid], -1061109505);
	PlayerTextDrawSetShadow(playerid, PlayerTD_CategoryExperience[playerid], 0);
	PlayerTextDrawSetOutline(playerid, PlayerTD_CategoryExperience[playerid], 0);
	PlayerTextDrawBackgroundColor(playerid, PlayerTD_CategoryExperience[playerid], 255);
	PlayerTextDrawFont(playerid, PlayerTD_CategoryExperience[playerid], 2);
	PlayerTextDrawSetProportional(playerid, PlayerTD_CategoryExperience[playerid], 1);
	PlayerTextDrawSetShadow(playerid, PlayerTD_CategoryExperience[playerid], 0);
}