#include <YSI_Coding\y_hooks>

const guardians_count = 4;
static guardians[guardians_count];

static DropStuff(Float:x, Float:y, Float:z) {
	#pragma unused x, y, z
}

timer CreateSnakeFarmGuardian[1000](i) {
	printf("Creating SnakeFarmNpc: %i", i);
	if(FCNPC_IsValid(guardians[i])) {
		printf("Destroying SnakeFarmNpc: %i", i);
		FCNPC_Destroy(guardians[i]);
	}
	new Float:x, Float:y, Float:z;
	x = -23.5921 + (floatrandom(15.0) - floatrandom(15.0));
	y = 2341.7852 + (floatrandom(15.0) - floatrandom(15.0));
	z = 24.1406;
	new gid = CreateGuardian(GUARDIAN_TYPE_STRONG, x, y, z, 33, WEAPON_SHOTGUN);
	SetGuardianPoint(gid, -23.5921,2341.7852,24.1406);

	guardians[i] = gid;
}

hook OnGameModeInit() {
	for(new i; i < guardians_count; ++i) {
		defer CreateSnakeFarmGuardian[i * 1000](i);
	}
}

hook FCNPC_OnDeath(npcid, killerid, weaponid) {
	for(new i; i < guardians_count; ++i) {
		if(guardians[i] == npcid) {
			guardians[i] = INVALID_PLAYER_ID;
			
			new Float:x, Float:y, Float:z;
			FCNPC_GetPosition(npcid, x, y, z);
			DropStuff(x, y, z);
			defer CreateSnakeFarmGuardian[15000](i);
			return;
		}
	}
}