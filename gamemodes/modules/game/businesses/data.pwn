#define MAX_BUSINESSES (200)

enum E_BUSINESS_DATA {
	ORM:E_BUSINESS_ORM,
	E_BUSINESS_ID,
	E_BUSINESS_OWNER_ID,
	E_BUSINESS_PRICE,
	E_BUSINESS_TYPE,
	E_BUSINESS_CASH,
	Float:E_BUSINESS_ENTRANCE_X,
	Float:E_BUSINESS_ENTRANCE_Y,
	Float:E_BUSINESS_ENTRANCE_Z,
	E_BUSINESS_NAME[50],
	E_BUSINESS_DESCRIPTION[256]
};

static Businesses[MAX_BUSINESSES][E_BUSINESS_DATA];

hook OnGameModeInit() {
	LoadBusinesses();
}

static ORM:CreateBusinessORM(business_id) {
	new ORM:ormid = Businesses[business_id][E_BUSINESS_ORM] = orm_create("businesses");

	Businesses[business_id][E_BUSINESS_ID] = business_id;

	orm_addvar_int(ormid, Businesses[business_id][E_BUSINESS_ID], "id");
	orm_setkey(ormid, "id");

	orm_addvar_int(ormid, Businesses[business_id][E_BUSINESS_OWNER_ID], "user_id");
	orm_addvar_int(ormid, Businesses[business_id][E_BUSINESS_PRICE], "price");
	orm_addvar_int(ormid, Businesses[business_id][E_BUSINESS_TYPE], "type");
	orm_addvar_int(ormid, Businesses[business_id][E_BUSINESS_CASH], "cash");
	orm_addvar_string(ormid, Businesses[business_id][E_BUSINESS_NAME], 50, "name");
	orm_addvar_float(ormid, Businesses[business_id][E_BUSINESS_ENTRANCE_X], "entrance_x");
	orm_addvar_float(ormid, Businesses[business_id][E_BUSINESS_ENTRANCE_Y], "entrance_y");
	orm_addvar_float(ormid, Businesses[business_id][E_BUSINESS_ENTRANCE_Z], "entrance_z");
	orm_addvar_string(ormid, Businesses[business_id][E_BUSINESS_DESCRIPTION], 256, "description");

	return ormid;
}

// kraunam visus verslus
static LoadBusinesses() {
	format_query("SELECT id FROM businesses ORDER BY id DESC LIMIT 1");

	new Cache:cache = mysql_query(database, query);

	// gaunam verl� kiek� i� duomen� baz�s
	new businesses_count = ret_cache_get_value_name_int(0, "id");

	cache_delete(cache);

	// kadangi verslai yra sunumeruoti eil�s tvarka nuo 0 iki businesses_count
	// galima juos taip paeiliui ir pakrauti
	do {
		// persivadinam kad ai�kiau b�t�
		new business_id = businesses_count;
		// susi�ymim koki� informacij� kur norim pakrauti
		new ORM:ormid = CreateBusinessORM(business_id);

		// ir j� pakraunam
		orm_load(ormid, "OnBusinessCreated", "i", business_id);
	}
	while(0 < businesses_count--);
}

static SaveBusinesses() {
	for(new index; index < MAX_BUSINESSES; ++i) {
		SaveBusiness(index);
	}
}

static SaveBusiness(index) {
	if(Businesses[index][E_BUSINESS_ORM]) {
		orm_save(Businesses[index][E_BUSINESS_ORM]);
	}
}

BusinessOwner(business_id, value = -1) {
	if(0 <= business_id < sizeof Businesses) {
		if(value != -1) {
			Businesses[business_id][E_BUSINESS_OWNER_ID] = value;
		}
		return Businesses[business_id][E_BUSINESS_OWNER_ID];
	}
	return 0;
}

BusinessEntrancePosition(business_id, &Float:x, &Float:y, &Float:z) {
	x = Businesses[business_id][E_BUSINESS_ENTRANCE_X];
	y = Businesses[business_id][E_BUSINESS_ENTRANCE_Y];
	z = Businesses[business_id][E_BUSINESS_ENTRANCE_Z];
}

BusinessName(business_id, value[] = "", len = sizeof value) {
	new name[50];
	if(0 <= business_id < sizeof Businesses) {
		if(len > 1) {
			format(Businesses[business_id][E_BUSINESS_NAME], 50, value);
		}
		format(name, 50, "%s", Businesses[business_id][E_BUSINESS_NAME]);
	}
	return name;
}

BusinessType(business_id, value = -1) {
	if(0 <= business_id < sizeof Businesses) {
		if(value != -1) {
			Businesses[business_id][E_BUSINESS_TYPE] = value;
		}
		return Businesses[business_id][E_BUSINESS_TYPE];
	}
	return 0;
}

BusinessPrice(business_id, value = -1) {
	if(0 <= business_id < sizeof Businesses) {
		if(value != -1) {
			Businesses[business_id][E_BUSINESS_PRICE] = value;
		}
		return Businesses[business_id][E_BUSINESS_PRICE];
	}
	return 0;
}

BusinessCash(business_id, value = -1) {
	if(0 <= business_id < sizeof Businesses) {
		if(value != -1) {
			Businesses[business_id][E_BUSINESS_CASH] = value;
		}
		return Businesses[business_id][E_BUSINESS_CASH];
	}
	return 0;
}

BusinessDescription(business_id, value = -1) {
	if(0 <= business_id < sizeof Businesses) {
		if(value != -1) {
			Businesses[business_id][E_BUSINESS_DESCRIPTION] = value;
		}
		return Businesses[business_id][E_BUSINESS_DESCRIPTION];
	}
	return 0;
}