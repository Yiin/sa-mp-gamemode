#include <YSI\y_hooks>

static business_manager;

hook OnGameModeInit() {
	business_manager = CreateActor(40, 1656.0361,-1574.0277,13.3828, 180.0);
}

hook OnPlayerUpdate(playerid) {
	static keys, unused;
	GetPlayerKeys(playerid, keys, unused, unused);

	if(keys & KEY_AIM) {
		static actorid;
		actorid = GetPlayerTargetActor(playerid);

		if(actorid == INVALID_ACTOR_ID) {
			return true;
		}

		if(actorid == business_manager) {
			TalkToBusinessManager(playerid);
		}
	}
	return true;
}

static TalkToBusinessManager(playerid) {
	
}