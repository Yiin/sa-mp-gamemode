#include <YSI\y_hooks>

static pickups[MAX_BUSINESSES];

hook OnBusinessCreated(business_id) {
	// default pick up id jeigu verslas laisvas
	new pickup_model_id = 1274;

	// jei verslas turi savinink�
	if(BusinessOwner(business_id)) {
		pickup_model_id = 1559;
	}

	new Float:x, Float:y, Float:z;

	BusinessEntrancePosition(business_id, x, y, z);

	pickups[business_id] = CreateDynamicPickup(pickup_model_id, 1, x, y, z);
}