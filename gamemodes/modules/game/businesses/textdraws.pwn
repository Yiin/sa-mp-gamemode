#include <YSI\y_hooks>

// i dont even give a fuck anymore about textdraw variables naming
static Text:TD[4];
static PlayerText:PTD[MAX_PLAYERS][6];

hook OnGameModeInit() {
	TD[0] = TextDrawCreate(208.333389, 368.214721, "LD_SPAC:white");
	TextDrawLetterSize(TD[0], 0.000000, 0.000000);
	TextDrawTextSize(TD[0], 63.000000, 17.000000);
	TextDrawAlignment(TD[0], 1);
	TextDrawColor(TD[0], 255);
	TextDrawSetShadow(TD[0], 0);
	TextDrawSetOutline(TD[0], 0);
	TextDrawBackgroundColor(TD[0], 255);
	TextDrawFont(TD[0], 4);
	TextDrawSetProportional(TD[0], 0);
	TextDrawSetShadow(TD[0], 0);

	TD[1] = TextDrawCreate(209.000045, 369.459197, "LD_SPAC:white");
	TextDrawLetterSize(TD[1], 0.000000, 0.000000);
	TextDrawTextSize(TD[1], 62.000000, 15.000000);
	TextDrawAlignment(TD[1], 1);
	TextDrawColor(TD[1], 353703423);
	TextDrawSetShadow(TD[1], 0);
	TextDrawSetOutline(TD[1], 0);
	TextDrawBackgroundColor(TD[1], 255);
	TextDrawFont(TD[1], 4);
	TextDrawSetProportional(TD[1], 0);
	TextDrawSetShadow(TD[1], 0);
	TextDrawSetSelectable(TD[1], true);

	TD[2] = TextDrawCreate(227.666595, 370.444427, "Pirkti");
	TextDrawLetterSize(TD[2], 0.190999, 1.110519);
	TextDrawAlignment(TD[2], 1);
	TextDrawColor(TD[2], -1);
	TextDrawSetShadow(TD[2], 0);
	TextDrawSetOutline(TD[2], 1);
	TextDrawBackgroundColor(TD[2], 126);
	TextDrawFont(TD[2], 2);
	TextDrawSetProportional(TD[2], 1);
	TextDrawSetShadow(TD[2], 0);

	TD[3] = TextDrawCreate(208.333343, 387.866699, "Neuzteka pinigu");
	TextDrawLetterSize(TD[3], 0.200666, 0.865778);
	TextDrawAlignment(TD[3], 1);
	TextDrawColor(TD[3], -1);
	TextDrawSetShadow(TD[3], 0);
	TextDrawSetOutline(TD[3], 1);
	TextDrawBackgroundColor(TD[3], 120);
	TextDrawFont(TD[3], 1);
	TextDrawSetProportional(TD[3], 1);
	TextDrawSetShadow(TD[3], 0);
}


hook OnPlayerConnect(playerid) {
	PTD[playerid][0] = CreatePlayerTextDraw(playerid, 209.0, 281.0, "Verslo patalpos nr. 65");
	PlayerTextDrawLetterSize(playerid, PTD[playerid][0], 0.233333, 1.326221);
	PlayerTextDrawAlignment(playerid, PTD[playerid][0], 1);
	PlayerTextDrawColor(playerid, PTD[playerid][0], -1);
	PlayerTextDrawSetShadow(playerid, PTD[playerid][0], 0);
	PlayerTextDrawSetOutline(playerid, PTD[playerid][0], -1);
	PlayerTextDrawBackgroundColor(playerid, PTD[playerid][0], 255);
	PlayerTextDrawFont(playerid, PTD[playerid][0], 2);
	PlayerTextDrawSetProportional(playerid, PTD[playerid][0], 1);
	PlayerTextDrawSetShadow(playerid, PTD[playerid][0], 0);

	PTD[playerid][1] = CreatePlayerTextDraw(playerid, 209.0, 303.0, "Tipas: ~g~~h~~h~0");
	PlayerTextDrawLetterSize(playerid, PTD[playerid][1], 0.185999, 1.139554);
	PlayerTextDrawAlignment(playerid, PTD[playerid][1], 1);
	PlayerTextDrawColor(playerid, PTD[playerid][1], -1);
	PlayerTextDrawSetShadow(playerid, PTD[playerid][1], 0);
	PlayerTextDrawSetOutline(playerid, PTD[playerid][1], -1);
	PlayerTextDrawBackgroundColor(playerid, PTD[playerid][1], 70);
	PlayerTextDrawFont(playerid, PTD[playerid][1], 2);
	PlayerTextDrawSetProportional(playerid, PTD[playerid][1], 1);
	PlayerTextDrawSetShadow(playerid, PTD[playerid][1], 0);

	PTD[playerid][2] = CreatePlayerTextDraw(playerid, 209.0, 315.0, "Savininkas: ~g~~h~~h~0");
	PlayerTextDrawLetterSize(playerid, PTD[playerid][2], 0.185999, 1.139554);
	PlayerTextDrawAlignment(playerid, PTD[playerid][2], 1);
	PlayerTextDrawColor(playerid, PTD[playerid][2], -1);
	PlayerTextDrawSetShadow(playerid, PTD[playerid][2], 0);
	PlayerTextDrawSetOutline(playerid, PTD[playerid][2], -1);
	PlayerTextDrawBackgroundColor(playerid, PTD[playerid][2], 70);
	PlayerTextDrawFont(playerid, PTD[playerid][2], 2);
	PlayerTextDrawSetProportional(playerid, PTD[playerid][2], 1);
	PlayerTextDrawSetShadow(playerid, PTD[playerid][2], 0);

	PTD[playerid][3] = CreatePlayerTextDraw(playerid, 209.0, 327.0, "Pelnas: ~g~~h~~h~0");
	PlayerTextDrawLetterSize(playerid, PTD[playerid][3], 0.185999, 1.139554);
	PlayerTextDrawAlignment(playerid, PTD[playerid][3], 1);
	PlayerTextDrawColor(playerid, PTD[playerid][3], -1);
	PlayerTextDrawSetShadow(playerid, PTD[playerid][3], 0);
	PlayerTextDrawSetOutline(playerid, PTD[playerid][3], -1);
	PlayerTextDrawBackgroundColor(playerid, PTD[playerid][3], 70);
	PlayerTextDrawFont(playerid, PTD[playerid][3], 2);
	PlayerTextDrawSetProportional(playerid, PTD[playerid][3], 1);
	PlayerTextDrawSetShadow(playerid, PTD[playerid][3], 0);

	PTD[playerid][4] = CreatePlayerTextDraw(playerid, 209.0, 339.0, "Kaina: ~g~~h~~h~0");
	PlayerTextDrawLetterSize(playerid, PTD[playerid][4], 0.185999, 1.139554);
	PlayerTextDrawAlignment(playerid, PTD[playerid][4], 1);
	PlayerTextDrawColor(playerid, PTD[playerid][4], -1);
	PlayerTextDrawSetShadow(playerid, PTD[playerid][4], 0);
	PlayerTextDrawSetOutline(playerid, PTD[playerid][4], -1);
	PlayerTextDrawBackgroundColor(playerid, PTD[playerid][4], 70);
	PlayerTextDrawFont(playerid, PTD[playerid][4], 2);
	PlayerTextDrawSetProportional(playerid, PTD[playerid][4], 1);
	PlayerTextDrawSetShadow(playerid, PTD[playerid][4], 0);

	PTD[playerid][5] = CreatePlayerTextDraw(playerid, 300.0, 303.0, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in consectetur neque, at cursus enim. Sed est eros, gravida n");
	PlayerTextDrawLetterSize(playerid, PTD[playerid][5], 0.185999, 1.139554);
	PlayerTextDrawAlignment(playerid, PTD[playerid][5], 1);
	PlayerTextDrawColor(playerid, PTD[playerid][5], -1);
	PlayerTextDrawSetShadow(playerid, PTD[playerid][5], 0);
	PlayerTextDrawSetOutline(playerid, PTD[playerid][5], -1);
	PlayerTextDrawBackgroundColor(playerid, PTD[playerid][5], 70);
	PlayerTextDrawFont(playerid, PTD[playerid][5], 2);
	PlayerTextDrawSetProportional(playerid, PTD[playerid][5], 1);
	PlayerTextDrawSetShadow(playerid, PTD[playerid][5], 0);
}

hook OnPlayerBusinessEvent(playerid, business_id, event) {
	switch(event) {
		case BUSINESS_EVENT_ENTRANCE_ENTER: {
			EnterEntrance(playerid, business_id);
		}
		case BUSINESS_EVENT_ENTRANCE_LEAVE: {
			LeaveEntrance(playerid, business_id);
		}
	}
}

static EnterEntrance(playerid, business_id) {
	// Parodom visus static textdrawus
	foreach(new td : Array(TD)) {
		// "neu�tenka pinig�" textdraw� ignorinam
		if(Text:td == TD[3]) {
			continue;
		}
		TextDrawShowForPlayer(playerid, Text:td);
	}
	// jeigu �aid�jui neu�tenka pinig� �iam verslui pirkti, parodom "neu�tenka pinig�" prane�im�
	if(GetPlayerCash(playerid) < BusinessPrice(business_id)) {
		TextDrawShowForPlayer(playerid, TD[3]);
	}
	PlayerTextDrawSetString(playerid, PTD[playerid][0], F:0("%s", BusinessName(business_id)));
	PlayerTextDrawSetString(playerid, PTD[playerid][1], F:0("Tipas: ~g~~h~~h~%i", BusinessType(business_id)));
	PlayerTextDrawSetString(playerid, PTD[playerid][2], F:0("Savininkas: ~g~~h~~h~%i", BusinessOwner(business_id)));
	PlayerTextDrawSetString(playerid, PTD[playerid][3], F:0("Pelnas: ~g~~h~~h~%i", BusinessCash(business_id)));
	PlayerTextDrawSetString(playerid, PTD[playerid][4], F:0("Kaina: ~g~~h~~h~%i", BusinessPrice(business_id)));
	PlayerTextDrawSetString(playerid, PTD[playerid][4], F:0("%s", BusinessDescription(business_id)));

	foreach(new ptd : Array(PTD[playerid])) {
		PlayerTextDrawShow(playerid, ptd);
	}
}

static LeaveEntrance(playerid, business_id) {
	// Pasl�piam visus textdrawus
	foreach(new td : Array(TD)) {
		TextDrawHideForPlayer(playerid, Text:td);
	}
	foreach(new ptd : Array(PTD[playerid])) {
		PlayerTextDrawHide(playerid, ptd);
	}
}