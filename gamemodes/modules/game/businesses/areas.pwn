#include <YSI\y_hooks>

static areas[MAX_BUSINESSES];

hook OnBusinessCreated(business_id) {
	new Float:x, Float:y, Float:z;

	BusinessEntrancePosition(business_id, x, y, z);

	areas[business_id] = CreateDynamicCircle(x, y, 3.0);
}

hook OnPlayerEnterDynArea(playerid, areaid) {
	new business_id = -1;

	// patikrinam ar kokio nors verslo ��jimui priklauso �is area
	for(new id; id < sizeof areas; ++id) {
		if(areas[id] == areaid) {
			business_id = id;
			break;
		}
	}

	// ir jei taip callinam kad �aid�jas ant � j� ��jo
	if(business_id != -1) {
		call OnPlayerBusinessEvent(playerid, business_id, BUSINESS_EVENT_ENTRANCE_ENTER);
	}
}

hook OnPlayerLeaveDynArea(playerid, areaid) {
	new business_id = -1;

	// patikrinam ar kokio nors verslo ��jimui priklauso �is area
	for(new id; id < sizeof areas; ++id) {
		if(areas[id] == areaid) {
			business_id = id;
			break;
		}
	}

	// ir jei taip callinam kad �aid�jas j� paliko
	if(business_id != -1) {
		call OnPlayerBusinessEvent(playerid, business_id, BUSINESS_EVENT_ENTRANCE_LEAVE);
	}
}