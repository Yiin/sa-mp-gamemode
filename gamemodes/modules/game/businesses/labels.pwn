#include <YSI\y_hooks>

static entrance_labels[MAX_BUSINESSES];

hook OnBusinessCreated(business_id) {
	new text[200];

	format(text, _, "\
		%s\n\
		{eeeeee}Tipas: {33ff33}%i\n\
		{eeeeee}Kaina: {33ff33}%i\n\
		{eeeeee}Vert�: {33ff33}%i\
	",
		BusinessName(business_id),
		BusinessType(business_id),
		BusinessPrice(business_id),
		BusinessCash(business_id)
	);

	new color;

	if(BusinessOwner(business_id)) {
		color = 0xAA5500FF;
	}
	else {
		color = 0x00AA00FF;
	}

	new Float:x, Float:y, Float:z;

	BusinessEntrancePosition(business_id, x, y, z);

	entrance_labels[business_id] = CreateDynamic3DTextLabel(text, color, x, y, z, 30);
}

