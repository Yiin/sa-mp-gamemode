
CMD:zvejoti(playerid, p[]) {
	if(TryToFish(playerid)) {
		StartFishing(playerid);
	}
	return true;
}

hook OnPlayerKeyStateChange(playerid, newkeys, oldkeys) {
	if(PRESSED(KEY_FIRE)) {
		if(TryToFish(playerid)) {
			StartFishing(playerid);
		}
	}
}

static TryToFish(playerid) {
	new 
		Float: depth = 0.0, 
		Float: playerdepth = 0.0
	;
	if( ! HasItemEquiped(playerid, "Meskere")) {
		// �aid�jas nelaiko rankose me�ker�s
		return false;
	}

	if(CA_IsPlayerInWater(playerid, depth, playerdepth) && playerdepth >= 0.0) {
		M:P:E(playerid, "Esi vandens telkinyje, kurio gylis yra [number]%.2f[]m. Tavo gylis: [number]%.2f[]m.", depth, playerdepth);
	}
	else {
		if(CA_IsPlayerNearWater(playerid)) {
			// �aid�jas yra �alia vandens, ta�iau pats n�ra vandenyje
			return true;
		}
	}
	// �aid�jas n�ra �alia vandens arba yra vandenyje
	return false;
}

static StartFishing(playerid) {
	// todo
	M:P:I(playerid, "�vejoji");
}