/////////////////////////////////////////////////////////////////////////////////////////////////////

Test:ItemRemoval_1() {
    TEST_REPORT("*** Test ItemRemoval_1 start");

    new Item:item = CreateItem("test");
    Container_AddItem(Equipment, 1, item);
    DeleteItem(item);

    ASSERT(Container_GetItemAt(Equipment, 1, 0) == Item:0);
}

Test:ItemRemoval_2() {
    TEST_REPORT("*** Test ItemRemoval_2 start");

    // pakraunam konteinerio informacij� i� Test_ItemRemoval_2.dat failo � Equipment[2]
    Container_Load(Equipment, 1, "scriptfiles/Equipment", "1_Yiin_Debug");

    new Item:item = Container_GetItemAt(Equipment, 1, 0);

    // perkeliam item� � kit� konteiner�
    Container_AddItem(Player, 1, item);

    ASSERT(Container_GetItemAt(Equipment, 1, 0) == Item:0);

    // i�trinam item�
    DeleteItem(item);

    ASSERT(Container_GetItemAt(Player, 1, 0) == Item:0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

Test:OnPlayerUpdateTest() {
    ASSERT(call OnPlayerUpdate(0));
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

#define ALS_DO_DefaultReturnTrue<%0> %0<DefaultReturnTrue,i>(end:unused)
HOOK_RET:OnDefaultReturnTrue() return 1;

hook OnDefaultReturnTrue(unused) {
    return unused;
}

#include <YSI\y_hooks>

hook OnDefaultReturnTrue(unused) {
    #pragma unused unused
    return true;
}

Test:OnDefaultReturnTrueTest() {
    ASSERT(call OnDefaultReturnTrue(1));
    ASSERT(call OnDefaultReturnTrue(0) == 0);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////