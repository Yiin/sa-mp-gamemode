#include <YSI_Coding\y_hooks>

// hook OnGameModeInit() {
// 	FCNPC_InitZMap("SAfull.hmap");
// }

/*
 *	Guardians
 */
 
 // Create
CMD:cg(playerid, params[]) {
	extract params -> new type, skin, weapon;
	new Float:x, Float:y, Float:z, gid;
	GetPlayerPos(playerid, x, y, z);

	if((gid = CreateGuardian(type, x, y, z, skin, weapon))) {
		M:P:G(playerid, "Sargybinis (id:[number]%i[]) sukurtas.", gid);
	}
	else {
		M:P:E(playerid, "Sargybinio sukurti nepavyko.");
	}

	return true;
}

 // Destroy
CMD:dg(playerid, params[]) {
	RemoveGuardian(strval(params));
	M:P:G(playerid, "Sargybinis panaikintas.");
	return true;
}

// Set Point
CMD:spg(playerid, params[]) {
	new Float:x, Float:y, Float:z;
	GetPlayerPos(playerid, x, y, z);
	SetGuardianPoint(strval(params), x, y, z);
	return true;
}

/*
 * NPCs
 */

CMD:testnpc(playerid, name[]) {
	new Float:x, Float:y, Float:z;

	new npcid = FCNPC_Create(name);

	GetPlayerPos(playerid, x, y, z);

	FCNPC_Spawn(npcid, random(299), x, y, z);
	FCNPC_SetAngle(npcid, 0.0);
	FCNPC_SetInterior(npcid, 0);
	// Give him some weapons and ammo
	FCNPC_SetWeapon(npcid, random(11) + 22);
	FCNPC_SetAmmo(npcid, 500);
	SetPlayerMaxHealth(npcid, 500.0);
	UpdatePlayerHealth(npcid, 500.0, .reset = true);

	return M:P:X(playerid, "FCNPC_Create(%s) : %i", name, npcid);
}

CMD:setinv(playerid, params[]) {
	extract params -> new npcid, inv;
	return M:P:X(playerid, "FCNPC_SetInvulnerable(%i) : %i", npcid, FCNPC_SetInvulnerable(npcid, bool:inv));
}

CMD:isinv(playerid, npcid[]) {
	return M:P:X(playerid, "FCNPC_IsInvulnerable(%s) : %i", npcid, FCNPC_IsInvulnerable(strval(npcid)));
}

CMD:create(playerid, npcname[]) {
	return M:P:X(playerid, "FCNPC_Create(%s) : %i", npcname, FCNPC_Create(npcname));
}

CMD:destroy(playerid, npcid[]) {
	return M:P:X(playerid, "FCNPC_Destroy(%s) : %i", npcid, FCNPC_Destroy(strval(npcid)));
}

CMD:spawn(playerid, params[]) {
	new npcid, skinid, Float:x, Float:y, Float:z, Float:a;
	sscanf(params, "I(-1)I(-1)ffff", npcid, skinid, x, y, z, a);

	if(npcid == -1 || ! IsPlayerNPC(npcid)) {
		return M:P:E(playerid, "NPC neeagzisutoja");
	}
	if(skinid == -1) {
		skinid = random(311) + 1;
	}
	if( ! (x && y && z)) {
		GetPlayerPos(playerid, x, y, z);
	}
	if( ! a) {
		GetPlayerFacingAngle(playerid, a);
	}
	return M:P:X(playerid, "FCNPC_Spawn(%i, %i, %f, %f, %f)", npcid, skinid, x, y, z, FCNPC_Spawn(npcid, skinid, x, y, z));
}

CMD:respawn(playerid, npcid[]) {
	return M:P:X(playerid, "FCNPC_Respawn(%i) : %i", strval(npcid), FCNPC_Respawn(strval(npcid)));
}

CMD:isspawned(playerid, npcid[]) {
	return M:P:X(playerid, "FCNPC_IsSpawned(%i) : %i", strval(npcid), FCNPC_IsSpawned(strval(npcid)));
}

CMD:kill(playerid, npcid[]) {
	return M:P:X(playerid, "FCNPC_Kill(%i) : %i", strval(npcid), FCNPC_Kill(strval(npcid)));
}

CMD:isdead(playerid, npcid[]) {
	return M:P:X(playerid, "FCNPC_IsDead(%i) : %i", strval(npcid), FCNPC_IsDead(strval(npcid)));
}

CMD:givegun(playerid, params[]) {
	extract params -> new npcid, wepid, ammo;
	if(IsPlayerNPC(npcid)) {
		FCNPC_SetWeapon(npcid, wepid);
		FCNPC_SetAmmo(npcid, ammo);
	}
	else {
		GivePlayerWeapon(npcid, wepid, ammo);
	}
	return true;
}

CMD:shootatme(playerid, params[]) {
	new Float:x, Float:y, Float:z, npcid = strval(params);
	GetPlayerPos(playerid, x, y, z);

	new ret = FCNPC_AimAt(npcid, x, y, z, true);
	if(ret) {
		new shootrate;
		if((shootrate = GetWeaponShootRate(FCNPC_GetWeapon(npcid)))) {
			FCNPC_AimAtPlayer(npcid, playerid, true, shootrate);
		}
		else {
			M:P:E(playerid, "shootrate: %i, weaponid: %i", shootrate, FCNPC_GetWeapon(npcid));
		}
	}
	return M:P:X(playerid, "FCNPC_AimAt(%i, %f, %f, %f, true) : %i", npcid, x, y, z, ret);
}

CMD:stopshooting(playerid, npcid[]) {
	new ret = FCNPC_StopAim(strval(npcid));

	return M:P:X(playerid, "FCNPC_StopAim(%i) : %i", strval(npcid), ret);
}

CMD:comehere(playerid, npcid[]) {
	new Float:x, Float:y, Float:z;
	GetPlayerPos(playerid, x, y, z);
	return M:P:X(playerid, "FCNPC_GoTo(%i, %f, %f, %f, MOVE_TYPE_RUN, 0.0, true) : %i", strval(npcid), x, y, z, FCNPC_GoTo(strval(npcid), x, y, z, MOVE_TYPE_RUN, 0.0, true));
}

CMD:stopmoving(playerid, npcid[]) {
	return M:P:X(playerid, "FCNPC_Stop(%i) : %i", strval(npcid), FCNPC_Stop(strval(npcid)));
}

CMD:sithere(playerid, npcid[]) {
	new vehicleid = GetPlayerVehicleID(playerid);
	new seat = vehicle_FindEmptySeat(vehicleid);
	return M:P:X(playerid, "FCNPC_EnterVehicle(%i, %i, %i, MOVE_TYPE_WALK) : %i", strval(npcid), vehicleid, seat, MOVE_TYPE_WALK, FCNPC_EnterVehicle(strval(npcid), vehicleid, seat, MOVE_TYPE_WALK));
}

CMD:exitvehicle(playerid, npcid[]) {
	return M:P:X(playerid, "FCNPC_ExitVehicle(%i) : %i", strval(npcid), FCNPC_ExitVehicle(strval(npcid)));
}

public FCNPC_OnSpawn(npcid) {
	FCNPC_ToggleReloading(npcid, true);
	FCNPC_ToggleInfiniteAmmo(npcid, true);
	return M:P:I(0, "FCNPC_OnSpawn(%i)", npcid), 1;
}