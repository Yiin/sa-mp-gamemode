#include <YSI_Coding\y_hooks>

new Text:TD_DebugText;

hook OnGameModeInit() {
	TD_DebugText = TextDrawCreate(336.666473, 345.970306, "TDEditor");
	TextDrawLetterSize(TD_DebugText, 0.240333, 1.425777);
	TextDrawTextSize(TD_DebugText, 481.000000, 0.000000);
	TextDrawAlignment(TD_DebugText, 1);
	TextDrawColor(TD_DebugText, -1);
	TextDrawUseBox(TD_DebugText, 1);
	TextDrawBoxColor(TD_DebugText, 0);
	TextDrawSetShadow(TD_DebugText, 0);
	TextDrawSetOutline(TD_DebugText, -1);
	TextDrawBackgroundColor(TD_DebugText, 16);
	TextDrawFont(TD_DebugText, 2);
	TextDrawSetProportional(TD_DebugText, 1);
	TextDrawSetShadow(TD_DebugText, 0);
}

DebugText(playerid, const text[], va_args<>) {
	static buffer[256];

	va_formatex(buffer, _, text, va_start<2>);

	TextDrawSetString(TD_DebugText, buffer);
	TextDrawShowForPlayer(playerid, TD_DebugText);
}

CMD:vw(playerid, params[]) {
	SetPlayerVirtualWorld(playerid, strval(params));

	return 1;
}

CMD:interior(playerid, params[]) {
	SetPlayerInterior(playerid, strval(params));

	return 1;
}

CMD:unequip(playerid, params[]) {
	Container_Clear(Equipment, playerid);
	return 1;
}

CMD:gunammo(playerid, params[]) {
	GiveNewItemToPlayer(playerid, "M4A1");
	GiveNewItemToPlayer(playerid, "Kulku paketas", 528);

	return 1;
}

CMD:getcash(playerid, params[]) {
	AdjustPlayerCash(playerid, strval(params));
	GivePlayerMoney(playerid, strval(params));
	return true;
}

CMD:setmypos(playerid, p[]) {
	extract p -> new Float:x, Float:y, Float:z;
	player_Teleport(playerid, x,y,z);
	return 1;
}

hook OnPlayerClickMap(playerid, Float:fX, Float:fY, Float:fZ) {
	SetPlayerPosFindZ(playerid, fX, fY, fZ + 2000.0);
	return true;
}