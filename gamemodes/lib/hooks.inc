#include <YSI\y_hooks>

DEFINE_HOOK_REPLACEMENT(Destination   , Dest);

#define ALS_R_CharacterDespawn 1
#define ALS_R_PlayerSelectTargetMenu 1
#define ALS_R_PlayerOpenTargetMenu 1
#define ALS_R_CreateVehicleORM 1
#define ALS_R_CreatePlayerORM 1
#define ALS_R_PlOpenTargetVehMenu 1
#define ALS_R_PlSelectTargetVehMenu 1
#define ALS_R_PlayerDamage 1
#define ALS_R_PlayerDamageDone 1
#define ALS_R_PlayerDeathFinished 1
#define ALS_R_PlayerLeftJob 1
#define ALS_R_PlayerJoinJob 1
#define ALS_R_JobSave 1
#define ALS_R_JobLoad 1
#define ALS_R_PlayerOpenLeaderMenu 1
#define ALS_R_PlayerSelectLeaderMenu 1
#define ALS_R_GuardianDetectTarget 1
#define ALS_R_NpcTakeDamage 1
#define ALS_R_GangLoad 1
#define ALS_R_GangSave 1
#define ALS_R_ResetPlayerVars 1
#define ALS_R_PlayerKillPlayer 1
#define ALS_R_GangWarEnd 1
#define ALS_R_PlayerCashUpdate 1
#define ALS_R_PlayerLevelUp 1
#define ALS_R_PlayerUseItem 0
HOOK_RET:OnPlayerUseItem() return 0;
#define ALS_R_PlayerSelectItem 0
HOOK_RET:OnPlayerSelectItem() return 0;
#define ALS_R_SelectManagedItemMenu 1
#define ALS_R_OpenManagedItemMenu 1
#define ALS_R_InventoryClose 1
#define ALS_R_PlayerDataLoaded 1
#define ALS_R_PlayerEquipItem 0
HOOK_RET:OnPlayerEquipItem() return 0;
#define ALS_R_PlayerTakeOffItem 1
#define ALS_R_InventoryLoaded 1
#define ALS_R_GenerateItemInfo 1
#define ALS_R_PlayerExtinguishFire 1
#define ALS_R_SelectQuestFromList 1
#define ALS_R_GenerateQuestList 1
#define ALS_R_UpdateQuestProgress 1
#define ALS_R_RequestQuestProgress 0
HOOK_RET:OnRequestQuestProgress() return 0;
#define ALS_R_PlayerEnterEntrance 1
#define ALS_R_PlayerLeaveEntrance 1
#define ALS_R_PlayerUseEntrance 1
#define ALS_R_PlayerEnterHouse 1
#define ALS_R_PlayerLeftHouse 1
#define ALS_R_PermissionCheck 1
#define ALS_R_PlayerRegister 1
#define ALS_R_PlayerLogin 1
#define ALS_R_PlayerEvent 1
// Alternates
#define ALS_R_PlayerLeaveRaceCP 1
#define ALS_R_PlayerEnterRaceCP 1
#define ALS_R_VehicleDamageStatusUpd 1
#define ALS_R_UnoccupiedVehicleUpd 1
#define ALS_R_PlayerClickPlayerTD 1
#define ALS_R_PlayerEditAttachedObj 1
#define ALS_R_PlayerCmdPerformed 1
#define ALS_R_PlayerEditDynObject 1
#define ALS_R_PlayerSelectDynObject 1
#define ALS_R_PlayerPickUpDynPickup 1
#define ALS_R_PlayerEnterDynRaceCP 1
#define ALS_R_PlayerLeaveDynRaceCP 1
#define ALS_R_PlayerEnterDynArea 1
#define ALS_R_PlayerLeaveDynArea 1
#define ALS_R_PlSelectTargetMenu 1
#define ALS_R_PlSelectLeaderMenu 1

HOOK_RET:OnGuardianDetectTarget() return 0;
HOOK_RET:OnPlayerTriesToUseItem() return 1;

#define ALS_DO_CharacterDespawn<%0> %0<CharacterDespawn,i>(end:playerid)
#define ALS_DO_PlayerSelectTargetMenu<%0> %0<PlayerSelectTargetMenu,iii>(more:playerid, more:targetid, end:li)
#define ALS_DO_PlayerOpenTargetMenu<%0> %0<PlayerOpenTargetMenu,ii>(more:playerid, end:targetid)
#define ALS_DO_CreateVehicleORM<%0> %0<CreateVehicleORM,ii>(ORM:ormid, end:vehicleid)
#define ALS_DO_CreatePlayerORM<%0> %0<CreatePlayerORM,ii>(ORM:ormid, end:playerid)
#define ALS_DO_PlOpenTargetVehMenu<%0> %0<PlOpenTargetVehMenu,ii>(more:playerid, end:vehicleid)
#define ALS_DO_PlSelectTargetVehMenu<%0> %0<PlSelectTargetVehMenu,iii>(more:playerid, more:vehicleid, end:li)
#define ALS_DO_PlayerDamage<%0> %0<PlayerDamage,ifiii>(more:playerid, Float:amount, more:issuerid, more:weapon, end:bodypart)
#define ALS_DO_PlayerDamageDone<%0> %0<PlayerDamageDone,ifiii>(more:playerid, Float:amount, more:issuerid, more:weapon, end:bodypart)
#define ALS_DO_PlayerDeathFinished<%0> %0<PlayerDeathFinished,ii>(more:playerid, end_tag:bool:cancelable)
#define ALS_DO_PlayerLeftJob<%0> %0<PlayerLeftJob,ii>(more:playerid, end:job)
#define ALS_DO_PlayerJoinJob<%0> %0<PlayerJoinJob,ii>(more:playerid, end:job)
#define ALS_DO_JobSave<%0> %0<JobSave,is>(more:job, end_string:file)
#define ALS_DO_JobLoad<%0> %0<JobLoad,is>(more:job, end_string:file)
#define ALS_DO_PlayerOpenLeaderMenu<%0> %0<PlayerOpenLeaderMenu,ii>(more:playerid, end:job)
#define ALS_DO_PlayerSelectLeaderMenu<%0> %0<PlayerSelectLeaderMenu,ii>(more:playerid, end:job)
#define ALS_DO_GuardianDetectTarget<%0> %0<GuardianDetectTarget,ii>(more:gid, end:target)
#define ALS_DO_NpcTakeDamage<%0> %0<NpcTakeDamage,iiiif>(more:npcid, more:damagerid, more:weaponid, more:bodypart, end_Float:health_loss)
#define ALS_DO_GangLoad<%0> %0<GangLoad,i>(end:gang)
#define ALS_DO_GangSave<%0> %0<GangSave,i>(end:gang)
#define ALS_DO_ResetPlayerVars<%0> %0<ResetPlayerVars,i>(end:playerid)
#define ALS_DO_PlayerKillPlayer<%0> %0<PlayerKillPlayer,iii>(more:playerid, more:killerid, end:weaponid)
#define ALS_DO_GangWarEnd<%0> %0<GangWarEnd,ii>(more:winner, end:loser)
#define ALS_DO_PlayerCashUpdate<%0> %0<PlayerCashUpdate,ii>(more:playerid, end:difference)
#define ALS_DO_PlayerLevelUp<%0> %0<PlayerLevelUp,i>(end:playerid)
#define ALS_DO_PlayerUseItem<%0> %0<PlayerUseItem,isi>(more:playerid, string:item_name[], end_tag:Item:uiid)
#define ALS_DO_PlayerSelectItem<%0> %0<PlayerSelectItem,isi>(more:playerid, string:item_name[], end_tag:Item:uiid)
#define ALS_DO_SelectManagedItemMenu<%0> %0<SelectManagedItemMenu,isi>(more:playerid, string:item_name[], end_tag:Item:uiid)
#define ALS_DO_OpenManagedItemMenu<%0> %0<OpenManagedItemMenu,isi>(more:playerid, string:item_name[], end_tag:Item:uiid)
#define ALS_DO_InventoryClose<%0> %0<InventoryClose,i>(end:playerid)
#define ALS_DO_PlayerDataLoaded<%0> %0<PlayerDataLoaded,i>(end:playerid)
#define ALS_DO_PlayerEquipItem<%0> %0<PlayerEquipItem,isi>(more:playerid, string:item_name[], end_tag:Item:uiid)
#define ALS_DO_PlayerTakeOffItem<%0> %0<PlayerTakeOffItem,isi>(more:playerid, string:item_name[], end_tag:Item:uiid)
#define ALS_DO_InventoryLoaded<%0> %0<InventoryLoaded,i>(end:playerid)
#define ALS_DO_GenerateItemInfo<%0> %0<GenerateItemInfo,i>(end_tag:Item:uiid)
#define ALS_DO_PlayerExtinguishFire<%0> %0<PlayerExtinguishFire,i>(end:playerid)
#define ALS_DO_SelectQuestFromList<%0> %0<SelectQuestFromList,i>(end:playerid)
#define ALS_DO_GenerateQuestList<%0> %0<GenerateQuestList,i>(end:playerid)
#define ALS_DO_UpdateQuestProgress<%0> %0<UpdateQuestProgress,isi>(more:playerid, string:quest[], end:progress)
#define ALS_DO_RequestQuestProgress<%0> %0<RequestQuestProgress,is>(more:playerid, end_string:quest[])
#define ALS_DO_PlayerEnterEntrance<%0> %0<PlayerEnterEntrance,ii>(more:playerid, end:entranceid)
#define ALS_DO_PlayerLeaveEntrance<%0> %0<PlayerLeaveEntrance,ii>(more:playerid, end:entranceid)
#define ALS_DO_PlayerUseEntrance<%0> %0<PlayerUseEntrance,ii>(more:playerid, end:entranceid)
#define ALS_DO_PlayerEnterHouse<%0> %0<PlayerEnterHouse,ii>(more:playerid, end:houseid)
#define ALS_DO_PlayerLeftHouse<%0> %0<PlayerLeftHouse,ii>(more:playerid, end:houseid)
#define ALS_DO_PermissionCheck<%0> %0<PermissionCheck,isssi>(more:playerid, string:entity[], string:node[], string:node_child[], end:entity_id)
#define ALS_DO_PlayerRegister<%0> %0<PlayerRegister,i>(end:playerid)
#if defined ALS_DO_PlayerLogin
	#undef ALS_DO_PlayerLogin
#endif
#define ALS_DO_PlayerLogin<%0> %0<PlayerLogin,i>(end:playerid)
#define ALS_DO_PlayerEvent<%0> %0<PlayerEvent,ii>(more:playerid, end:event)
#define ALS_DO_CharSelectStateUpdate<%0> %0<CharSelectStateUpdate,ii>(more:playerid, end:new_state)
#define ALS_DO_BusinessCreated<%0> %0<BusinessCreated,i>(end:business_id)
#define ALS_DO_PlayerBusinessEvent<%0> %0<PlayerBusinessEvent,i>(more:playerid, more:business_id, end:event)
#define ALS_DO_PlayerTriesToUseItem<%0> %0<PlayerTriesToUseItem,i>(more:playerid, string:item_name[], end_tag:Item:uiid)

// array support
#define ALS_KS_array:%0[%1],    %0[%1],ALS_KS_
#define ALS_KS_end_array:%0[%1]) %0[%1])

#define MAX_CALLBACK_INLINES 50

enum E_AVAILABLE_INLINE_CALLBACKS {
	IC_OnPlayerTakeOffItem = 1,
	IC_OnPlayerEvent
};
new callbackInlines[E_AVAILABLE_INLINE_CALLBACKS][MAX_CALLBACK_INLINES][E_CALLBACK_DATA];

hook OnGameModeInit() {
	Iter_Init(callbackInlines);
}

stock RegisterInlineCallback(E_AVAILABLE_INLINE_CALLBACKS:ic, callback:callback) {
	new data[E_CALLBACK_DATA];

	new params[255];
	switch(ic) {
		case IC_OnPlayerTakeOffItem: params = "isi";
		case IC_OnPlayerEvent: params = "ii";
	}
	printf("CALLBACK %i PARAMS %s", _:ic, params);
	new x = Callback_Get(callback, data, params);

	if(!x) {
		printf("CALLBACK %i GET FAILED", _:ic);
		return false;
	}

	for(new i; i < sizeof callbackInlines[]; ++i) {
		// toks callbackas jau u�registruotas
		if(callbackInlines[ic][i][E_CALLBACK_DATA_POINTER] == data[E_CALLBACK_DATA_POINTER]) {
			printf("CALLBACK %i ALEADY REGISTERED", _:ic);
			return true;
		}
	}
	for(new i; i < sizeof callbackInlines[]; ++i) {
		if(!callbackInlines[ic][i][E_CALLBACK_DATA_POINTER]) {
			callbackInlines[ic][i] = data;
			return true;
		}
	}
	return false;
}

hook OnPlayerTakeOffItem(playerid, item_name[], Item:item) {
	for(new i; i < MAX_CALLBACK_INLINES; ++i) {
		if(callbackInlines[IC_OnPlayerTakeOffItem][i][E_CALLBACK_DATA_POINTER]) {
			Callback_Call(callbackInlines[IC_OnPlayerTakeOffItem][i], playerid, item_name, _:item);
		}
	}
}

hook OnPlayerEvent(playerid, event) {
	for(new i; i < MAX_CALLBACK_INLINES; ++i) {
		if(callbackInlines[IC_OnPlayerEvent][i][E_CALLBACK_DATA_POINTER]) {
			Callback_Call(callbackInlines[IC_OnPlayerEvent][i], playerid, event);
		}
	}
}