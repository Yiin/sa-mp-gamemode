#define EVENT_LOADING_PREVIEW_SKIN (1)
#define EVENT_LOADED_PREVIEW_SKIN (2)
#define EVENT_GAMESTART_SPAWN (3)
#define EVENT_CONNECTED (4)
#define EVENT_HOSPITALIZED (5)
#define EVENT_DISCHARGED_FROM_HOSPITAL (6)

#define BUSINESS_EVENT_ENTRANCE_ENTER (7)
#define BUSINESS_EVENT_ENTRANCE_LEAVE (8)