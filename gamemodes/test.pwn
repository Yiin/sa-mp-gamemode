#include <YSI\y_hooks>

new cb[E_CALLBACK_DATA];

hook OnGameModeInit()
{
	inline a() {
		print("inline function called: a");
	}

	register(using inline a);
}

forward timer231321();
public timer231321() {
	Callback_Call(cb);
}

register(callback:a) {
	if(Callback_Get(a, cb)) {
		SetTimer("timer231321", 1000, true);
	}
	else {
		print("can't get callback data");
	}
}