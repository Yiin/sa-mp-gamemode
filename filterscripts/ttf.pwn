#define FILTERSCRIPT

#include <a_samp>

public OnFilterScriptInit() {

//	new Text:fix = TextDrawCreate(0.0, 0.0, "fix");
//	TextDrawLetterSize(fix, 1.000000, 1.000000);
//	TextDrawShowForPlayer(0, fix);

	new Text:x = TextDrawCreate(200.0, 255.0, "LD_SPAC:white");
	TextDrawLetterSize(x, 0.0, 0.0);
	TextDrawAlignment(x, 1);
	TextDrawColor(x, -230);
	TextDrawUseBox(x, 0);
	TextDrawSetShadow(x, 0);
	TextDrawSetOutline(x, 0);
	TextDrawFont(x, 4);
	TextDrawTextSize(x, 90.0, 90.0);
	TextDrawBoxColor(x, 255);
	TextDrawBackgroundColor(x, 255);
	TextDrawSetProportional(x, 0);

	TextDrawShowForPlayer(0, x);
	return 1;
}
